##### Odontoclinica #####

Queria treinar um pouco de programação em C++ e aprender o framework QT. Por isso resolvi desenvolver um sistema cliente-servidor para automatizar os processos da clínica odontológica da minha mulher.

Neste repositório estão os fontes do projeto odondoclinica feito em C++ com QT5.5 usando o QT Creator.

Por enquanto as dependências são:

QxOrm 1.1.8 (http://www.qxorm.com)

boost 1.57 (http://www.qxorm.com/lib/boost_1_57.zip)

QxtCore e QxtWidgets (ambos eu baixei de https://bitbucket.org/libqxt/libqxt/wiki/Home e criei meu próprio repositório - qxtwidgets_adapted para fazer as adaptações necessárias)

 
A parte servidor é um BD MySQL 5. O script para criação do esquema de encontra em:
odontoclinica/scripts/create_schema.sql

Contato: vagnerfonseca@yahoo.com