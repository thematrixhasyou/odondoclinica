#ifndef _CLINICA_CLIENTE_H_
#define _CLINICA_CLIENTE_H_

#include <QxOrm.h>
#include <QSqlQueryModel>
#include "persist/include/clinica_export.h"

namespace clinica {
class plano_odontologico;
class agendamento;
class tratamento;
}

namespace clinica {

class CLINICA_EXPORT cliente : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::cliente)

   QX_PERSISTABLE_HPP(clinica::cliente)

public:

   typedef QSharedPointer<clinica::plano_odontologico> type_plano_odontologico;
   typedef qx::QxCollection<long, QSharedPointer<clinica::agendamento> > type_listaAgendamento;
   typedef qx::QxCollection<long, QSharedPointer<clinica::tratamento> > type_listaTratamento;


protected:

   long m_cliente_id;
   QDateTime m_inativo;
   QString m_nome;
   QString m_doc_identidade;
   QString m_cpf;
   QString m_genero;
   QString m_profissao;
   QString m_cep;
   QString m_logradouro;
   QString m_num_endereco;
   QString m_compl_endereco;
   QString m_bairro;
   QString m_cidade;
   QString m_uf;
   QString m_ddd_celular;
   QString m_tel_celular;
   QString m_ddd_fixo;
   QString m_tel_fixo;
   QString m_observacao;
   type_listaAgendamento m_listaAgendamento;
   type_listaTratamento m_listaTratamento;
   type_plano_odontologico m_plano_odontologico;

public:

   cliente();
   cliente(const long & id);
   virtual ~cliente();

   long getcliente_id() const;
   QDateTime getinativo() const;
   QString getnome() const;
   QString getdoc_identidade() const;
   QString getcpf() const;
   QString getgenero() const;
   QString getprofissao() const;
   QString getcep() const;
   QString getlogradouro() const;
   QString getnum_endereco() const;
   QString getcompl_endereco() const;
   QString getbairro() const;
   QString getcidade() const;
   QString getuf() const;
   QString getddd_celular() const;
   QString gettel_celular() const;
   QString getddd_fixo() const;
   QString gettel_fixo() const;
   QString getobservacao() const;
   type_plano_odontologico getplano_odontologico() const;
   type_listaAgendamento getlistaAgendamento() const;
   type_listaAgendamento & listaAgendamento();
   const type_listaAgendamento & listaAgendamento() const;
   type_listaTratamento getlistaTratamento() const;
   type_listaTratamento & listaTratamento();
   const type_listaTratamento & listaTratamento() const;


   void setcliente_id(const long & val);
   void setinativo(const QDateTime & val);
   void setnome(const QString & val);
   void setdoc_identidade(const QString & val);
   void setcpf(const QString & val);
   void setgenero(const QString & val);
   void setprofissao(const QString & val);
   void setcep(const QString & val);
   void setlogradouro(const QString & val);
   void setnum_endereco(const QString & val);
   void setcompl_endereco(const QString & val);
   void setbairro(const QString & val);
   void setcidade(const QString & val);
   void setuf(const QString & val);
   void setddd_celular(const QString & val);
   void settel_celular(const QString & val);
   void setddd_fixo(const QString & val);
   void settel_fixo(const QString & val);
   void setobservacao(const QString & val);
   void setplano_odontologico(const type_plano_odontologico & val);
   void setlistaAgendamento(const type_listaAgendamento & val);
   void setlistaTratamento(const type_listaTratamento & val);

   type_plano_odontologico getplano_odontologico(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaAgendamento getlistaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaAgendamento & listaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaTratamento getlistaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaTratamento & listaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar();
   QString inativar();
   void load();
   type_listaTratamento getlistaTratamentoOrndenadoPorDataInicioDescDentistaAsc();

   static qx::QxCollection<long, QSharedPointer<clinica::cliente> > buscaTextualPorNome(QString text) {
       qx::QxSqlQuery query;
       QStringList palavras = text.split(" ");
       QString text_search;

       for (int i = 0; i < palavras.size(); ++i) {
           palavras.replace(i, "+" + palavras.at(i));
           if(i == palavras.size()-1) {
               palavras.replace(i, palavras.at(i) + "*");
           }
       }
       text_search = palavras.join(" ");

       query.query("SELECT cliente.*, MATCH(cliente.nome, cliente.cpf, cliente.doc_identidade) AGAINST('" + text_search + "' IN BOOLEAN MODE) AS score FROM cliente WHERE MATCH(cliente.nome, cliente.cpf, cliente.doc_identidade) AGAINST('" + text_search + "' IN BOOLEAN MODE) > 0 ORDER BY score DESC, nome");

       qx::QxCollection<long, QSharedPointer<clinica::cliente> > lista_clientes;
       QSqlError daoError = qx::dao::execute_query(query, lista_clientes);
       if(daoError.isValid()) {
           lista_clientes.clear();
       }
       return lista_clientes;
   }


    static QAbstractItemModel* retornaTodosNomeID(QObject * parent = 0) {
        QSqlQueryModel *cliente_model = new QSqlQueryModel(parent);
        cliente_model->setQuery(QString("select concat(nome, ' - ', lpad(cliente_id, 6, '0')), cliente_id from cliente where inativo is null order by nome"), qx::QxSqlDatabase::getSingleton()->getDatabase());
        return cliente_model;
    }

    static QSharedPointer<clinica::cliente> retornaPeloID(long cliente_id) {
        QSharedPointer<clinica::cliente> cliente;
        cliente.reset(new clinica::cliente());
        cliente->setcliente_id(cliente_id);
        QSqlError daoError = qx::dao::fetch_by_id(cliente);
        if(daoError.isValid()) {
            cliente.reset(new clinica::cliente());
        }
        return cliente;
    }

    static bool existeCPF(QString cpf) {
        qx::QxSqlQuery query("select count(*) as quantidade from cliente where cpf = :cpf");
        query.bind(":cpf", QVariant::fromValue(cpf));

        qx::dao::call_query(query);

        return (query.getSqlResultAt(0, "quantidade").toInt() > 0);
    }

public:

   static QString relation_plano_odontologico() { return "plano_odontologico_id"; }
   static QString relation_listaAgendamento() { return "listaAgendamento"; }
   static QString relation_listaTratamento() { return "listaTratamento"; }


public:

   static QString column_cliente_id() { return "cliente_id"; }
   static QString column_inativo() { return "inativo"; }
   static QString column_nome() { return "nome"; }
   static QString column_doc_identidade() { return "doc_identidade"; }
   static QString column_cpf() { return "cpf"; }
   static QString column_genero() { return "genero"; }
   static QString column_profissao() { return "profissao"; }
   static QString column_cep() { return "cep"; }
   static QString column_logradouro() { return "logradouro"; }
   static QString column_num_endereco() { return "num_endereco"; }
   static QString column_compl_endereco() { return "compl_endereco"; }
   static QString column_bairro() { return "bairro"; }
   static QString column_cidade() { return "cidade"; }
   static QString column_uf() { return "uf"; }
   static QString column_ddd_celular() { return "ddd_celular"; }
   static QString column_tel_celular() { return "tel_celular"; }
   static QString column_ddd_fixo() { return "ddd_fixo"; }
   static QString column_tel_fixo() { return "tel_fixo"; }
   static QString column_observacao() { return "observacao"; }

public:

   static QString table_name() { return "cliente"; }

};

typedef QSharedPointer<cliente> cliente_ptr;
typedef qx::QxCollection<long, cliente_ptr> list_of_cliente;


} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::cliente, QObject, 0, clinica_cliente)

#include "clinica_plano_odontologico.h"
#include "clinica_agendamento.h"
#include "clinica_tratamento.h"


#endif // _CLINICA_CLIENTE_H_
