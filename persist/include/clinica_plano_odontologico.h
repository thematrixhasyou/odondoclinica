#ifndef _CLINICA_PLANO_ODONTOLOGICO_H_
#define _CLINICA_PLANO_ODONTOLOGICO_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class cliente;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT plano_odontologico : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::plano_odontologico)

   QX_PERSISTABLE_HPP(clinica::plano_odontologico)

public:

   typedef qx::QxCollection<long, QSharedPointer<clinica::cliente> > type_list_of_cliente;

protected:

   long m_plano_odontologico_id;
   QString m_nome;
   QString m_observacao;
   type_list_of_cliente m_list_of_cliente;

public:

   plano_odontologico();
   plano_odontologico(const long & id);
   virtual ~plano_odontologico();

   long getplano_odontologico_id() const;
   QString getnome() const;
   QString getobservacao() const;
   type_list_of_cliente getlist_of_cliente() const;
   type_list_of_cliente & list_of_cliente();
   const type_list_of_cliente & list_of_cliente() const;

   void setplano_odontologico_id(const long & val);
   void setnome(const QString & val);
   void setobservacao(const QString & val);
   void setlist_of_cliente(const type_list_of_cliente & val);

   type_list_of_cliente getlist_of_cliente(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_list_of_cliente & list_of_cliente(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar();
   QString inativar();

   static qx::QxCollection<long, QSharedPointer<clinica::plano_odontologico> > retornaTodosPlanosOrdenadosPorNome() {
       qx::QxSqlQuery query("ORDER BY plano_odontologico.nome");
       qx::QxCollection<long, QSharedPointer<clinica::plano_odontologico> > lista_planos;
       QSqlError daoError = qx::dao::fetch_by_query(query, lista_planos);
       if(daoError.isValid()) {
           lista_planos.clear();
       }
       return lista_planos;
   }

public:

   static QString relation_list_of_cliente() { return "list_of_cliente"; }

public:

   static QString column_plano_odontologico_id() { return "plano_odontologico_id"; }
   static QString column_nome() { return "nome"; }
   static QString column_observacao() { return "observacao"; }

public:

   static QString table_name() { return "plano_odontologico"; }

};

typedef QSharedPointer<plano_odontologico> plano_odontologico_ptr;
typedef qx::QxCollection<long, plano_odontologico_ptr> list_of_plano_odontologico;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::plano_odontologico, QObject, 0, clinica_plano_odontologico)

#include "clinica_cliente.h"

#endif // _CLINICA_PLANO_ODONTOLOGICO_H_
