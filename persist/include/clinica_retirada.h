#ifndef _CLINICA_RETIRADA_H_
#define _CLINICA_RETIRADA_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class caixa;
} 

namespace clinica {

class CLINICA_EXPORT retirada : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::retirada)

   QX_PERSISTABLE_HPP(clinica::retirada)

public:

   typedef QSharedPointer<clinica::caixa> type_caixa;

protected:

   long m_retirada_id;
   QString m_tipo;
   double m_valor;
   QString m_autor;
   QString m_motivo;
   type_caixa m_caixa;

public:

   retirada();
   retirada(const long & id);
   virtual ~retirada();

   long getretirada_id() const;
   QString gettipo() const;
   double getvalor() const;
   QString getautor() const;
   QString getmotivo() const;
   type_caixa getcaixa() const;

   void setretirada_id(const long & val);
   void settipo(const QString & val);
   void setvalor(const double & val);
   void setautor(const QString & val);
   void setmotivo(const QString & val);
   void setcaixa(const type_caixa & val);

   type_caixa getcaixa(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar();

public:

   static QString relation_caixa() { return "caixa_id"; }

public:

   static QString column_retirada_id() { return "retirada_id"; }
   static QString column_tipo() { return "tipo"; }
   static QString column_valor() { return "valor"; }
   static QString column_autor() { return "autor"; }
   static QString column_motivo() { return "motivo"; }

public:

   static QString table_name() { return "retirada"; }

};

typedef QSharedPointer<retirada> retirada_ptr;
typedef qx::QxCollection<long, retirada_ptr> list_of_retirada;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::retirada, QObject, 0, clinica_retirada)

#include "clinica_caixa.h"

#endif // _CLINICA_RETIRADA_H_
