#ifndef _CLINICA_DENTISTA_H_
#define _CLINICA_DENTISTA_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class horarioTrabalho;
class especialidade;
class agendamento;
class tratamento;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT dentista : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::dentista)

   QX_PERSISTABLE_HPP(clinica::dentista)

public:

   typedef qx::QxCollection<long, QSharedPointer<clinica::horarioTrabalho> > type_listaHorarioTrabalho;
   typedef qx::QxCollection<long, QSharedPointer<clinica::especialidade> > type_listaEspecialidade;
   typedef qx::QxCollection<long, QSharedPointer<clinica::agendamento> > type_listaAgendamento;
   typedef qx::QxCollection<long, QSharedPointer<clinica::tratamento> > type_listaTratamento;


protected:

   long m_id;
   QString m_nome;
   QString m_registroCro;
   int m_duracaoConsulta;
   float m_percentualComissao;
   QString m_inativo;
   type_listaHorarioTrabalho m_listaHorarioTrabalho;
   type_listaEspecialidade m_listaEspecialidade;
   type_listaAgendamento m_listaAgendamento;
   type_listaTratamento m_listaTratamento;


public:

   dentista();
   dentista(const long & id);
   virtual ~dentista();

   long getid() const;
   QString getnome() const;
   QString getregistroCro() const;
   int getduracaoConsulta() const;
   float getpercentualComissao() const;
   QString getinativo() const;
   type_listaHorarioTrabalho getlistaHorarioTrabalho() const;
   type_listaHorarioTrabalho & listaHorarioTrabalho();
   const type_listaHorarioTrabalho & listaHorarioTrabalho() const;
   type_listaEspecialidade getlistaEspecialidade() const;
   type_listaEspecialidade & listaEspecialidade();
   const type_listaEspecialidade & listaEspecialidade() const;
   type_listaAgendamento getlistaAgendamento() const;
   type_listaAgendamento & listaAgendamento();
   const type_listaAgendamento & listaAgendamento() const;
   type_listaTratamento getlistaTratamento() const;
   type_listaTratamento & listaTratamento();
   const type_listaTratamento & listaTratamento() const;



   void setid(const long & val);
   void setnome(const QString & val);
   void setregistroCro(const QString & val);
   void setduracaoConsulta(const int & val);
   void setpercentualComissao(const float & val);
   void setinativo(const QString & val);
   void setlistaHorarioTrabalho(const type_listaHorarioTrabalho & val);
   void setlistaEspecialidade(const type_listaEspecialidade & val);
   void setlistaAgendamento(const type_listaAgendamento & val);
   void setlistaTratamento(const type_listaTratamento & val);



   type_listaHorarioTrabalho getlistaHorarioTrabalho(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaHorarioTrabalho & listaHorarioTrabalho(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaEspecialidade getlistaEspecialidade(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaEspecialidade & listaEspecialidade(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaAgendamento getlistaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaAgendamento & listaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaTratamento getlistaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaTratamento & listaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar(QList<long> lista_horarios_removidos);
   QString inativar();
   void load();
   bool estaNoHorarioDeTrabalho(QDateTime horario, int duracao);
   type_listaAgendamento getlistaAgendamentoDaSemana(QDate segundaFeira);
   type_listaAgendamento getlistaAgendamentoDoDia(QDate dia);
   type_listaHorarioTrabalho getlistaHorarioTrabalhoOrdenadoPorDiaHoraInicio();

   static qx::QxCollection<long, QSharedPointer<clinica::dentista> > retornaTodosDentistasOrdenadosPorNome() {
       qx::QxSqlQuery query("ORDER BY dentista.nome");
       qx::QxCollection<long, QSharedPointer<clinica::dentista> > lista_dentistas;
       QSqlError daoError = qx::dao::fetch_by_query(query, lista_dentistas);
       if(daoError.isValid()) {
           lista_dentistas.clear();
       }
       return lista_dentistas;
   }


public:

   static QString relation_listaHorarioTrabalho() { return "listaHorarioTrabalho"; }
   static QString relation_listaEspecialidade() { return "listaEspecialidade"; }
   static QString relation_listaAgendamento() { return "listaAgendamento"; }
   static QString relation_listaTratamento() { return "listaTratamento"; }


public:

   static QString column_id() { return "dentista_id"; }
   static QString column_nome() { return "nome"; }
   static QString column_registroCro() { return "registro_cro"; }
   static QString column_duracaoConsulta() { return "duracao_consulta"; }
   static QString column_percentualComissao() { return "percentual_comissao"; }
   static QString column_inativo() { return "inativo"; }

public:

   static QString table_name() { return "dentista"; }

};

typedef QSharedPointer<dentista> dentista_ptr;
typedef qx::QxCollection<long, dentista_ptr> list_of_dentista;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::dentista, QObject, 0, clinica_dentista)

#include "clinica_horarioTrabalho.h"
#include "clinica_especialidade.h"
#include "clinica_agendamento.h"
#include "clinica_tratamento.h"

#endif // _CLINICA_DENTISTA_H_
