#ifndef _CLINICA_ITEM_TRATAMENTO_H_
#define _CLINICA_ITEM_TRATAMENTO_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class procedimento;
} // namespace clinica
namespace clinica {
class tratamento;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT item_tratamento : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::item_tratamento)

   QX_PERSISTABLE_HPP(clinica::item_tratamento)

public:

   typedef QSharedPointer<clinica::procedimento> type_procedimento;
   typedef QSharedPointer<clinica::tratamento> type_tratamento;

protected:

   long m_item_tratamento_id;
   long m_ordem;
   long m_quantidade;
   double m_valor_unitario;
   QString m_realizacao;
   type_procedimento m_procedimento;
   type_tratamento m_tratamento;

public:

   item_tratamento();
   item_tratamento(const long & id);
   virtual ~item_tratamento();

   long getitem_tratamento_id() const;
   long getordem() const;
   long getquantidade() const;
   double getvalor_unitario() const;
   QString getrealizacao() const;
   type_procedimento getprocedimento() const;
   type_tratamento gettratamento() const;

   void setitem_tratamento_id(const long & val);
   void setordem(const long & val);
   void setquantidade(const long & val);
   void setvalor_unitario(const double & val);
   void setrealizacao(const QString & val);
   void setprocedimento(const type_procedimento & val);
   void settratamento(const type_tratamento & val);

   type_procedimento getprocedimento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_tratamento gettratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar();
   QString excluir();
public:

   static QString relation_procedimento() { return "procedimento_id"; }
   static QString relation_tratamento() { return "tratamento_id"; }

public:

   static QString column_item_tratamento_id() { return "item_tratamento_id"; }
   static QString column_ordem() { return "ordem"; }
   static QString column_quantidade() { return "quantidade"; }
   static QString column_valor_unitario() { return "valor_unitario"; }
   static QString column_realizacao() { return "realizacao"; }

public:

   static QString table_name() { return "item_tratamento"; }

};

typedef QSharedPointer<item_tratamento> item_tratamento_ptr;
typedef qx::QxCollection<long, item_tratamento_ptr> list_of_item_tratamento;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::item_tratamento, QObject, 0, clinica_item_tratamento)

#include "clinica_procedimento.h"
#include "clinica_tratamento.h"

#endif // _CLINICA_ITEM_TRATAMENTO_H_
