#ifndef _CLINICA_HORARIOTRABALHO_H_
#define _CLINICA_HORARIOTRABALHO_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class dentista;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT horarioTrabalho : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::horarioTrabalho)

   QX_PERSISTABLE_HPP(clinica::horarioTrabalho)

public:

   typedef QSharedPointer<clinica::dentista> type_dentista;

protected:

   long m_id;
   int m_diaDaSemana;
   QTime m_horarioInicio;
   QTime m_horarioTermino;
   type_dentista m_dentista;

public:

   horarioTrabalho();
   horarioTrabalho(const long & id);
   virtual ~horarioTrabalho();

   long getid() const;
   int getdiaDaSemana() const;
   QTime gethorarioInicio() const;
   QTime gethorarioTermino() const;
   type_dentista getdentista() const;

   void setid(const long & val);
   void setdiaDaSemana(const int & val);
   void sethorarioInicio(const QTime & val);
   void sethorarioTermino(const QTime & val);
   void setdentista(const type_dentista & val);

   type_dentista getdentista(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

public:

   static QString relation_dentista() { return "dentista_id"; }

public:

   static QString column_id() { return "horario_trabalho_id"; }
   static QString column_diaDaSemana() { return "dia_da_semana"; }
   static QString column_horarioInicio() { return "horario_inicio"; }
   static QString column_horarioTermino() { return "horario_termino"; }

public:

   static QString table_name() { return "horario_trabalho"; }

};

typedef QSharedPointer<horarioTrabalho> horarioTrabalho_ptr;
typedef qx::QxCollection<long, horarioTrabalho_ptr> list_of_horarioTrabalho;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::horarioTrabalho, QObject, 0, clinica_horarioTrabalho)

#include "clinica_dentista.h"

#endif // _CLINICA_HORARIOTRABALHO_H_
