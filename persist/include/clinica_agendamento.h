#ifndef _CLINICA_AGENDAMENTO_H_
#define _CLINICA_AGENDAMENTO_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class cliente;
} // namespace clinica
namespace clinica {
class dentista;
} // namespace clinica
namespace clinica {
class especialidade;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT agendamento : public QObject, public qx::IxPersistable
{

    Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::agendamento)

   QX_PERSISTABLE_HPP(clinica::agendamento)

public:

   typedef QSharedPointer<clinica::cliente> type_cliente_id;
   typedef QSharedPointer<clinica::dentista> type_dentista_id;
   typedef QSharedPointer<clinica::especialidade> type_especialidade_id;

protected:

   long m_agendamento_id;
   QDateTime m_data_hora;
   long m_duracao;
   QString m_observacao;
   bool m_comparecimento;
   type_cliente_id m_cliente_id;
   type_dentista_id m_dentista_id;
   type_especialidade_id m_especialidade_id;
   QString m_nome_paciente;

public:

   agendamento();
   agendamento(const long & id);
   virtual ~agendamento();

   long getagendamentoId() const;
   QDateTime getdataHora() const;
   long getduracao() const;
   QString getobservacao() const;
   bool getcomparecimento() const;
   type_cliente_id getclienteId() const;
   type_dentista_id getdentistaId() const;
   type_especialidade_id getespecialidadeId() const;
   QString getnome_paciente() const;

   QColor getCorFundo();

   void setagendamentoId(const long & val);
   void setdataHora(const QDateTime & val);
   void setduracao(const long & val);
   void setobservacao(const QString & val);
   void setcomparecimento(const bool & val);
   void setclienteId(const type_cliente_id & val);
   void setdentistaId(const type_dentista_id & val);
   void setespecialidadeId(const type_especialidade_id & val);
   void setnome_paciente(const QString & val);

   type_cliente_id getclienteId(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_dentista_id getdentistaId(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_especialidade_id getespecialidadeId(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar();
   QString apagar();

   static QSharedPointer<agendamento> recuperar(long id) {
       QSharedPointer<agendamento> agendamento;
       agendamento.reset(new clinica::agendamento(id));
       qx::dao::fetch_by_id_with_all_relation(agendamento);
       return agendamento;
   }

public:

   static QString relation_clienteId() { return "cliente_id"; }
   static QString relation_dentistaId() { return "dentista_id"; }
   static QString relation_especialidadeId() { return "especialidade_id"; }

public:

   static QString column_agendamentoId() { return "agendamento_id"; }
   static QString column_dataHora() { return "data_hora"; }
   static QString column_duracao() { return "duracao"; }
   static QString column_observacao() { return "observacao"; }
   static QString column_comparecimento() { return "comparecimento"; }
   static QString column_nome_paciente() { return "nome_paciente"; }

public:

   static QString table_name() { return "agendamento"; }

};

typedef QSharedPointer<agendamento> agendamento_ptr;
typedef qx::QxCollection<long, agendamento_ptr> list_of_agendamento;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::agendamento, QObject, 0, clinica_agendamento)

#include "clinica_cliente.h"
#include "clinica_dentista.h"
#include "clinica_especialidade.h"

#endif // _CLIENTE_AGENTAMENTO_DENTISTA_CLINICA_AGENDAMENTO_H_
