#ifndef _CLINICA_PROCEDIMENTO_H_
#define _CLINICA_PROCEDIMENTO_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {

class CLINICA_EXPORT procedimento : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::procedimento)

   QX_PERSISTABLE_HPP(clinica::procedimento)

protected:

   long m_procedimento_id;
   QString m_descricao;

public:

   procedimento();
   procedimento(const long & id);
   virtual ~procedimento();

   long getprocedimento_id() const;
   QString getdescricao() const;

   void setprocedimento_id(const long & val);
   void setdescricao(const QString & val);

   QString salvar();
   QString inativar();

   static qx::QxCollection<long, QSharedPointer<clinica::procedimento> > retornaTodosProcedimentosOrdenadosPorNome() {
       qx::QxSqlQuery query("ORDER BY procedimento.descricao");
       qx::QxCollection<long, QSharedPointer<clinica::procedimento> > lista_procedimentos;
       QSqlError daoError = qx::dao::fetch_by_query(query, lista_procedimentos);
       if(daoError.isValid()) {
           lista_procedimentos.clear();
       }
       return lista_procedimentos;
   }

public:

   static QString column_procedimento_id() { return "procedimento_id"; }
   static QString column_descricao() { return "descricao"; }

public:

   static QString table_name() { return "procedimento"; }

};

typedef QSharedPointer<procedimento> procedimento_ptr;
typedef qx::QxCollection<long, procedimento_ptr> list_of_procedimento;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::procedimento, QObject, 0, clinica_procedimento)

#endif // _CLINICA_PROCEDIMENTO_H_
