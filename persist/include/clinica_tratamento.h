#ifndef _CLINICA_TRATAMENTO_H_
#define _CLINICA_TRATAMENTO_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class pagamento;
} // namespace clinica
namespace clinica {
class cliente;
} // namespace clinica
namespace clinica {
class dentista;
} // namespace clinica
namespace clinica {
class item_tratamento;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT tratamento : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::tratamento)

   QX_PERSISTABLE_HPP(clinica::tratamento)

public:

   typedef qx::QxCollection<long, QSharedPointer<clinica::pagamento> > type_listaPagamento;
   typedef qx::QxCollection<long, QSharedPointer<clinica::item_tratamento> > type_listaItemTratamento;
   typedef QSharedPointer<clinica::cliente> type_cliente;
   typedef QSharedPointer<clinica::dentista> type_dentista;
   struct detalhe_tratamento_devedor {
       QString nome_dentista;
       QString nome_cliente;
       QString data_inicio;
       QString valor_tratamento;
       QString valor_pagto;
       QString saldo_devedor;
   };

protected:

   long m_tratamento_id;
   QDate m_data_inicio;
   QDate m_data_termino;
   type_listaPagamento m_listaPagamento;
   type_listaItemTratamento m_listaItemTratamento;
   type_cliente m_cliente;
   type_dentista m_dentista;

public:

   tratamento();
   tratamento(const long & id);
   virtual ~tratamento();

   long gettratamento_id() const;
   QDate getdata_inicio() const;
   QDate getdata_termino() const;
   type_listaPagamento getlistaPagamento() const;
   type_listaPagamento & listaPagamento();
   const type_listaPagamento & listaPagamento() const;
   type_listaItemTratamento getlistaItemTratamento() const;
   type_listaItemTratamento & listaItemTratamento();
   const type_listaItemTratamento & listaItemTratamento() const;
   type_cliente getcliente() const;
   type_dentista getdentista() const;

   void settratamento_id(const long & val);
   void setdata_inicio(const QDate & val);
   void setdata_termino(const QDate & val);
   void setlistaPagamento(const type_listaPagamento & val);
   void setlistaItemTratamento(const type_listaItemTratamento & val);
   void setcliente(const type_cliente & val);
   void setdentista(const type_dentista & val);

   type_listaPagamento getlistaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaPagamento & listaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaItemTratamento getlistaItemTratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaItemTratamento & listaItemTratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_cliente getcliente(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_dentista getdentista(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   type_dentista getDentistaMesmoInativo();
   type_listaItemTratamento getlistaItemTratamentoOrdenadaPelaOrdem();
   type_listaPagamento getlistaPagamentoOrdenadaPelaData();
   QString salvarComListaItemTratamento();
   QString salvarSemListaItemTratamento();

   static QSharedPointer<clinica::tratamento> retornaPeloID(long tratamento_id) {
       QSharedPointer<clinica::tratamento> tratamento;
       tratamento.reset(new clinica::tratamento());
       tratamento->settratamento_id(tratamento_id);
       QSqlError daoError = qx::dao::fetch_by_id(tratamento);
       if(daoError.isValid()) {
           tratamento.reset(new clinica::tratamento());
       }
       return tratamento;
   }
   long retornaProximaOrdemLivre();
   QList<detalhe_tratamento_devedor> retornaListaTratamentoDevedor();

public:

   static QString relation_listaPagamento() { return "listaPagamento"; }
   static QString relation_listaItemTratamento() { return "listaItemTratamento"; }
   static QString relation_cliente() { return "cliente_id"; }
   static QString relation_dentista() { return "dentista_id"; }

public:

   static QString column_tratamento_id() { return "tratamento_id"; }
   static QString column_data_inicio() { return "data_inicio"; }
   static QString column_data_termino() { return "data_termino"; }

public:

   static QString table_name() { return "tratamento"; }

};

typedef QSharedPointer<tratamento> tratamento_ptr;
typedef qx::QxCollection<long, tratamento_ptr> list_of_tratamento;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::tratamento, QObject, 0, clinica_tratamento)

#include "clinica_pagamento.h"
#include "clinica_cliente.h"
#include "clinica_dentista.h"
#include "clinica_item_tratamento.h"

#endif // _CLINICA_TRATAMENTO_H_
