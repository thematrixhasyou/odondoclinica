#ifndef _CLINICA_CAIXA_H_
#define _CLINICA_CAIXA_H_

#include <QxOrm.h>
#include <QSqlQueryModel>
#include "persist/include/clinica_export.h"

namespace clinica {
class pagamento;
class retirada;
}

namespace clinica {

class CLINICA_EXPORT caixa : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::caixa)

   QX_PERSISTABLE_HPP(clinica::caixa)

public:

   typedef qx::QxCollection<long, QSharedPointer<clinica::pagamento> > type_listaPagamento;
   typedef qx::QxCollection<long, QSharedPointer<clinica::retirada> > type_listaRetirada;
   typedef qx::QxCollection<QString, QString> type_listaTotalizacao;
   struct detalhe_pagamento {
       QString nome_dentista;
       QString nome_cliente;
       QString tipo_pagamento;
       QString valor;
       QString data_realizacao;
       QString data_efetivacao;
       QString bandeira_cartao;
       QString autorizacao_cartao;
   };
   typedef QList<detalhe_pagamento> type_listaDetalhePagamento;

protected:

   long m_caixa_id;
   QDate m_dataMovimento;
   QDateTime m_dataHoraFechamento;
   double m_trocoInicial;
   type_listaPagamento m_listaPagamento;
   type_listaRetirada m_listaRetirada;

public:

   caixa();
   caixa(const long & id);
   virtual ~caixa();

   long getcaixa_id() const;
   QDate getdata_movimento() const;
   QDateTime getdata_hora_fechamento() const;
   double gettroco_inicial() const;
   type_listaPagamento getlistaPagamento() const;
   type_listaPagamento & listaPagamento();
   const type_listaPagamento & listaPagamento() const;
   type_listaRetirada getlistaRetirada() const;
   type_listaRetirada & listaRetirada();
   const type_listaRetirada & listaRetirada() const;

   void setcaixa_id(const long id);
   void setdata_movimento(const QDate & val);
   void setdata_hora_fechamento(const QDateTime & val);
   void settroco_inicial(const double & val);
   void setlistaPagamento(const type_listaPagamento & val);
   void setlistaRetirada(const type_listaRetirada & val);

   type_listaPagamento getlistaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaPagamento & listaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaRetirada getlistaRetirada(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaRetirada & listaRetirada(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar();
   QSharedPointer<caixa> retornaUltimoMovimento();
   bool isMovimentoAberto(QDate data_movimento);
   QSharedPointer<caixa> retornaMovimento(QDate data_movimento);
   type_listaTotalizacao retornaPagamentosTotalizados();
   type_listaTotalizacao retornaRetiradasTotalizadas();
   type_listaDetalhePagamento retornaListaDetalhePagamento();

public:

   static QString relation_listaPagamento() { return "listaPagamento"; }
   static QString relation_listaRetirada() { return "listaRetirada"; }

public:

   static QString column_caixa_id() { return "caixa_id"; }
   static QString column_dataMovimento() { return "data_movimento"; }
   static QString column_dataHoraFechamento() { return "data_hora_fechamento"; }
   static QString column_trocoInicial() { return "troco_inicial"; }

public:

   static QString table_name() { return "caixa"; }
};

typedef QSharedPointer<caixa> caixa_ptr;
typedef qx::QxCollection<QDate, caixa_ptr> list_of_caixa;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::caixa, QObject, 0, clinica_caixa)

#include "clinica_pagamento.h"
#include "clinica_retirada.h"

#endif // _CLINICA_CAIXA_H_
