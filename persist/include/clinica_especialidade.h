#ifndef _CLINICA_ESPECIALIDADE_H_
#define _CLINICA_ESPECIALIDADE_H_

#include <QxOrm.h>
#include "persist/include/clinica_export.h"

namespace clinica {
class dentista;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT especialidade : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::especialidade)

   QX_PERSISTABLE_HPP(clinica::especialidade)

public:

   typedef qx::QxCollection<long, QSharedPointer<clinica::dentista> > type_listaDentista;

protected:

   long m_id;
   QString m_descricao;
   type_listaDentista m_listaDentista;

public:

   especialidade();
   especialidade(const long & id);
   virtual ~especialidade();

   long getid() const;
   QString getdescricao() const;
   type_listaDentista getlistaDentista() const;
   type_listaDentista & listaDentista();
   const type_listaDentista & listaDentista() const;

   void setid(const long & val);
   void setdescricao(const QString & val);
   void setlistaDentista(const type_listaDentista & val);

   type_listaDentista getlistaDentista(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_listaDentista & listaDentista(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   static qx::QxCollection<long, QSharedPointer<clinica::especialidade> > retornaTodasEspecialidadesOrdenadasPorDescricao() {
       qx::QxSqlQuery query("ORDER BY especialidade.descricao");
       qx::QxCollection<long, QSharedPointer<clinica::especialidade> > lista_especialidade;
       QSqlError daoError = qx::dao::fetch_by_query(query, lista_especialidade);
       if(daoError.isValid()) {
           lista_especialidade.clear();
       }
       return lista_especialidade;
   }

public:

   static QString relation_listaDentista() { return "listaDentista"; }

public:

   static QString column_id() { return "especialidade_id"; }
   static QString column_descricao() { return "descricao"; }

public:

   static QString table_name() { return "especialidade"; }

};

typedef QSharedPointer<especialidade> especialidade_ptr;
typedef qx::QxCollection<long, especialidade_ptr> list_of_especialidade;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::especialidade, QObject, 0, clinica_especialidade)

#include "clinica_dentista.h"

#endif // _CLINICA_ESPECIALIDADE_H_
