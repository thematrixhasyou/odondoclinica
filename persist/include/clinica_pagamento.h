#ifndef _CLINICA_PAGAMENTO_H_
#define _CLINICA_PAGAMENTO_H_

#include <QxOrm.h>
#include "persist/include/clinica_caixa.h"
#include "persist/include/clinica_export.h"

namespace clinica {
class tratamento;
class caixa;
} // namespace clinica

namespace clinica {

class CLINICA_EXPORT pagamento : public QObject, public qx::IxPersistable
{

   Q_OBJECT
   QX_REGISTER_FRIEND_CLASS(clinica::pagamento)

   QX_PERSISTABLE_HPP(clinica::pagamento)

public:

   typedef QSharedPointer<clinica::tratamento> type_tratamento;
   typedef QSharedPointer<clinica::caixa> type_caixa;
   struct detalhe_fechamento_periodo {
       QString nome_dentista;
       QString producao_dinheiro;
       QString producao_cheque;
       QString producao_credito;
       QString producao_debito;
       QString producao_total;
       QString fat_corr_dinheiro;
       QString fat_corr_chegue;
       QString fat_corr_debito;
       QString fat_corr_credito;
       QString fat_corr_total;
       QString fat_corr_comissao;
       QString fat_fut_credito;
       QString fat_fut_debito;
       QString fat_fut_total;
   };

protected:

   long m_pagamento_id;
   double m_valor;
   QString m_tipo;
   QDate m_data_realizacao;
   QDate m_data_efetivacao;
   QString m_cc_bandeira;
   QString m_cc_final_numero;
   QString m_cc_autorizacao;
   type_tratamento m_tratamento;
   type_caixa m_caixa;

public:

   pagamento();
   pagamento(const long & id);
   virtual ~pagamento();

   long getpagamento_id() const;
   double getvalor() const;
   QString gettipo() const;
   QString getcc_bandeira() const;
   QString getcc_final_numero() const;
   QString getcc_autorizacao() const;
   QDate getdata_realizacao() const;
   QDate getdata_efetivacao() const;
   type_tratamento gettratamento() const;
   type_caixa getCaixa() const;

   void setpagamento_id(const long & val);
   void setvalor(const double & val);
   void settipo(const QString & val);
   void setcc_bandeira(const QString & val);
   void setcc_final_numero(const QString & val);
   void setcc_autorizacao(const QString & val);
   void setdata_realizacao(const QDate & val);
   void setdata_efetivacao(const QDate & val);
   void settratamento(const type_tratamento & val);
   void setCaixa(const type_caixa & val);

   type_tratamento gettratamento(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);
   type_caixa getCaixa(bool bLoadFromDatabase, const QString & sAppendRelations = QString(), QSqlDatabase * pDatabase = NULL, QSqlError * pDaoError = NULL);

   QString salvar();
   QString excluir();

   QList<detalhe_fechamento_periodo> retornaFechamentoPeriodo(QDate data_inicio, QDate data_fim);
   QList<caixa::detalhe_pagamento> retornaDetalheProducaoPeriodo(QDate data_inicio, QDate data_fim);
   QList<caixa::detalhe_pagamento> retornaDetalheFaturamentoPeriodo(QDate data_inicio, QDate data_fim);
   QList<caixa::detalhe_pagamento> retornaDetalheFaturamentoFuturo(QDate data_corte);

public:

   static QString relation_tratamento() { return "tratamento_id"; }
   static QString relation_caixa() { return "caixa_id"; }

public:

   static QString column_pagamento_id() { return "pagamento_id"; }
   static QString column_valor() { return "valor"; }
   static QString column_tipo() { return "tipo"; }
   static QString column_cc_bandeira() { return "cc_bandeira"; }
   static QString column_cc_final_numero() { return "cc_final_numero"; }
   static QString column_cc_autorizacao() { return "cc_autorizacao"; }
   static QString column_data_realizacao() { return "data_realizacao"; }
   static QString column_data_efetivacao() { return "data_efetivacao"; }

public:

   static QString table_name() { return "pagamento"; }

};

typedef QSharedPointer<pagamento> pagamento_ptr;
typedef qx::QxCollection<long, pagamento_ptr> list_of_pagamento;

} // namespace clinica

QX_REGISTER_COMPLEX_CLASS_NAME_HPP_CLINICA(clinica::pagamento, QObject, 0, clinica_pagamento)

#include "clinica_tratamento.h"
#include "clinica_caixa.h"

#endif // _CLINICA_PAGAMENTO_H_
