
#include "../include/clinica_plano_odontologico.h"
#include "../include/clinica_cliente.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::plano_odontologico, clinica_plano_odontologico)

namespace qx {

template <>
void register_class(QxClass<clinica::plano_odontologico> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("plano_odontologico");

   pData = t.id(& clinica::plano_odontologico::m_plano_odontologico_id, "plano_odontologico_id", 0);

   pData = t.data(& clinica::plano_odontologico::m_nome, "nome", 0, true, true);
   pData = t.data(& clinica::plano_odontologico::m_observacao, "observacao", 0, true, true);

   pRelation = t.relationOneToMany(& clinica::plano_odontologico::m_list_of_cliente, "list_of_cliente", "plano_odontologico_id", 0);

   qx::QxValidatorX<clinica::plano_odontologico> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(plano_odontologico)

plano_odontologico::plano_odontologico() : m_plano_odontologico_id(0) { ; }

plano_odontologico::plano_odontologico(const long & id) : m_plano_odontologico_id(id) { ; }

plano_odontologico::~plano_odontologico() { ; }

long plano_odontologico::getplano_odontologico_id() const { return m_plano_odontologico_id; }

QString plano_odontologico::getnome() const { return m_nome; }

QString plano_odontologico::getobservacao() const { return m_observacao; }

plano_odontologico::type_list_of_cliente plano_odontologico::getlist_of_cliente() const { return m_list_of_cliente; }

plano_odontologico::type_list_of_cliente & plano_odontologico::list_of_cliente() { return m_list_of_cliente; }

const plano_odontologico::type_list_of_cliente & plano_odontologico::list_of_cliente() const { return m_list_of_cliente; }

void plano_odontologico::setplano_odontologico_id(const long & val) { m_plano_odontologico_id = val; }

void plano_odontologico::setnome(const QString & val) { m_nome = val; }

void plano_odontologico::setobservacao(const QString & val) { m_observacao = val; }

void plano_odontologico::setlist_of_cliente(const plano_odontologico::type_list_of_cliente & val) { m_list_of_cliente = val; }

plano_odontologico::type_list_of_cliente plano_odontologico::getlist_of_cliente(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlist_of_cliente(); }
   QString sRelation = "list_of_cliente";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::plano_odontologico tmp;
   tmp.m_plano_odontologico_id = this->m_plano_odontologico_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_list_of_cliente = tmp.m_list_of_cliente; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_list_of_cliente;
}

plano_odontologico::type_list_of_cliente & plano_odontologico::list_of_cliente(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return list_of_cliente(); }
   QString sRelation = "list_of_cliente";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::plano_odontologico tmp;
   tmp.m_plano_odontologico_id = this->m_plano_odontologico_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_list_of_cliente = tmp.m_list_of_cliente; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_list_of_cliente;
}

QString plano_odontologico::salvar() {
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QString plano_odontologico::inativar() {
    QString retorno;
    QSqlError daoError = qx::dao::delete_by_id(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}


} // namespace clinica
