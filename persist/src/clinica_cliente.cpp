#include "../include/clinica_cliente.h"
#include "../include/clinica_plano_odontologico.h"
#include "../include/clinica_tratamento.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::cliente, clinica_cliente)

namespace qx {

template <>
void register_class(QxClass<clinica::cliente> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("cliente");
   t.setSoftDelete(qx::QxSoftDelete("inativo"));
   pData = t.id(& clinica::cliente::m_cliente_id, "cliente_id", 0);

   pData = t.data(& clinica::cliente::m_inativo, "inativo", 0, true, true);
   pData = t.data(& clinica::cliente::m_nome, "nome", 0, true, true);
   pData = t.data(& clinica::cliente::m_doc_identidade, "doc_identidade", 0, true, true);
   pData = t.data(& clinica::cliente::m_cpf, "cpf", 0, true, true);
   pData = t.data(& clinica::cliente::m_genero, "genero", 0, true, true);
   pData = t.data(& clinica::cliente::m_profissao, "profissao", 0, true, true);
   pData = t.data(& clinica::cliente::m_cep, "cep", 0, true, true);
   pData = t.data(& clinica::cliente::m_logradouro, "logradouro", 0, true, true);
   pData = t.data(& clinica::cliente::m_num_endereco, "num_endereco", 0, true, true);
   pData = t.data(& clinica::cliente::m_compl_endereco, "compl_endereco", 0, true, true);
   pData = t.data(& clinica::cliente::m_bairro, "bairro", 0, true, true);
   pData = t.data(& clinica::cliente::m_cidade, "cidade", 0, true, true);
   pData = t.data(& clinica::cliente::m_uf, "uf", 0, true, true);
   pData = t.data(& clinica::cliente::m_ddd_celular, "ddd_celular", 0, true, true);
   pData = t.data(& clinica::cliente::m_tel_celular, "tel_celular", 0, true, true);
   pData = t.data(& clinica::cliente::m_ddd_fixo, "ddd_fixo", 0, true, true);
   pData = t.data(& clinica::cliente::m_tel_fixo, "tel_fixo", 0, true, true);
   pData = t.data(& clinica::cliente::m_observacao, "observacao", 0, true, true);

   pRelation = t.relationManyToOne(& clinica::cliente::m_plano_odontologico, "plano_odontologico_id", 0);
   pRelation = t.relationOneToMany(& clinica::cliente::m_listaAgendamento, "listaAgendamento", "cliente_id", 0);
   pRelation = t.relationOneToMany(& clinica::cliente::m_listaTratamento, "listaTratamento", "cliente_id", 0);

}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(cliente)

cliente::cliente() : m_cliente_id(0) { ; }

cliente::cliente(const long & id) : m_cliente_id(id) { ; }

cliente::~cliente() { ; }

long cliente::getcliente_id() const { return m_cliente_id; }

QDateTime cliente::getinativo() const { return m_inativo; }

QString cliente::getnome() const { return m_nome; }

QString cliente::getdoc_identidade() const { return m_doc_identidade; }

QString cliente::getcpf() const { return m_cpf; }

QString cliente::getgenero() const { return m_genero; }

QString cliente::getprofissao() const { return m_profissao; }

QString cliente::getcep() const { return m_cep; }

QString cliente::getlogradouro() const { return m_logradouro; }

QString cliente::getnum_endereco() const { return m_num_endereco; }

QString cliente::getcompl_endereco() const { return m_compl_endereco; }

QString cliente::getbairro() const { return m_bairro; }

QString cliente::getcidade() const { return m_cidade; }

QString cliente::getuf() const { return m_uf; }

QString cliente::getddd_celular() const { return m_ddd_celular; }

QString cliente::gettel_celular() const { return m_tel_celular; }

QString cliente::getddd_fixo() const { return m_ddd_fixo; }

QString cliente::gettel_fixo() const { return m_tel_fixo; }

QString cliente::getobservacao() const { return m_observacao; }

cliente::type_plano_odontologico cliente::getplano_odontologico() const { return m_plano_odontologico; }

cliente::type_listaAgendamento cliente::getlistaAgendamento() const { return m_listaAgendamento; }

cliente::type_listaAgendamento & cliente::listaAgendamento() { return m_listaAgendamento; }

const cliente::type_listaAgendamento & cliente::listaAgendamento() const { return m_listaAgendamento; }

cliente::type_listaTratamento cliente::getlistaTratamento() const { return m_listaTratamento; }

cliente::type_listaTratamento & cliente::listaTratamento() { return m_listaTratamento; }

const cliente::type_listaTratamento & cliente::listaTratamento() const { return m_listaTratamento; }


void cliente::setcliente_id(const long & val) { m_cliente_id = val; }

void cliente::setinativo(const QDateTime & val) { m_inativo = val; }

void cliente::setnome(const QString & val) { m_nome = val; }

void cliente::setdoc_identidade(const QString & val) { m_doc_identidade = val; }

void cliente::setcpf(const QString & val) { m_cpf = val; }

void cliente::setgenero(const QString & val) { m_genero = val; }

void cliente::setprofissao(const QString & val) { m_profissao = val; }

void cliente::setcep(const QString & val) { m_cep = val; }

void cliente::setlogradouro(const QString & val) { m_logradouro = val; }

void cliente::setnum_endereco(const QString & val) { m_num_endereco = val; }

void cliente::setcompl_endereco(const QString & val) { m_compl_endereco = val; }

void cliente::setbairro(const QString & val) { m_bairro = val; }

void cliente::setcidade(const QString & val) { m_cidade = val; }

void cliente::setuf(const QString & val) { m_uf = val; }

void cliente::setddd_celular(const QString & val) { m_ddd_celular = val; }

void cliente::settel_celular(const QString & val) { m_tel_celular = val; }

void cliente::setddd_fixo(const QString & val) { m_ddd_fixo = val; }

void cliente::settel_fixo(const QString & val) { m_tel_fixo = val; }

void cliente::setobservacao(const QString & val) { m_observacao = val; }

void cliente::setplano_odontologico(const cliente::type_plano_odontologico & val) { m_plano_odontologico = val; }

void cliente::setlistaAgendamento(const cliente::type_listaAgendamento & val) { m_listaAgendamento = val; }

void cliente::setlistaTratamento(const cliente::type_listaTratamento & val) { m_listaTratamento = val; }

cliente::type_plano_odontologico cliente::getplano_odontologico(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getplano_odontologico(); }
   QString sRelation = "plano_odontologico_id";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::cliente tmp;
   tmp.m_cliente_id = this->m_cliente_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_plano_odontologico = tmp.m_plano_odontologico; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_plano_odontologico;
}

cliente::type_listaAgendamento cliente::getlistaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaAgendamento(); }
   QString sRelation = "listaAgendamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::cliente tmp;
   tmp.m_cliente_id = this->m_cliente_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaAgendamento = tmp.m_listaAgendamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaAgendamento;
}

cliente::type_listaAgendamento & cliente::listaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaAgendamento(); }
   QString sRelation = "listaAgendamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::cliente tmp;
   tmp.m_cliente_id = this->m_cliente_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaAgendamento = tmp.m_listaAgendamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaAgendamento;
}

cliente::type_listaTratamento cliente::getlistaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaTratamento(); }
   QString sRelation = "listaTratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::cliente tmp;
   tmp.m_cliente_id = this->m_cliente_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaTratamento = tmp.m_listaTratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaTratamento;
}

cliente::type_listaTratamento & cliente::listaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaTratamento(); }
   QString sRelation = "listaTratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::cliente tmp;
   tmp.m_cliente_id = this->m_cliente_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaTratamento = tmp.m_listaTratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaTratamento;
}

QString cliente::salvar() {
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QString cliente::inativar() {
    QString retorno;
    QSqlError daoError = qx::dao::delete_by_id(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

void cliente::load()
{
    qx::dao::fetch_by_id(*this);
}

cliente::type_listaTratamento cliente::getlistaTratamentoOrndenadoPorDataInicioDescDentistaAsc()
{
    cliente::type_listaTratamento tratamentos;

    qx::QxSqlQuery query("SELECT tratamento.tratamento_id, tratamento.data_inicio, tratamento.data_termino, tratamento.cliente_id AS cliente_id, tratamento.dentista_id as dentista_id, dentista.nome \
                         FROM tratamento INNER JOIN dentista ON tratamento.dentista_id = dentista.dentista_id \
                         WHERE tratamento.cliente_id = :cliente_id \
                         ORDER BY YEAR(tratamento.data_inicio) DESC, dentista.nome, tratamento.data_inicio DESC");
    query.bind(":cliente_id", QVariant::fromValue(this->m_cliente_id));

    QSqlError daoError = qx::dao::execute_query(query, tratamentos);
    if(daoError.isValid()) {
        tratamentos.clear();
    }
    return tratamentos;
}

} // namespace clinica

