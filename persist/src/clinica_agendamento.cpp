#include "../include/clinica_agendamento.h"
#include "../include/clinica_cliente.h"
#include "../include/clinica_dentista.h"
#include "../include/clinica_especialidade.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>
#include <QColor>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::agendamento, clinica_agendamento)

namespace qx {

template <>
void register_class(QxClass<clinica::agendamento> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("agendamento");

   pData = t.id(& clinica::agendamento::m_agendamento_id, "agendamento_id", 0);

   pData = t.data(& clinica::agendamento::m_data_hora, "data_hora", 0, true, true);
   pData = t.data(& clinica::agendamento::m_duracao, "duracao", 0, true, true);
   pData = t.data(& clinica::agendamento::m_observacao, "observacao", 0, true, true);
   pData = t.data(& clinica::agendamento::m_comparecimento, "comparecimento", 0, true, true);
   pData = t.data(& clinica::agendamento::m_nome_paciente, "nome_paciente", 0, true, true);

   pRelation = t.relationManyToOne(& clinica::agendamento::m_cliente_id, "cliente_id", 0);
   pRelation = t.relationManyToOne(& clinica::agendamento::m_dentista_id, "dentista_id", 0);
   pRelation = t.relationManyToOne(& clinica::agendamento::m_especialidade_id, "especialidade_id", 0);

   qx::QxValidatorX<clinica::agendamento> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(agendamento)

agendamento::agendamento() : m_agendamento_id(0), m_comparecimento(false) { ; }

agendamento::agendamento(const long & id) : m_agendamento_id(id), m_comparecimento(false) { ; }

agendamento::~agendamento() { ; }

long agendamento::getagendamentoId() const { return m_agendamento_id; }

QDateTime agendamento::getdataHora() const { return m_data_hora; }

long agendamento::getduracao() const { return m_duracao; }

QString agendamento::getobservacao() const { return m_observacao; }

bool agendamento::getcomparecimento() const { return m_comparecimento; }

agendamento::type_cliente_id agendamento::getclienteId() const { return m_cliente_id; }

agendamento::type_dentista_id agendamento::getdentistaId() const { return m_dentista_id; }

agendamento::type_especialidade_id agendamento::getespecialidadeId() const { return m_especialidade_id; }

QString agendamento::getnome_paciente() const { return m_nome_paciente; }

QColor agendamento::getCorFundo()
{
    if(getcomparecimento()) {
        return QColor(0,255,0);
    } else if(getdataHora() < QDateTime::currentDateTime()) {
        return QColor(255,0,0);
    } else {
        return QColor(0,0,255);
    }
}

void agendamento::setagendamentoId(const long & val) { m_agendamento_id = val; }

void agendamento::setdataHora(const QDateTime & val) { m_data_hora = val; }

void agendamento::setduracao(const long &val) { m_duracao = val; }

void agendamento::setobservacao(const QString & val) { m_observacao = val; }

void agendamento::setcomparecimento(const bool & val) { m_comparecimento = val; }

void agendamento::setclienteId(const agendamento::type_cliente_id & val) { m_cliente_id = val; }

void agendamento::setdentistaId(const agendamento::type_dentista_id & val) { m_dentista_id = val; }

void agendamento::setespecialidadeId(const agendamento::type_especialidade_id & val) { m_especialidade_id = val; }

void agendamento::setnome_paciente(const QString &val) { m_nome_paciente = val; }

agendamento::type_cliente_id agendamento::getclienteId(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getclienteId(); }
   QString sRelation = "cliente_id";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::agendamento tmp;
   tmp.m_agendamento_id = this->m_agendamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_cliente_id = tmp.m_cliente_id; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_cliente_id;
}

agendamento::type_dentista_id agendamento::getdentistaId(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getdentistaId(); }
   QString sRelation = "dentista_id";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::agendamento tmp;
   tmp.m_agendamento_id = this->m_agendamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_dentista_id = tmp.m_dentista_id; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_dentista_id;
}

agendamento::type_especialidade_id agendamento::getespecialidadeId(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getespecialidadeId(); }
   QString sRelation = "especialidade_id";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::agendamento tmp;
   tmp.m_agendamento_id = this->m_agendamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_especialidade_id = tmp.m_especialidade_id; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_especialidade_id;
}

QString agendamento::salvar()
{
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QString agendamento::apagar()
{
    QString retorno;
    QSqlError daoError = qx::dao::destroy_by_id(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

} // namespace clinica
