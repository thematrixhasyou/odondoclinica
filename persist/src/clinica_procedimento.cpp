#include "../include/clinica_procedimento.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::procedimento, clinica_procedimento)

namespace qx {

template <>
void register_class(QxClass<clinica::procedimento> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("procedimento");
   t.setSoftDelete(qx::QxSoftDelete("inativo"));

   pData = t.id(& clinica::procedimento::m_procedimento_id, "procedimento_id", 0);

   pData = t.data(& clinica::procedimento::m_descricao, "descricao", 0, true, true);

   qx::QxValidatorX<clinica::procedimento> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_MinLength("descricao", 5);
   pAllValidator->add_MaxLength("descricao", 30);
   pAllValidator->add_NotNull("descricao");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(procedimento)

procedimento::procedimento() : m_procedimento_id(0) { ; }

procedimento::procedimento(const long & id) : m_procedimento_id(id) { ; }

procedimento::~procedimento() { ; }

long procedimento::getprocedimento_id() const { return m_procedimento_id; }

QString procedimento::getdescricao() const { return m_descricao; }

void procedimento::setprocedimento_id(const long & val) { m_procedimento_id = val; }

void procedimento::setdescricao(const QString & val) { m_descricao = val; }

QString procedimento::salvar() {
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QString procedimento::inativar() {
    QString retorno;
    QSqlError daoError = qx::dao::delete_by_id(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

} // namespace clinica
