#include "../include/clinica_caixa.h"
#include "../include/clinica_pagamento.h"
#include "../include/clinica_retirada.h"

#include <QxOrm.h>
#include <QxMemLeak.h>
#include <QDebug>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::caixa, clinica_caixa)

namespace qx {

template <>
void register_class(QxClass<clinica::caixa> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("caixa");

   pData = t.id(& clinica::caixa::m_caixa_id, "caixa_id", 0);
   pData->setName("caixa_id");

   pData = t.data(& clinica::caixa::m_dataMovimento, "data_movimento", 0, true, true);
   pData = t.data(& clinica::caixa::m_dataHoraFechamento, "data_hora_fechamento", 0, true, true);
   pData = t.data(& clinica::caixa::m_trocoInicial, "troco_inicial", 0, true, true);

   pRelation = t.relationOneToMany(& clinica::caixa::m_listaPagamento, "listaPagamento", "caixa_id", 0);
   pRelation = t.relationOneToMany(& clinica::caixa::m_listaRetirada, "listaRetirada", "caixa_id", 0);

   qx::QxValidatorX<clinica::caixa> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_NotNull("data_movimento");
   pAllValidator->add_MinDecimal("troco_inicial", 0);
   pAllValidator->add_NotNull("troco_inicial");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(caixa)

caixa::caixa() : m_caixa_id(0), m_trocoInicial(0.0) { ; }

caixa::caixa(const long &id) : m_caixa_id(id), m_trocoInicial(0.0) { ; }

caixa::~caixa() { ; }

long caixa::getcaixa_id() const { return m_caixa_id; }

QDate caixa::getdata_movimento() const { return m_dataMovimento; }

QDateTime caixa::getdata_hora_fechamento() const { return m_dataHoraFechamento; }

double caixa::gettroco_inicial() const { return m_trocoInicial; }

caixa::type_listaPagamento caixa::getlistaPagamento() const { return m_listaPagamento; }

caixa::type_listaPagamento & caixa::listaPagamento() { return m_listaPagamento; }

const caixa::type_listaPagamento & caixa::listaPagamento() const { return m_listaPagamento; }

caixa::type_listaRetirada caixa::getlistaRetirada() const { return m_listaRetirada; }

caixa::type_listaRetirada & caixa::listaRetirada() { return m_listaRetirada; }

const caixa::type_listaRetirada & caixa::listaRetirada() const { return m_listaRetirada; }

void caixa::setcaixa_id(const long id) { m_caixa_id = id; }

void caixa::setdata_movimento(const QDate & val) { m_dataMovimento = val; }

void caixa::setdata_hora_fechamento(const QDateTime & val) { m_dataHoraFechamento = val; }

void caixa::settroco_inicial(const double & val) { m_trocoInicial = val; }

void caixa::setlistaPagamento(const caixa::type_listaPagamento & val) { m_listaPagamento = val; }

void caixa::setlistaRetirada(const caixa::type_listaRetirada & val) { m_listaRetirada = val; }

caixa::type_listaPagamento caixa::getlistaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaPagamento(); }
   QString sRelation = "listaPagamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::caixa tmp;
   tmp.m_caixa_id = this->m_caixa_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaPagamento = tmp.m_listaPagamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaPagamento;
}

caixa::type_listaPagamento & caixa::listaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaPagamento(); }
   QString sRelation = "listaPagamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::caixa tmp;
   tmp.m_caixa_id = this->m_caixa_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaPagamento = tmp.m_listaPagamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaPagamento;
}

caixa::type_listaRetirada caixa::getlistaRetirada(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaRetirada(); }
   QString sRelation = "listaRetirada";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::caixa tmp;
   tmp.m_caixa_id = this->m_caixa_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaRetirada = tmp.m_listaRetirada; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaRetirada;
}

caixa::type_listaRetirada & caixa::listaRetirada(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaRetirada(); }
   QString sRelation = "listaRetirada";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::caixa tmp;
   tmp.m_caixa_id = this->m_caixa_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaRetirada = tmp.m_listaRetirada; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaRetirada;
}

QString caixa::salvar()
{
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QSharedPointer<caixa> caixa::retornaUltimoMovimento()
{
    list_of_caixa listMovimento;
    caixa_ptr movimento;

    qx::QxSqlQuery query("WHERE caixa.data_movimento = (select max(caixa.data_movimento) from caixa)");
    qx::dao::fetch_by_query(query, listMovimento);

    return (listMovimento.count() > 0 ? listMovimento.getFirst() : movimento);
}

bool caixa::isMovimentoAberto(QDate data_movimento) {
    list_of_caixa listMovimento;
    caixa_ptr movimento;

    qx::QxSqlQuery query("WHERE caixa.data_movimento = STR_TO_DATE('" + data_movimento.toString("yyyy-MM-dd") + "', '%Y-%m-%d')");
    qx::dao::fetch_by_query(query, listMovimento);

    if(listMovimento.count() > 0) {
        movimento = listMovimento.getFirst();
    }
    if(movimento.isNull() || movimento->getdata_movimento().isNull()) {
        return false;
    } else {
        return movimento->getdata_hora_fechamento().isNull();
    }
}

QSharedPointer<caixa> caixa::retornaMovimento(QDate data_movimento)
{
    list_of_caixa listMovimento;
    caixa_ptr movimento;

    qx::QxSqlQuery query("WHERE caixa.data_movimento = STR_TO_DATE('" + data_movimento.toString("yyyy-MM-dd") + "', '%Y-%m-%d')");
    qx::dao::fetch_by_query(query, listMovimento);

    return (listMovimento.count() > 0 ? listMovimento.getFirst() : movimento);
}

caixa::type_listaTotalizacao caixa::retornaPagamentosTotalizados()
{
    qx::QxSqlQuery query("select tipo, sum(valor) as valor from pagamento where caixa_id = :caixa_id group by tipo");
    query.bind(":caixa_id", QVariant::fromValue(this->m_caixa_id));

    qx::dao::call_query(query);

    long quantidadeResultado = query.getSqlResultRowCount();
    type_listaTotalizacao resultado;
    for(int i = 0; i < quantidadeResultado; ++i) {
        resultado.insert(query.getSqlResultAt(i, "tipo").toString(), query.getSqlResultAt(i, "valor").toString());
    }
    return resultado;
}

caixa::type_listaTotalizacao caixa::retornaRetiradasTotalizadas()
{
    qx::QxSqlQuery query("select tipo, sum(valor) as valor from retirada where caixa_id = :caixa_id group by tipo");
    query.bind(":caixa_id", QVariant::fromValue(this->m_caixa_id));

    qx::dao::call_query(query);

    long quantidadeResultado = query.getSqlResultRowCount();
    type_listaTotalizacao resultado;
    for(int i = 0; i < quantidadeResultado; ++i) {
        resultado.insert(query.getSqlResultAt(i, "tipo").toString(), query.getSqlResultAt(i, "valor").toString());
    }
    return resultado;
}

caixa::type_listaDetalhePagamento caixa::retornaListaDetalhePagamento()
{
    qx::QxSqlQuery query("select c.nome, p.tipo, p.valor, p.data_realizacao, p.data_efetivacao, p.cc_bandeira as bandeira, p.cc_autorizacao as autorizacao \
                         from pagamento p \
                         inner join tratamento t on p.tratamento_id = t.tratamento_id \
                         inner join cliente c on t.cliente_id = c.cliente_id \
                         where p.caixa_id = :caixa_id \
                         order by p.data_realizacao, c.nome, p.tipo, p.data_efetivacao, p.cc_bandeira, p.cc_autorizacao");
    query.bind(":caixa_id", QVariant::fromValue(this->m_caixa_id));

    qx::dao::call_query(query);

    long quantidadeResultado = query.getSqlResultRowCount();
    type_listaDetalhePagamento resultado;
    detalhe_pagamento detalhe_pagto;
    for(int i = 0; i < quantidadeResultado; ++i) {
        detalhe_pagto.nome_cliente = query.getSqlResultAt(i, "nome").toString();
        detalhe_pagto.tipo_pagamento = query.getSqlResultAt(i, "tipo").toString();
        detalhe_pagto.valor = query.getSqlResultAt(i, "valor").toString();
        detalhe_pagto.data_realizacao = query.getSqlResultAt(i, "data_realizacao").toDate().toString("dd/MM/yyyy");
        detalhe_pagto.data_efetivacao = query.getSqlResultAt(i, "data_efetivacao").toDate().toString("dd/MM/yyyy");
        detalhe_pagto.bandeira_cartao = query.getSqlResultAt(i, "bandeira").toString();
        detalhe_pagto.autorizacao_cartao = query.getSqlResultAt(i, "autorizacao").toString();

        resultado.append(detalhe_pagto);

        detalhe_pagto.bandeira_cartao.clear();
        detalhe_pagto.autorizacao_cartao.clear();
    }
    return resultado;
}

} // namespace clinica
