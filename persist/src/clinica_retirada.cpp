#include "../include/clinica_retirada.h"
#include "../include/clinica_caixa.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::retirada, clinica_retirada)

namespace qx {

template <>
void register_class(QxClass<clinica::retirada> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("retirada");

   pData = t.id(& clinica::retirada::m_retirada_id, "retirada_id", 0);

   pData = t.data(& clinica::retirada::m_tipo, "tipo", 0, true, true);
   pData = t.data(& clinica::retirada::m_valor, "valor", 0, true, true);
   pData = t.data(& clinica::retirada::m_autor, "autor", 0, true, true);
   pData = t.data(& clinica::retirada::m_motivo, "motivo", 0, true, true);

   pRelation = t.relationManyToOne(& clinica::retirada::m_caixa, "caixa_id", 0);
   pRelation->getDataMember()->setName("caixa_id");

   qx::QxValidatorX<clinica::retirada> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_MinLength("tipo", 3);
   pAllValidator->add_MaxLength("tipo", 3);
   pAllValidator->add_NotNull("tipo");
   pAllValidator->add_MinDecimal("valor", 0.5);
   pAllValidator->add_NotNull("valor");
   pAllValidator->add_MinLength("autor", 5);
   pAllValidator->add_MaxLength("autor", 60);
   pAllValidator->add_NotNull("autor");
   pAllValidator->add_MaxLength("motivo", 500);
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(retirada)

retirada::retirada() : m_retirada_id(0), m_valor(0.0) { ; }

retirada::retirada(const long & id) : m_retirada_id(id), m_valor(0.0) { ; }

retirada::~retirada() { ; }

long retirada::getretirada_id() const { return m_retirada_id; }

QString retirada::gettipo() const { return m_tipo; }

double retirada::getvalor() const { return m_valor; }

QString retirada::getautor() const { return m_autor; }

QString retirada::getmotivo() const { return m_motivo; }

retirada::type_caixa retirada::getcaixa() const { return m_caixa; }

void retirada::setretirada_id(const long & val) { m_retirada_id = val; }

void retirada::settipo(const QString & val) { m_tipo = val; }

void retirada::setvalor(const double & val) { m_valor = val; }

void retirada::setautor(const QString & val) { m_autor = val; }

void retirada::setmotivo(const QString & val) { m_motivo = val; }

void retirada::setcaixa(const retirada::type_caixa & val) { m_caixa = val; }

retirada::type_caixa retirada::getcaixa(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getcaixa(); }
   QString sRelation = "caixa";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::retirada tmp;
   tmp.m_retirada_id = this->m_retirada_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_caixa = tmp.m_caixa; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_caixa;
}

QString retirada::salvar()
{
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

} // namespace clinica
