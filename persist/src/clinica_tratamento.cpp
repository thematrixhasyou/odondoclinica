#include "../include/clinica_tratamento.h"
#include "../include/clinica_item_tratamento.h"
#include "../include/clinica_pagamento.h"
#include "../include/clinica_cliente.h"
#include "../include/clinica_dentista.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::tratamento, clinica_tratamento)

namespace qx {

template <>
void register_class(QxClass<clinica::tratamento> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("tratamento");

   pData = t.id(& clinica::tratamento::m_tratamento_id, "tratamento_id", 0);

   pData = t.data(& clinica::tratamento::m_data_inicio, "data_inicio", 0, true, true);
   pData = t.data(& clinica::tratamento::m_data_termino, "data_termino", 0, true, true);

   pRelation = t.relationOneToMany(& clinica::tratamento::m_listaPagamento, "listaPagamento", "tratamento_id", 0);
   pRelation = t.relationOneToMany(& clinica::tratamento::m_listaItemTratamento, "listaItemTratamento", "tratamento_id", 0);
   pRelation = t.relationManyToOne(& clinica::tratamento::m_cliente, "cliente_id", 0);
   pRelation = t.relationManyToOne(& clinica::tratamento::m_dentista, "dentista_id", 0);

   qx::QxValidatorX<clinica::tratamento> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_NotNull("data_inicio");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(tratamento)

tratamento::tratamento() : m_tratamento_id(0) { ; }

tratamento::tratamento(const long & id) : m_tratamento_id(id) { ; }

tratamento::~tratamento() { ; }

long tratamento::gettratamento_id() const { return m_tratamento_id; }

QDate tratamento::getdata_inicio() const { return m_data_inicio; }

QDate tratamento::getdata_termino() const { return m_data_termino; }

tratamento::type_listaPagamento tratamento::getlistaPagamento() const { return m_listaPagamento; }

tratamento::type_listaPagamento & tratamento::listaPagamento() { return m_listaPagamento; }

const tratamento::type_listaPagamento & tratamento::listaPagamento() const { return m_listaPagamento; }

tratamento::type_listaItemTratamento tratamento::getlistaItemTratamento() const { return m_listaItemTratamento; }

tratamento::type_listaItemTratamento & tratamento::listaItemTratamento() { return m_listaItemTratamento; }

const tratamento::type_listaItemTratamento & tratamento::listaItemTratamento() const { return m_listaItemTratamento; }


tratamento::type_cliente tratamento::getcliente() const { return m_cliente; }

tratamento::type_dentista tratamento::getdentista() const { return m_dentista; }

void tratamento::settratamento_id(const long & val) { m_tratamento_id = val; }

void tratamento::setdata_inicio(const QDate & val) { m_data_inicio = val; }

void tratamento::setdata_termino(const QDate & val) { m_data_termino = val; }

void tratamento::setlistaPagamento(const tratamento::type_listaPagamento & val) { m_listaPagamento = val; }

void tratamento::setlistaItemTratamento(const tratamento::type_listaItemTratamento & val) { m_listaItemTratamento = val; }


void tratamento::setcliente(const tratamento::type_cliente & val) { m_cliente = val; }

void tratamento::setdentista(const tratamento::type_dentista & val) { m_dentista = val; }

tratamento::type_listaPagamento tratamento::getlistaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaPagamento(); }
   QString sRelation = "listaPagamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::tratamento tmp;
   tmp.m_tratamento_id = this->m_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaPagamento = tmp.m_listaPagamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaPagamento;
}

tratamento::type_listaPagamento & tratamento::listaPagamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaPagamento(); }
   QString sRelation = "listaPagamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::tratamento tmp;
   tmp.m_tratamento_id = this->m_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaPagamento = tmp.m_listaPagamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaPagamento;
}

tratamento::type_listaItemTratamento tratamento::getlistaItemTratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaItemTratamento(); }
   QString sRelation = "listaItemTratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::tratamento tmp;
   tmp.m_tratamento_id = this->m_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaItemTratamento = tmp.m_listaItemTratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaItemTratamento;
}

tratamento::type_listaItemTratamento & tratamento::listaItemTratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaItemTratamento(); }
   QString sRelation = "listaItemTratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::tratamento tmp;
   tmp.m_tratamento_id = this->m_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaItemTratamento = tmp.m_listaItemTratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaItemTratamento;
}

tratamento::type_cliente tratamento::getcliente(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getcliente(); }
   QString sRelation = "cliente";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::tratamento tmp;
   tmp.m_tratamento_id = this->m_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_cliente = tmp.m_cliente; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_cliente;
}

tratamento::type_dentista tratamento::getdentista(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getdentista(); }
   QString sRelation = "dentista";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::tratamento tmp;
   tmp.m_tratamento_id = this->m_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_dentista = tmp.m_dentista; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_dentista;
}

tratamento::type_dentista tratamento::getDentistaMesmoInativo()
{
    tratamento::type_dentista retorno;
    retorno.reset(new clinica::dentista());

    qx::QxSqlQuery query("SELECT dentista.dentista_id AS id, dentista.nome AS nome, dentista.registro_cro AS registroCro, dentista.duracao_consulta AS duracaoConsulta, dentista.percentual_comissao AS percentualComissao, dentista.inativo AS inativo\
                          FROM dentista\
                          WHERE dentista.dentista_id = :dentista_id");
    query.bind(":dentista_id", QVariant::fromValue(this->m_dentista->getid()));

    QSqlError daoError = qx::dao::execute_query(query, retorno);
    if(daoError.isValid()) {
        retorno.reset(new clinica::dentista());
    }
    this->m_dentista = retorno;
    return retorno;
}

tratamento::type_listaItemTratamento tratamento::getlistaItemTratamentoOrdenadaPelaOrdem()
{
    tratamento::type_listaItemTratamento listaItemTratamento;
    qx::QxSqlQuery query;
    query.where(item_tratamento::relation_tratamento()).isEqualTo(QVariant::fromValue(this->m_tratamento_id)).orderAsc(item_tratamento::column_ordem());

    QSqlError daoError = qx::dao::fetch_by_query_with_relation("procedimento", query, listaItemTratamento);
    if(daoError.isValid()) {
        listaItemTratamento.clear();
    }
    this->m_listaItemTratamento = listaItemTratamento;
    return listaItemTratamento;
}

tratamento::type_listaPagamento tratamento::getlistaPagamentoOrdenadaPelaData()
{
    tratamento::type_listaPagamento listaPagamento;

    qx::QxSqlQuery query;
    query.where(pagamento::relation_tratamento()).isEqualTo(QVariant::fromValue(this->m_tratamento_id)).orderAsc(pagamento::column_data_realizacao(), pagamento::column_data_efetivacao());

    QSqlError daoError = qx::dao::fetch_by_query_with_relation(QStringList() << "caixa", query, listaPagamento);
    if(daoError.isValid()) {
        listaPagamento.clear();
    }
    this->m_listaPagamento = listaPagamento;
    return listaPagamento;
}

QString tratamento::salvarComListaItemTratamento()
{
    QString retorno;
    QSqlError daoError = qx::dao::save_with_relation(relation_listaItemTratamento(), *this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QString tratamento::salvarSemListaItemTratamento()
{
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

long tratamento::retornaProximaOrdemLivre()
{
    qx::QxSqlQuery query("SELECT if(MAX(ordem) is null, 0, MAX(ordem))+1 as proxima_ordem FROM item_tratamento WHERE tratamento_id = :tratamento_id");
    query.bind(":tratamento_id", QVariant::fromValue(this->m_tratamento_id));

    qx::dao::call_query(query);

    return query.getSqlResultAt(0, "proxima_ordem").toInt();
}

QList<tratamento::detalhe_tratamento_devedor> tratamento::retornaListaTratamentoDevedor()
{
    qx::QxSqlQuery query("select d.nome as nome_dentista, c.nome as nome_cliente, t.data_inicio, i.valor_tratamento, p.total_pgto, (i.valor_tratamento-p.total_pgto) as saldo_devedor \
                         from tratamento t \
                         inner join dentista d on t.dentista_id = d.dentista_id \
                         inner join cliente c on t.cliente_id = c.cliente_id \
                         left join (select tratamento_id, sum(valor) as total_pgto from pagamento group by tratamento_id) p on t.tratamento_id = p.tratamento_id \
                         left join (select tratamento_id, sum(quantidade*valor_unitario) as valor_tratamento from item_tratamento group by tratamento_id) i on t.tratamento_id = i.tratamento_id \
                         where t.data_termino is null and (i.valor_tratamento-p.total_pgto) > 0 \
                         order by nome_dentista, nome_cliente");

     qx::dao::call_query(query);

     QList<tratamento::detalhe_tratamento_devedor> listaTratamentoDevedor;
     long quantidadeResultado = query.getSqlResultRowCount();
     tratamento::detalhe_tratamento_devedor detalhe;

     for(int i = 0; i < quantidadeResultado; ++i) {
         detalhe.nome_dentista = query.getSqlResultAt(i, "nome_dentista").toString();
         detalhe.nome_cliente = query.getSqlResultAt(i, "nome_cliente").toString();
         detalhe.data_inicio = query.getSqlResultAt(i, "data_inicio").toDate().toString("dd/MM/yyyy");
         detalhe.valor_tratamento = query.getSqlResultAt(i, "valor_tratamento").toString();
         detalhe.valor_pagto = query.getSqlResultAt(i, "total_pgto").toString();
         detalhe.saldo_devedor = query.getSqlResultAt(i, "saldo_devedor").toString();
         listaTratamentoDevedor.append(detalhe);
     }
     return listaTratamentoDevedor;
}

} // namespace clinica
