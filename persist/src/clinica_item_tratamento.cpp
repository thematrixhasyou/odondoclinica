#include "../include/clinica_item_tratamento.h"
#include "../include/clinica_procedimento.h"
#include "../include/clinica_tratamento.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::item_tratamento, clinica_item_tratamento)

namespace qx {

template <>
void register_class(QxClass<clinica::item_tratamento> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("item_tratamento");

   pData = t.id(& clinica::item_tratamento::m_item_tratamento_id, "item_tratamento_id", 0);

   pData = t.data(& clinica::item_tratamento::m_ordem, "ordem", 0, true, true);
   pData = t.data(& clinica::item_tratamento::m_quantidade, "quantidade", 0, true, true);
   pData = t.data(& clinica::item_tratamento::m_valor_unitario, "valor_unitario", 0, true, true);
   pData = t.data(& clinica::item_tratamento::m_realizacao, "realizacao", 0, true, true);

   pRelation = t.relationManyToOne(& clinica::item_tratamento::m_procedimento, "procedimento", 0);
   pRelation->getDataMember()->setName("procedimento_id");
   pRelation = t.relationManyToOne(& clinica::item_tratamento::m_tratamento, "tratamento", 0);
   pRelation->getDataMember()->setName("tratamento_id");

   qx::QxValidatorX<clinica::item_tratamento> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_MinDecimal("ordem", 1);
   pAllValidator->add_NotNull("ordem");
   pAllValidator->add_NotNull("quantidade");
   pAllValidator->add_MinDecimal("valor_unitario", 1);
   pAllValidator->add_NotNull("valor_unitario");
   pAllValidator->add_MinLength("realizacao", 3);
   pAllValidator->add_MaxLength("realizacao", 3);
   pAllValidator->add_NotNull("realizacao");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(item_tratamento)

item_tratamento::item_tratamento() : m_item_tratamento_id(0), m_ordem(0), m_quantidade(0), m_valor_unitario(0.0) { ; }

item_tratamento::item_tratamento(const long & id) : m_item_tratamento_id(id), m_ordem(0), m_quantidade(0), m_valor_unitario(0.0) { ; }

item_tratamento::~item_tratamento() { ; }

long item_tratamento::getitem_tratamento_id() const { return m_item_tratamento_id; }

long item_tratamento::getordem() const { return m_ordem; }

long item_tratamento::getquantidade() const { return m_quantidade; }

double item_tratamento::getvalor_unitario() const { return m_valor_unitario; }

QString item_tratamento::getrealizacao() const { return m_realizacao; }

item_tratamento::type_procedimento item_tratamento::getprocedimento() const { return m_procedimento; }

item_tratamento::type_tratamento item_tratamento::gettratamento() const { return m_tratamento; }

void item_tratamento::setitem_tratamento_id(const long & val) { m_item_tratamento_id = val; }

void item_tratamento::setordem(const long & val) { m_ordem = val; }

void item_tratamento::setquantidade(const long & val) { m_quantidade = val; }

void item_tratamento::setvalor_unitario(const double & val) { m_valor_unitario = val; }

void item_tratamento::setrealizacao(const QString & val) { m_realizacao = val; }

void item_tratamento::setprocedimento(const item_tratamento::type_procedimento & val) { m_procedimento = val; }

void item_tratamento::settratamento(const item_tratamento::type_tratamento & val) { m_tratamento = val; }

item_tratamento::type_procedimento item_tratamento::getprocedimento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getprocedimento(); }
   QString sRelation = "procedimento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::item_tratamento tmp;
   tmp.m_item_tratamento_id = this->m_item_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_procedimento = tmp.m_procedimento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_procedimento;
}

item_tratamento::type_tratamento item_tratamento::gettratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return gettratamento(); }
   QString sRelation = "tratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::item_tratamento tmp;
   tmp.m_item_tratamento_id = this->m_item_tratamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_tratamento = tmp.m_tratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_tratamento;
}

QString item_tratamento::salvar()
{
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QString item_tratamento::excluir()
{
    QString retorno;
    QSqlError daoError = qx::dao::delete_by_id(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

} // namespace clinica
