#include "../include/clinica_especialidade.h"
#include "../include/clinica_dentista.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::especialidade, clinica_especialidade)

namespace qx {

template <>
void register_class(QxClass<clinica::especialidade> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("especialidade");

   pData = t.id(& clinica::especialidade::m_id, "id", 0);
   pData->setName("especialidade_id");

   pData = t.data(& clinica::especialidade::m_descricao, "descricao", 0, true, true);
   pData->setIsUnique(true);

   pRelation = t.relationManyToMany(& clinica::especialidade::m_listaDentista, "listaDentista", "dentista_especialidade", "especialidade_id", "dentista_id", 0);

   qx::QxValidatorX<clinica::especialidade> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_MinLength("descricao", 5);
   pAllValidator->add_MaxLength("descricao", 30);
   pAllValidator->add_NotNull("descricao");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(especialidade)

especialidade::especialidade() : m_id(0) { ; }

especialidade::especialidade(const long & id) : m_id(id) { ; }

especialidade::~especialidade() { ; }

long especialidade::getid() const { return m_id; }

QString especialidade::getdescricao() const { return m_descricao; }

especialidade::type_listaDentista especialidade::getlistaDentista() const { return m_listaDentista; }

especialidade::type_listaDentista & especialidade::listaDentista() { return m_listaDentista; }

const especialidade::type_listaDentista & especialidade::listaDentista() const { return m_listaDentista; }

void especialidade::setid(const long & val) { m_id = val; }

void especialidade::setdescricao(const QString & val) { m_descricao = val; }

void especialidade::setlistaDentista(const especialidade::type_listaDentista & val) { m_listaDentista = val; }

especialidade::type_listaDentista especialidade::getlistaDentista(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaDentista(); }
   QString sRelation = "listaDentista";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::especialidade tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaDentista = tmp.m_listaDentista; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaDentista;
}

especialidade::type_listaDentista & especialidade::listaDentista(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaDentista(); }
   QString sRelation = "listaDentista";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::especialidade tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaDentista = tmp.m_listaDentista; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaDentista;
}

} // namespace clinica
