#include "../include/clinica_pagamento.h"
#include "../include/clinica_tratamento.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::pagamento, clinica_pagamento)

namespace qx {

template <>
void register_class(QxClass<clinica::pagamento> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("pagamento");

   pData = t.id(& clinica::pagamento::m_pagamento_id, "pagamento_id", 0);

   pData = t.data(& clinica::pagamento::m_valor, "valor", 0, true, true);
   pData = t.data(& clinica::pagamento::m_tipo, "tipo", 0, true, true);
   pData = t.data(& clinica::pagamento::m_data_realizacao, "data_realizacao", 0, true, true);
   pData = t.data(& clinica::pagamento::m_data_efetivacao, "data_efetivacao", 0, true, true);
   pData = t.data(& clinica::pagamento::m_cc_bandeira, "cc_bandeira", 0, true, true);
   pData = t.data(& clinica::pagamento::m_cc_final_numero, "cc_final_numero", 0, true, true);
   pData = t.data(& clinica::pagamento::m_cc_autorizacao, "cc_autorizacao", 0, true, true);

   pRelation = t.relationManyToOne(& clinica::pagamento::m_tratamento, "tratamento", 0);
   pRelation->getDataMember()->setName("tratamento_id");
   pRelation = t.relationManyToOne(& clinica::pagamento::m_caixa, "caixa", 0);
   pRelation->getDataMember()->setName("caixa_id");

   qx::QxValidatorX<clinica::pagamento> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_MinDecimal("valor", 1);
   pAllValidator->add_NotNull("valor");
   pAllValidator->add_MinLength("tipo", 3);
   pAllValidator->add_MaxLength("tipo", 3);
   pAllValidator->add_NotNull("tipo");
   pAllValidator->add_MaxLength("cc_bandeira", 15);
   pAllValidator->add_MaxLength("cc_final_numero", 6);
   pAllValidator->add_MaxLength("cc_autorizacao", 8);
   pAllValidator->add_NotNull("data_realizacao");
   pAllValidator->add_NotNull("data_efetivacao");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(pagamento)

pagamento::pagamento() : m_pagamento_id(0), m_valor(0.0) { ; }

pagamento::pagamento(const long & id) : m_pagamento_id(id), m_valor(0.0) { ; }

pagamento::~pagamento() { ; }

long pagamento::getpagamento_id() const { return m_pagamento_id; }

double pagamento::getvalor() const { return m_valor; }

QString pagamento::gettipo() const { return m_tipo; }

QDate pagamento::getdata_realizacao() const { return m_data_realizacao; }

QDate pagamento::getdata_efetivacao() const { return m_data_efetivacao; }

QString pagamento::getcc_bandeira() const { return m_cc_bandeira; }

QString pagamento::getcc_final_numero() const { return m_cc_final_numero; }

QString pagamento::getcc_autorizacao() const { return m_cc_autorizacao; }

pagamento::type_tratamento pagamento::gettratamento() const { return m_tratamento; }

pagamento::type_caixa pagamento::getCaixa() const { return m_caixa; }

void pagamento::setpagamento_id(const long & val) { m_pagamento_id = val; }

void pagamento::setvalor(const double & val) { m_valor = val; }

void pagamento::settipo(const QString & val) { m_tipo = val; }

void pagamento::setdata_realizacao(const QDate & val) { m_data_realizacao = val; }

void pagamento::setdata_efetivacao(const QDate & val) { m_data_efetivacao = val; }

void pagamento::setcc_bandeira(const QString & val) { m_cc_bandeira = val; }

void pagamento::setcc_final_numero(const QString & val) { m_cc_final_numero = val; }

void pagamento::setcc_autorizacao(const QString & val) { m_cc_autorizacao = val; }

void pagamento::settratamento(const pagamento::type_tratamento & val) { m_tratamento = val; }

void pagamento::setCaixa(const pagamento::type_caixa & val) { m_caixa = val; }

pagamento::type_tratamento pagamento::gettratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return gettratamento(); }
   QString sRelation = "tratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::pagamento tmp;
   tmp.m_pagamento_id = this->m_pagamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_tratamento = tmp.m_tratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_tratamento;
}

pagamento::type_caixa pagamento::getCaixa(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getCaixa(); }
   QString sRelation = "caixa";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::pagamento tmp;
   tmp.m_pagamento_id = this->m_pagamento_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_caixa = tmp.m_caixa; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_caixa;
}

QString pagamento::salvar()
{
    QString retorno;
    QSqlError daoError = qx::dao::save(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QString pagamento::excluir()
{
    QString retorno;
    QSqlError daoError = qx::dao::delete_by_id(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

QList<pagamento::detalhe_fechamento_periodo> pagamento::retornaFechamentoPeriodo(QDate data_inicio, QDate data_fim)
{

    qx::QxSqlQuery query("(select d.dentista_id, d.nome,  'producao' as faturamento, p.tipo, sum(p.valor) as valor \
                         from pagamento p \
                         inner join tratamento t on t.tratamento_id = p.tratamento_id \
                         inner join dentista d on d.dentista_id = t.dentista_id \
                         where p.data_realizacao >= STR_TO_DATE('" + data_inicio.toString("dd/MM/yyyy") + "', '%d/%m/%Y') and \
                               p.data_realizacao <= STR_TO_DATE('" + data_fim.toString("dd/MM/yyyy") + "', '%d/%m/%Y') \
                         group by d.dentista_id, d.nome, faturamento, p.tipo) \
                         \
                         union \
                         \
                         (select d.dentista_id, d.nome,  'corrente' as faturamento, p.tipo, sum(p.valor) as valor \
                         from pagamento p \
                         inner join tratamento t on t.tratamento_id = p.tratamento_id \
                         inner join dentista d on d.dentista_id = t.dentista_id \
                         where p.data_efetivacao >= STR_TO_DATE('" + data_inicio.toString("dd/MM/yyyy") + "', '%d/%m/%Y') and \
                               p.data_efetivacao <= STR_TO_DATE('" + data_fim.toString("dd/MM/yyyy") + "', '%d/%m/%Y') \
                         group by d.dentista_id, d.nome, faturamento, p.tipo) \
                         \
                         union \
                         \
                         (select d.dentista_id, d.nome,  'futuro' as faturamento, p.tipo, sum(p.valor) as valor \
                         from pagamento p \
                         inner join tratamento t on t.tratamento_id = p.tratamento_id \
                         inner join dentista d on d.dentista_id = t.dentista_id \
                         where p.data_efetivacao > STR_TO_DATE('" + data_fim.toString("dd/MM/yyyy") + "', '%d/%m/%Y') \
                         group by d.dentista_id, d.nome, faturamento, p.tipo) \
                         order by nome, faturamento, tipo");

     qx::dao::call_query(query);

     clinica::dentista_ptr dentista;
     QList<detalhe_fechamento_periodo> listaFechamento;
     QString nome_dentista = "";
     long quantidadeResultado = query.getSqlResultRowCount();
     detalhe_fechamento_periodo detalhe;
     double producao_total, fat_corr_total, fat_fut_total, perc_comissao;
     producao_total = 0.0f;
     fat_corr_total = 0.0f;
     fat_fut_total = 0.0f;
     perc_comissao = 0.0f;

     for(int i = 0; i < quantidadeResultado; ++i) {

         if(query.getSqlResultAt(i, "nome").toString() != nome_dentista) {
             if(nome_dentista != "") {
                 listaFechamento.append(detalhe);
             }
             dentista.reset(new clinica::dentista(query.getSqlResultAt(i, "dentista_id").toInt()));
             dentista->load();
             perc_comissao = dentista->getpercentualComissao();
             nome_dentista = query.getSqlResultAt(i, "nome").toString();
             detalhe.nome_dentista = nome_dentista;
             detalhe.producao_cheque = "";
             detalhe.producao_credito = "";
             detalhe.producao_debito = "";
             detalhe.producao_dinheiro = "";
             detalhe.producao_total = "";
             detalhe.fat_corr_chegue = "";
             detalhe.fat_corr_debito = "";
             detalhe.fat_corr_dinheiro = "";
             detalhe.fat_corr_credito = "";
             detalhe.fat_corr_total = "";
             detalhe.fat_corr_comissao = "";
             detalhe.fat_fut_credito = "";
             detalhe.fat_fut_debito = "";
             detalhe.fat_fut_total = "";
             producao_total = 0.0f;
             fat_corr_total = 0.0f;
             fat_fut_total = 0.0f;
         }

         if(query.getSqlResultAt(i, "faturamento").toString() == "producao") {
             producao_total += query.getSqlResultAt(i, "valor").toInt();
             detalhe.producao_total = QString::number(producao_total);
             if(query.getSqlResultAt(i, "tipo").toString() == "DIN") {
                 detalhe.producao_dinheiro = query.getSqlResultAt(i, "valor").toString();
             } else if(query.getSqlResultAt(i, "tipo").toString() == "CHE") {
                 detalhe.producao_cheque = query.getSqlResultAt(i, "valor").toString();
             } else if(query.getSqlResultAt(i, "tipo").toString() == "CRE") {
                 detalhe.producao_credito = query.getSqlResultAt(i, "valor").toString();
             } else if(query.getSqlResultAt(i, "tipo").toString() == "DEB") {
                 detalhe.producao_debito = query.getSqlResultAt(i, "valor").toString();
             }
         } else if(query.getSqlResultAt(i, "faturamento").toString() == "corrente") {
             fat_corr_total += query.getSqlResultAt(i, "valor").toInt();
             detalhe.fat_corr_total = QString::number(fat_corr_total);
             detalhe.fat_corr_comissao = QString::number(fat_corr_total*perc_comissao/100);
             if(query.getSqlResultAt(i, "tipo").toString() == "DIN") {
                 detalhe.fat_corr_dinheiro = query.getSqlResultAt(i, "valor").toString();
             } else if(query.getSqlResultAt(i, "tipo").toString() == "CHE") {
                 detalhe.fat_corr_chegue = query.getSqlResultAt(i, "valor").toString();
             } else if(query.getSqlResultAt(i, "tipo").toString() == "CRE") {
                 detalhe.fat_corr_credito = query.getSqlResultAt(i, "valor").toString();
             } else if(query.getSqlResultAt(i, "tipo").toString() == "DEB") {
                 detalhe.fat_corr_debito = query.getSqlResultAt(i, "valor").toString();
             }
         } else if(query.getSqlResultAt(i, "faturamento").toString() == "futuro") {
             fat_fut_total += query.getSqlResultAt(i, "valor").toInt();
             detalhe.fat_fut_total = QString::number(fat_fut_total);
             if(query.getSqlResultAt(i, "tipo").toString() == "CRE") {
                 detalhe.fat_fut_credito = query.getSqlResultAt(i, "valor").toString();
             } else if(query.getSqlResultAt(i, "tipo").toString() == "DEB") {
                 detalhe.fat_fut_debito = query.getSqlResultAt(i, "valor").toString();
             }
         }
     }
     listaFechamento.append(detalhe);
     return listaFechamento;
}

QList<caixa::detalhe_pagamento> pagamento::retornaDetalheProducaoPeriodo(QDate data_inicio, QDate data_fim)
{
    qx::QxSqlQuery query("select d.nome as nome_dentista, p.tipo, c.nome as nome_cliente, p.valor, p.data_realizacao, p.data_efetivacao, p.cc_bandeira as bandeira, p.cc_autorizacao as autorizacao \
                         from pagamento p \
                         inner join tratamento t on p.tratamento_id = t.tratamento_id \
                         inner join dentista d on t.dentista_id = d.dentista_id \
                         inner join cliente c on t.cliente_id = c.cliente_id \
                         where p.data_realizacao >= STR_TO_DATE('" + data_inicio.toString("dd/MM/yyyy") + "', '%d/%m/%Y') and \
                         p.data_realizacao <= STR_TO_DATE('" + data_fim.toString("dd/MM/yyyy") + "', '%d/%m/%Y') \
                         order by d.nome, p.data_realizacao, p.data_efetivacao, c.nome, p.tipo");

     qx::dao::call_query(query);

     QList<caixa::detalhe_pagamento> listaFechamento;
     long quantidadeResultado = query.getSqlResultRowCount();
     caixa::detalhe_pagamento detalhe;

     for(int i = 0; i < quantidadeResultado; ++i) {
         detalhe.nome_dentista = query.getSqlResultAt(i, "nome_dentista").toString();
         detalhe.tipo_pagamento = query.getSqlResultAt(i, "tipo").toString();
         detalhe.nome_cliente = query.getSqlResultAt(i, "nome_cliente").toString();
         detalhe.valor = query.getSqlResultAt(i, "valor").toString();
         detalhe.data_realizacao = query.getSqlResultAt(i, "data_realizacao").toDate().toString("dd/MM/yyyy");
         detalhe.data_efetivacao = query.getSqlResultAt(i, "data_efetivacao").toDate().toString("dd/MM/yyyy");
         detalhe.bandeira_cartao = query.getSqlResultAt(i, "bandeira").toString();
         detalhe.autorizacao_cartao = query.getSqlResultAt(i, "autorizacao").toString();
         listaFechamento.append(detalhe);
     }
     return listaFechamento;
}

QList<caixa::detalhe_pagamento> pagamento::retornaDetalheFaturamentoPeriodo(QDate data_inicio, QDate data_fim)
{
    qx::QxSqlQuery query("select d.nome as nome_dentista, p.tipo, c.nome as nome_cliente, p.valor, p.data_realizacao, p.data_efetivacao, p.cc_bandeira as bandeira, p.cc_autorizacao as autorizacao \
                         from pagamento p \
                         inner join tratamento t on p.tratamento_id = t.tratamento_id \
                         inner join dentista d on t.dentista_id = d.dentista_id \
                         inner join cliente c on t.cliente_id = c.cliente_id \
                         where p.data_efetivacao >= STR_TO_DATE('" + data_inicio.toString("dd/MM/yyyy") + "', '%d/%m/%Y') and \
                         p.data_efetivacao <= STR_TO_DATE('" + data_fim.toString("dd/MM/yyyy") + "', '%d/%m/%Y') \
                         order by d.nome, p.data_efetivacao, c.nome, p.tipo");

     qx::dao::call_query(query);

     QList<caixa::detalhe_pagamento> listaFechamento;
     long quantidadeResultado = query.getSqlResultRowCount();
     caixa::detalhe_pagamento detalhe;

     for(int i = 0; i < quantidadeResultado; ++i) {
         detalhe.nome_dentista = query.getSqlResultAt(i, "nome_dentista").toString();
         detalhe.tipo_pagamento = query.getSqlResultAt(i, "tipo").toString();
         detalhe.nome_cliente = query.getSqlResultAt(i, "nome_cliente").toString();
         detalhe.valor = query.getSqlResultAt(i, "valor").toString();
         detalhe.data_realizacao = query.getSqlResultAt(i, "data_realizacao").toDate().toString("dd/MM/yyyy");
         detalhe.data_efetivacao = query.getSqlResultAt(i, "data_efetivacao").toDate().toString("dd/MM/yyyy");
         detalhe.bandeira_cartao = query.getSqlResultAt(i, "bandeira").toString();
         detalhe.autorizacao_cartao = query.getSqlResultAt(i, "autorizacao").toString();
         listaFechamento.append(detalhe);
     }
     return listaFechamento;
}

QList<caixa::detalhe_pagamento> pagamento::retornaDetalheFaturamentoFuturo(QDate data_corte)
{
    qx::QxSqlQuery query("select d.nome as nome_dentista, p.tipo, c.nome as nome_cliente, p.valor, p.data_realizacao, p.data_efetivacao, p.cc_bandeira as bandeira, p.cc_autorizacao as autorizacao \
                         from pagamento p \
                         inner join tratamento t on p.tratamento_id = t.tratamento_id \
                         inner join dentista d on t.dentista_id = d.dentista_id \
                         inner join cliente c on t.cliente_id = c.cliente_id \
                         where p.data_efetivacao > STR_TO_DATE('" + data_corte.toString("dd/MM/yyyy") + "', '%d/%m/%Y') \
                         order by d.nome, p.data_efetivacao, c.nome, p.tipo");

     qx::dao::call_query(query);

     QList<caixa::detalhe_pagamento> listaFechamento;
     long quantidadeResultado = query.getSqlResultRowCount();
     caixa::detalhe_pagamento detalhe;

     for(int i = 0; i < quantidadeResultado; ++i) {
         detalhe.nome_dentista = query.getSqlResultAt(i, "nome_dentista").toString();
         detalhe.tipo_pagamento = query.getSqlResultAt(i, "tipo").toString();
         detalhe.nome_cliente = query.getSqlResultAt(i, "nome_cliente").toString();
         detalhe.valor = query.getSqlResultAt(i, "valor").toString();
         detalhe.data_realizacao = query.getSqlResultAt(i, "data_realizacao").toDate().toString("dd/MM/yyyy");
         detalhe.data_efetivacao = query.getSqlResultAt(i, "data_efetivacao").toDate().toString("dd/MM/yyyy");
         detalhe.bandeira_cartao = query.getSqlResultAt(i, "bandeira").toString();
         detalhe.autorizacao_cartao = query.getSqlResultAt(i, "autorizacao").toString();
         listaFechamento.append(detalhe);
     }
     return listaFechamento;
}


} // namespace clinica
