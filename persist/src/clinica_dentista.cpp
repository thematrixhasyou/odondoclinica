#include "../include/clinica_dentista.h"
#include "../include/clinica_horarioTrabalho.h"
#include "../include/clinica_especialidade.h"
#include "../include/clinica_tratamento.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::dentista, clinica_dentista)

namespace qx {

template <>
void register_class(QxClass<clinica::dentista> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("dentista");
   t.setSoftDelete(qx::QxSoftDelete("inativo"));

   pData = t.id(& clinica::dentista::m_id, "id", 0);
   pData->setName("dentista_id");

   pData = t.data(& clinica::dentista::m_nome, "nome", 0, true, true);
   pData->setIsUnique(true);
   pData = t.data(& clinica::dentista::m_registroCro, "registroCro", 0, true, true);
   pData->setName("registro_cro");
   pData = t.data(& clinica::dentista::m_duracaoConsulta, "duracaoConsulta", 0, true, true);
   pData->setName("duracao_consulta");
   pData = t.data(& clinica::dentista::m_percentualComissao, "percentualComissao", 0, true, true);
   pData->setName("percentual_comissao");
   pData = t.data(& clinica::dentista::m_inativo, "inativo", 0, true, true);

   pRelation = t.relationOneToMany(& clinica::dentista::m_listaHorarioTrabalho, "listaHorarioTrabalho", "dentista_id", 0);
   pRelation = t.relationManyToMany(& clinica::dentista::m_listaEspecialidade, "listaEspecialidade", "dentista_especialidade", "dentista_id", "especialidade_id", 0);
   pRelation = t.relationOneToMany(& clinica::dentista::m_listaAgendamento, "listaAgendamento", "dentista_id", 0);
   pRelation = t.relationOneToMany(& clinica::dentista::m_listaTratamento, "listaTratamento", "dentista_id", 0);


   qx::QxValidatorX<clinica::dentista> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_MinLength("nome", 5);
   pAllValidator->add_MaxLength("nome", 30);
   pAllValidator->add_NotNull("nome");
   pAllValidator->add_MaxLength("registroCro", 20);
   pAllValidator->add_MinDecimal("duracaoConsulta", 10);
   pAllValidator->add_MaxDecimal("duracaoConsulta", 240);
   pAllValidator->add_NotNull("duracaoConsulta");
   pAllValidator->add_MinDecimal("percentualComissao", 0.0);
   pAllValidator->add_MaxDecimal("percentualComissao", 100.0);
   pAllValidator->add_NotNull("percentualComissao");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(dentista)

dentista::dentista() : m_id(0), m_duracaoConsulta(0), m_percentualComissao(0.0) { ; }

dentista::dentista(const long & id) : m_id(id), m_duracaoConsulta(0), m_percentualComissao(0.0) { ; }

dentista::~dentista() { ; }

long dentista::getid() const { return m_id; }

QString dentista::getnome() const { return m_nome; }

QString dentista::getregistroCro() const { return m_registroCro; }

int dentista::getduracaoConsulta() const { return m_duracaoConsulta; }

float dentista::getpercentualComissao() const { return m_percentualComissao; }

QString dentista::getinativo() const { return m_inativo; }

dentista::type_listaHorarioTrabalho dentista::getlistaHorarioTrabalho() const { return m_listaHorarioTrabalho; }

dentista::type_listaHorarioTrabalho & dentista::listaHorarioTrabalho() { return m_listaHorarioTrabalho; }

const dentista::type_listaHorarioTrabalho & dentista::listaHorarioTrabalho() const { return m_listaHorarioTrabalho; }

dentista::type_listaEspecialidade dentista::getlistaEspecialidade() const { return m_listaEspecialidade; }

dentista::type_listaEspecialidade & dentista::listaEspecialidade() { return m_listaEspecialidade; }

const dentista::type_listaEspecialidade & dentista::listaEspecialidade() const { return m_listaEspecialidade; }

dentista::type_listaAgendamento dentista::getlistaAgendamento() const { return m_listaAgendamento; }

dentista::type_listaAgendamento & dentista::listaAgendamento() { return m_listaAgendamento; }

const dentista::type_listaAgendamento & dentista::listaAgendamento() const { return m_listaAgendamento; }

dentista::type_listaTratamento dentista::getlistaTratamento() const { return m_listaTratamento; }

dentista::type_listaTratamento & dentista::listaTratamento() { return m_listaTratamento; }

const dentista::type_listaTratamento & dentista::listaTratamento() const { return m_listaTratamento; }


void dentista::setid(const long & val) { m_id = val; }

void dentista::setnome(const QString & val) { m_nome = val; }

void dentista::setregistroCro(const QString & val) { m_registroCro = val; }

void dentista::setduracaoConsulta(const int & val) { m_duracaoConsulta = val; }

void dentista::setpercentualComissao(const float & val) { m_percentualComissao = val; }

void dentista::setinativo(const QString & val) { m_inativo = val; }

void dentista::setlistaHorarioTrabalho(const dentista::type_listaHorarioTrabalho & val) { m_listaHorarioTrabalho = val; }

void dentista::setlistaEspecialidade(const dentista::type_listaEspecialidade & val) { m_listaEspecialidade = val; }

void dentista::setlistaAgendamento(const dentista::type_listaAgendamento & val) { m_listaAgendamento = val; }

void dentista::setlistaTratamento(const dentista::type_listaTratamento & val) { m_listaTratamento = val; }

dentista::type_listaHorarioTrabalho dentista::getlistaHorarioTrabalho(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaHorarioTrabalho(); }
   QString sRelation = "listaHorarioTrabalho";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaHorarioTrabalho = tmp.m_listaHorarioTrabalho; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaHorarioTrabalho;
}

dentista::type_listaHorarioTrabalho & dentista::listaHorarioTrabalho(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaHorarioTrabalho(); }
   QString sRelation = "listaHorarioTrabalho";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaHorarioTrabalho = tmp.m_listaHorarioTrabalho; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaHorarioTrabalho;
}

dentista::type_listaEspecialidade dentista::getlistaEspecialidade(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaEspecialidade(); }
   QString sRelation = "listaEspecialidade";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaEspecialidade = tmp.m_listaEspecialidade; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaEspecialidade;
}

dentista::type_listaEspecialidade & dentista::listaEspecialidade(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaEspecialidade(); }
   QString sRelation = "listaEspecialidade";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaEspecialidade = tmp.m_listaEspecialidade; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaEspecialidade;
}

dentista::type_listaAgendamento dentista::getlistaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaAgendamento(); }
   QString sRelation = "listaAgendamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaAgendamento = tmp.m_listaAgendamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaAgendamento;
}

dentista::type_listaAgendamento & dentista::listaAgendamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaAgendamento(); }
   QString sRelation = "listaAgendamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaAgendamento = tmp.m_listaAgendamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaAgendamento;
}

QString dentista::salvar(QList<long> lista_horarios_removidos)
{
    QString retorno = "Ok";
    bool update = m_id;
    QSqlError daoError = qx::dao::save_with_relation(QStringList() << relation_listaEspecialidade() << relation_listaHorarioTrabalho(), *this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else if (update) {
        for(int i = 0; i < lista_horarios_removidos.count(); ++i) {
            clinica::horarioTrabalho horario;
            horario.setid(lista_horarios_removidos.value(i));
            QSqlError daoError = qx::dao::destroy_by_id(horario);
            if(daoError.isValid()) {
                return daoError.text();
            }
        }
    }
    return retorno;
}

QString dentista::inativar()
{
    QString retorno;
    QSqlError daoError = qx::dao::delete_by_id(*this);
    if(daoError.isValid()) {
        retorno = daoError.text();
    } else {
        retorno = "Ok";
    }
    return retorno;
}

void dentista::load()
{
    qx::dao::fetch_by_id(*this);
}

bool dentista::estaNoHorarioDeTrabalho(QDateTime horario, int duracao)
{
    this->getlistaHorarioTrabalho(true);

    _foreach(QSharedPointer<clinica::horarioTrabalho> l_horarioTrabalho, m_listaHorarioTrabalho) {
        if(l_horarioTrabalho->getdiaDaSemana() == horario.date().dayOfWeek()) {
            if(l_horarioTrabalho->gethorarioInicio() <= horario.time()) {
                if(l_horarioTrabalho->gethorarioTermino() >= horario.time().addSecs(60*duracao)) {
                    return true;
                }

            }
        }
    }
    return false;
}

dentista::type_listaAgendamento dentista::getlistaAgendamentoDaSemana(QDate segundaFeira)
{
    dentista::type_listaAgendamento agendamentosDaSemana;

    qx::QxSqlQuery query("where agendamento.dentista_id = :dentista_id and agendamento.data_hora >= str_to_date('" + segundaFeira.toString("yyyy-MM-dd") + "', '%Y-%m-%d') and agendamento.data_hora <= str_to_date('" + segundaFeira.addDays(5).toString("yyyy-MM-dd") + "', '%Y-%m-%d') order by agendamento.data_hora");
    query.bind(":dentista_id", QVariant::fromValue(this->m_id));

    QSqlError daoError = qx::dao::fetch_by_query(query, agendamentosDaSemana);
    if(daoError.isValid()) {
        agendamentosDaSemana.clear();
    }
    return agendamentosDaSemana;
}

dentista::type_listaAgendamento dentista::getlistaAgendamentoDoDia(QDate dia)
{
    dentista::type_listaAgendamento agendamentosDoDia;

    qx::QxSqlQuery query("where agendamento.dentista_id = :dentista_id and agendamento.data_hora >= str_to_date('" + dia.toString("yyyy-MM-dd") + "', '%Y-%m-%d') and agendamento.data_hora < str_to_date('" + dia.addDays(1).toString("yyyy-MM-dd") + "', '%Y-%m-%d') order by agendamento.data_hora");
    query.bind(":dentista_id", QVariant::fromValue(this->m_id));

    QSqlError daoError = qx::dao::fetch_by_query_with_all_relation(query, agendamentosDoDia);
    if(daoError.isValid()) {
        agendamentosDoDia.clear();
    }
    return agendamentosDoDia;
}

dentista::type_listaHorarioTrabalho dentista::getlistaHorarioTrabalhoOrdenadoPorDiaHoraInicio()
{
    dentista::type_listaHorarioTrabalho horarios;

    qx::QxSqlQuery query("where horario_trabalho.dentista_id = :dentista_id order by horario_trabalho.dia_da_semana, horario_trabalho.horario_inicio");
    query.bind(":dentista_id", QVariant::fromValue(this->m_id));

    QSqlError daoError = qx::dao::fetch_by_query(query, horarios);
    if(daoError.isValid()) {
        horarios.clear();
    }
    return horarios;
}

dentista::type_listaTratamento dentista::getlistaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getlistaTratamento(); }
   QString sRelation = "listaTratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaTratamento = tmp.m_listaTratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaTratamento;
}

dentista::type_listaTratamento & dentista::listaTratamento(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return listaTratamento(); }
   QString sRelation = "listaTratamento";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::dentista tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_listaTratamento = tmp.m_listaTratamento; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_listaTratamento;
}

} // namespace clinica
