#include "../include/clinica_horarioTrabalho.h"
#include "../include/clinica_dentista.h"
#include "persist/include/clinica_export.h"

#include <QxOrm.h>
#include <QxMemLeak.h>

QX_REGISTER_COMPLEX_CLASS_NAME_CPP_CLINICA(clinica::horarioTrabalho, clinica_horarioTrabalho)

namespace qx {

template <>
void register_class(QxClass<clinica::horarioTrabalho> & t)
{
   qx::IxDataMember * pData = NULL; Q_UNUSED(pData);
   qx::IxSqlRelation * pRelation = NULL; Q_UNUSED(pRelation);
   qx::IxFunction * pFct = NULL; Q_UNUSED(pFct);
   qx::IxValidator * pValidator = NULL; Q_UNUSED(pValidator);

   t.setName("horario_trabalho");

   pData = t.id(& clinica::horarioTrabalho::m_id, "id", 0);
   pData->setName("horario_trabalho_id");

   pData = t.data(& clinica::horarioTrabalho::m_diaDaSemana, "diaDaSemana", 0, true, true);
   pData->setName("dia_da_semana");
   pData = t.data(& clinica::horarioTrabalho::m_horarioInicio, "horarioInicio", 0, true, true);
   pData->setName("horario_inicio");
   pData = t.data(& clinica::horarioTrabalho::m_horarioTermino, "horarioTermino", 0, true, true);
   pData->setName("horario_termino");

   pRelation = t.relationManyToOne(& clinica::horarioTrabalho::m_dentista, "dentista_id", 0);
   pRelation->getDataMember()->setName("dentista_id");

   qx::QxValidatorX<clinica::horarioTrabalho> * pAllValidator = t.getAllValidator(); Q_UNUSED(pAllValidator);
   pAllValidator->add_NotNull("diaDaSemana");
   pAllValidator->add_NotNull("horarioInicio");
   pAllValidator->add_NotNull("horarioTermino");
}

} // namespace qx

namespace clinica {

QX_PERSISTABLE_CPP(horarioTrabalho)

horarioTrabalho::horarioTrabalho() : m_id(0) { ; }

horarioTrabalho::horarioTrabalho(const long & id) : m_id(id) { ; }

horarioTrabalho::~horarioTrabalho() { ; }

long horarioTrabalho::getid() const { return m_id; }

int horarioTrabalho::getdiaDaSemana() const { return m_diaDaSemana; }

QTime horarioTrabalho::gethorarioInicio() const { return m_horarioInicio; }

QTime horarioTrabalho::gethorarioTermino() const { return m_horarioTermino; }

horarioTrabalho::type_dentista horarioTrabalho::getdentista() const { return m_dentista; }

void horarioTrabalho::setid(const long & val) { m_id = val; }

void horarioTrabalho::setdiaDaSemana(const int & val) { m_diaDaSemana = val; }

void horarioTrabalho::sethorarioInicio(const QTime & val) { m_horarioInicio = val; }

void horarioTrabalho::sethorarioTermino(const QTime & val) { m_horarioTermino = val; }

void horarioTrabalho::setdentista(const horarioTrabalho::type_dentista & val) { m_dentista = val; }

horarioTrabalho::type_dentista horarioTrabalho::getdentista(bool bLoadFromDatabase, const QString & sAppendRelations /* = QString() */, QSqlDatabase * pDatabase /* = NULL */, QSqlError * pDaoError /* = NULL */)
{
   if (pDaoError) { (* pDaoError) = QSqlError(); }
   if (! bLoadFromDatabase) { return getdentista(); }
   QString sRelation = "dentista";
   if (! sAppendRelations.isEmpty() && ! sAppendRelations.startsWith("->") && ! sAppendRelations.startsWith(">>")) { sRelation += "->" + sAppendRelations; }
   else if (! sAppendRelations.isEmpty()) { sRelation += sAppendRelations; }
   clinica::horarioTrabalho tmp;
   tmp.m_id = this->m_id;
   QSqlError daoError = qx::dao::fetch_by_id_with_relation(sRelation, tmp, pDatabase);
   if (! daoError.isValid()) { this->m_dentista = tmp.m_dentista; }
   if (pDaoError) { (* pDaoError) = daoError; }
   return m_dentista;
}

} // namespace clinica
