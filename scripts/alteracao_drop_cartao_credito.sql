-- MySQL Workbench Synchronization
-- Generated: 2018-01-30 20:08
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Vaguinho

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `clinica`.`pagamento` 
DROP FOREIGN KEY `fk_pagamento_cartao_credito1`;

ALTER TABLE `clinica`.`pagamento` 
DROP COLUMN `cartao_credito_id`,
ADD COLUMN `cc_bandeira` VARCHAR(15) NULL DEFAULT NULL COMMENT '' AFTER `data_efetivacao`,
ADD COLUMN `cc_final_numero` VARCHAR(6) NULL DEFAULT NULL COMMENT '' AFTER `cc_bandeira`,
ADD COLUMN `cc_autorizacao` VARCHAR(10) NULL DEFAULT NULL COMMENT '' AFTER `cc_final_numero`,
DROP INDEX `fk_pagamento_cartao_credito1_idx` ;

DROP TABLE IF EXISTS `clinica`.`cartao_credito` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
