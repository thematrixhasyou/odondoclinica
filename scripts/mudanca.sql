-- MySQL Workbench Synchronization
-- Generated: 2017-04-27 21:06
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Vaguinho

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `clinica`.`pagamento` 
DROP COLUMN `caixa_id`,
ADD COLUMN `caixa_id` INT(11) NOT NULL COMMENT '' AFTER `tratamento_id`,
ADD INDEX `fk_pagamento_caixa1_idx` (`caixa_id` ASC)  COMMENT '',
DROP INDEX `fk_pagamento_caixa1_idx` ;

ALTER TABLE `clinica`.`retirada` 
DROP COLUMN `caixa_id`,
ADD COLUMN `caixa_id` INT(11) NOT NULL COMMENT '' AFTER `motivo`,
ADD INDEX `fk_retirada_caixa1_idx` (`caixa_id` ASC)  COMMENT '',
DROP INDEX `fk_retirada_caixa1_idx` ;

ALTER TABLE `clinica`.`pagamento` 
DROP FOREIGN KEY `fk_pagamento_caixa1`;

ALTER TABLE `clinica`.`pagamento` ADD CONSTRAINT `fk_pagamento_caixa1`
  FOREIGN KEY (`caixa_id`)
  REFERENCES `clinica`.`caixa` (`caixa_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `clinica`.`retirada` 
ADD CONSTRAINT `fk_retirada_caixa1`
  FOREIGN KEY (`caixa_id`)
  REFERENCES `clinica`.`caixa` (`caixa_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
