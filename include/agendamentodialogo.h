#ifndef AGENDAMENTODIALOGO_H
#define AGENDAMENTODIALOGO_H

#include <QDialog>
#include <QMenu>
#include <QStandardItemModel>
#include "persist/include/clinica_agendamento.h"
#include "persist/include/clinica_especialidade.h"

namespace Ui {
class AgendamentoDialogo;
}

class AgendamentoDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit AgendamentoDialogo(QWidget *parent = 0);
    ~AgendamentoDialogo();

private:
    Ui::AgendamentoDialogo *ui;
    QStandardItemModel *m_agenda_model;
    clinica::dentista_ptr m_dentista;
    clinica::list_of_dentista m_lista_dentista;
    clinica::list_of_especialidade m_lista_especialidades;
    QModelIndex m_model_index;
    QDateTime m_novo_horario;
    QMenu* m_contextMenu;
    QAction* m_adiconarAction;
    QAction* m_removerAction;
    QAction* m_confirmarPresencaAction;
    void init();
    void configurarEventos();
    void carregarListaDentista();
    void configurarMenuContexto();
    void carregarEspecialidadesDentista();
    QDate retornaSegundaFeira(QDate dia);
    void exibeHorarioDentista();

private slots:
    void onDentistaSelecionado(int row);
    void contextMenuRequested(const QDateTime &dateTime);
    void contextMenuRequested(const QModelIndex &index);
    void removerAgendamento();
    void adicionarAgendamento();
    void confirmarPresenca();
    void alterarAgendamento(QModelIndex modelIndex);
    void carregaAgendaDentista();
    void showTooltip(QModelIndex modelIndex);

};

#endif // AGENDAMENTODIALOGO_H
