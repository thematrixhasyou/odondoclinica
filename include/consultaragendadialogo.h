#ifndef CONSULTARAGENDADIALOGO_H
#define CONSULTARAGENDADIALOGO_H

#include <QDialog>
#include "persist/include/clinica_dentista.h"

namespace Ui {
class ConsultarAgendaDialogo;
}

class ConsultarAgendaDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit ConsultarAgendaDialogo(QWidget *parent = 0);
    ~ConsultarAgendaDialogo();

private:
    Ui::ConsultarAgendaDialogo *ui;
    clinica::list_of_dentista m_lista_dentista;
    clinica::dentista_ptr m_dentista;
    QDate m_data;

    void init();
    void carregarListaDentista();
    void carregarAgenda();

private slots:
    void onDentistaSelecionado(int row);
    void onDataAlterada(QDate data);
    void imprimir();
    void setValue(const int recNo, const QString paramName, QVariant& paramValue, const int reportPage);
};

#endif // CONSULTARAGENDADIALOGO_H
