#ifndef AGENDARDIALOGO_H
#define AGENDARDIALOGO_H

#include <QDialog>
#include <QDateTime>
#include <QString>
#include <QCompleter>
#include "persist/include/clinica_especialidade.h"
#include "persist/include/clinica_cliente.h"
#include "persist/include/clinica_dentista.h"

namespace Ui {
class AgendarDialogo;
}

class AgendarDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit AgendarDialogo(QWidget *parent = 0);
    ~AgendarDialogo();
    QDateTime m_dataHora;
    clinica::dentista_ptr m_dentista;
    clinica::cliente_ptr m_cliente;
    QString m_especialidade;
    clinica::list_of_especialidade m_lista_especialidades;
    void init();

private:
    Ui::AgendarDialogo *ui;
    QCompleter *completer;
    void carregarEspecialidadesDentista();
    void configurarEventos();
    int obtemClienteID();

private slots:
    void agendar();
};

#endif // AGENDARDIALOGO_H
