#ifndef ITEMTRATAMENTODELEGATE_H
#define ITEMTRATAMENTODELEGATE_H

#include <QItemDelegate>
#include "./persist/include/clinica_procedimento.h"

class ItemTratamentoDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    ItemTratamentoDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    clinica::list_of_procedimento m_listaProcedimentos;
};

#endif // ITEMTRATAMENTODELEGATE_H
