#ifndef CAIXADIALOGO_H
#define CAIXADIALOGO_H

#include "./persist/include/clinica_caixa.h"
#include <QDialog>

namespace Ui {
class CaixaDialogo;
}

class CaixaDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit CaixaDialogo(QWidget *parent = 0);
    ~CaixaDialogo();

public slots:
    int exec();

private:
    Ui::CaixaDialogo *ui;
    clinica::caixa_ptr m_caixa;
    clinica::caixa::type_listaTotalizacao m_listaTotaisPagamentos;
    clinica::caixa::type_listaTotalizacao m_listaTotaisRetiradas;
    clinica::caixa::type_listaDetalhePagamento m_listaDetalhePagamento;
    clinica::caixa::type_listaRetirada m_listaRetirada;

    void init();
    void configurarEventos();
    void verificaSituacaoMovimento();
    void carregaTotaisMovimento();
    void limparTotaisMovimento();

private slots:
    void onDataMovimentoSelecionada();
    void abrirMovimento();
    void fecharMovimento();
    void detalharMovimento();
    void setValue(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage);

};

const int linha_dinheiro = 0;
const int linha_cheque = 1;
const int linha_cartao_debito = 2;
const int linha_cartao_credito = 3;
const int linha_total = 5;
const int linha_saldo = 7;

const int coluna_entrada = 0;
const int coluna_saida = 1;


#endif // CAIXADIALOGO_H
