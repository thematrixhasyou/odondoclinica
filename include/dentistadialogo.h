#ifndef DENTISTADIALOGO_H
#define DENTISTADIALOGO_H

#include <QDialog>
#include <QMenu>
#include <QList>
#include <QStandardItemModel>
#include "persist/include/clinica_dentista.h"

namespace Ui {
class DentistaDialogo;
}

class DentistaDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit DentistaDialogo(QWidget *parent = 0);
    ~DentistaDialogo();

private:
    Ui::DentistaDialogo *ui;
    QStandardItemModel *m_horario_model;
    clinica::dentista_ptr m_dentista;
    qx::QxCollection<long, QSharedPointer<clinica::dentista> > m_lista_dentista;
    QModelIndex m_model_index;
    QDateTime m_novo_horario;
    QMenu* m_contextMenu;
    QAction* m_adiconarAction;
    QAction* m_removerAction;
    QList<long> m_lista_horarios_removidos;
    void carregaComboEspecialidade();
    void configurarEventos();
    void init();
    void iniciarHorariosTrabalho();
    void carregarListaDentista();
    void configurarMenuContexto();
    void desmarcarTodasEspecialidades();
    void marcarEspecialidadesDentista();
    void carregarHorariosTrabalho();
    void limpar();

private slots:
    void novo();
    void salvar();
    void inativar();
    void onDentistaSelecionado(int row);
    void contextMenuRequested(const QDateTime &dateTime);
    void contextMenuRequested(const QModelIndex &index);
    void removerItemHorario();
    void adicionarItemHorario();


};

#endif // DENTISTADIALOGO_H
