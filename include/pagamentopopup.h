#ifndef PAGAMENTOPOPUP_H
#define PAGAMENTOPOPUP_H

#include <QDialog>
#include <QCompleter>
#include "./persist/include/clinica_pagamento.h"

namespace Ui {
class Pagamentopopup;
}

class Pagamentopopup : public QDialog
{
    Q_OBJECT

public:
    explicit Pagamentopopup(QWidget *parent = 0);
    ~Pagamentopopup();

public slots:
    int exec();
    int exec(clinica::tratamento_ptr tratamento);

private slots:
    void pagamentoEmDebito(bool pressed);
    void pagamentoEmCredito(bool pressed);
    void pagamentoEmDinheiro(bool pressed);
    void pagamentoEmCheque(bool pressed);
    void calcularParcelas();
    void carregarDetalhesCliente();
    void accept();

private:
    Ui::Pagamentopopup *ui;
    QCompleter *nome_completer;
    clinica::list_of_pagamento m_listaPagamento;
    clinica::list_of_dentista m_listaDentista;
    clinica::tratamento_ptr m_tratamento;
    clinica::cliente_ptr m_cliente;

    void init();
    void configurarEventos();
    void habilitarCartaoCredito(bool habilitar);
    bool validarPagamento();
    bool validarCartaoCreditoUltimosDigitos();
    bool validarCartaoCreditoAutorizacao();
    void criarListaPagamento();
    void carregarListaDentista();
    int obterClienteID();
    void salvarPagamento();
    void carregarTratamento();
};

#endif // PAGAMENTOPOPUP_H
