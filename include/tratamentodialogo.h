#ifndef TRATAMENTODIALOGO_H
#define TRATAMENTODIALOGO_H

#include <QDialog>
#include <QCompleter>
#include "persist/include/clinica_cliente.h"
#include "persist/include/clinica_dentista.h"
#include "include/tratamentopopup.h"
#include "include/itemtratamentopopup.h"
#include "include/pagamentopopup.h"

namespace Ui {
class TratamentoDialogo;
}

class TratamentoDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit TratamentoDialogo(QWidget *parent = 0);
    ~TratamentoDialogo();

private:
    Ui::TratamentoDialogo *ui;
    QCompleter *completer;
    clinica::cliente_ptr m_cliente;
    clinica::tratamento_ptr m_tratamento;
    clinica::list_of_tratamento m_lista_tratamento;
    clinica::list_of_item_tratamento m_lista_item_tratamento;
    clinica::list_of_pagamento m_lista_pagamento;
    double m_valor_tratamento, m_valor_pagamento, m_valor_pagar;
    TratamentoPopup *m_tratamentoPopup;
    ItemTratamentoPopup *m_itemTratamentoPopup;
    Pagamentopopup *m_pagamentoPopup;

    void init();
    int obterClienteID();
    void configurarEventos();

    void carregarTratamentos();
    void carregarPagamentos();
    void atualizarValoresTotais();
    bool validarSalvar();
    bool validarEncerrarTratamento();
    bool tratamentoEmAndamento();
    bool todosItensConcluidos();
    bool existeTratamentoEmAndamento();
    bool existeTratamentoMaisRecente();

    void limparExcetoCliente();
    void atualizaSituacaoBotoes();
    void salvarTratatamento();
    void salvarItemTratamento(clinica::item_tratamento_ptr itemTratamento);
    void excluirItemTratamento(clinica::item_tratamento_ptr itemTratamento);
    void excluirPagamento(clinica::pagamento_ptr pagamento);
    void limparDetalhes();
    bool isPagamentoParcelado(clinica::pagamento_ptr pagamento);
    void excluirTodasParcelas(clinica::pagamento_ptr pagamento);
    
private slots:
    void carregarItensTratamento();
    void carregarDetalhesCliente();

    void novoItemTratamento();
    void editarItemTratamento(QModelIndex model);
    void removerItemTratamento();

    void novoPagamento();
    void removerPagamento();

    void novoTratamento();
    void encerrarTratamento();
    void reabrirTratamento();
    void imprimirTratamento();
    void setValoresImpressao(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage);

    void limpar();
};

#endif // TRATAMENTODIALOGO_H
