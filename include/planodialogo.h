#ifndef PLANODIALOGO_H
#define PLANODIALOGO_H

#include <QDialog>
#include "persist/include/clinica_plano_odontologico.h"

namespace Ui {
class PlanoDialogo;
}

class PlanoDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit PlanoDialogo(QWidget *parent = 0);
    ~PlanoDialogo();

private:
    Ui::PlanoDialogo *ui;
    clinica::plano_odontologico_ptr m_plano;
    qx::QxCollection<long, QSharedPointer<clinica::plano_odontologico> > m_lista_planos;
    void configurarEventos();
    void carregaListaPlano();
    bool eventFilter(QObject *obj, QEvent *ev);
    void onListaRecebeFoco();
    void limpar();

private slots:
    void novo();
    void inativar();
    void salvar();
    void onPlanoSelecionado(int row);

};

#endif // PLANODIALOGO_H
