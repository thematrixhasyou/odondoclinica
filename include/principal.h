#ifndef PRINCIPAL_H
#define PRINCIPAL_H

#include <QMainWindow>
#include "persist/include/clinica_tratamento.h"

namespace Ui {
class Principal;
}

class Principal : public QMainWindow
{
    Q_OBJECT

public:
    explicit Principal(QWidget *parent = 0);
    ~Principal();

private:
    Ui::Principal *ui;
    QList<clinica::tratamento::detalhe_tratamento_devedor> m_listaTratamentoDevedor;
    void init();
    void readSettings();
    void writeSettings();
    void connectDatabase();

protected:
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private slots:
    void about();
    void cliente();
    void plano();
    void dentista();
    void procedimento();
    void agendamento();
    void consultarAgenda();
    void cadastroTratamento();
    void caixaMovimento();
    void caixaRetirada();
    void caixaPagamento();
    void relatorioFechamentoConsolidado();
    void relatorioFechamentoDetalhado();
    void relatorioTratamentoDevedor();
    void setValoresRelTratamentoDevedor(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage);

};

#endif // PRINCIPAL_H
