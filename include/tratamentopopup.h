#ifndef TRATAMENTOPOPUP_H
#define TRATAMENTOPOPUP_H

#include <QDialog>
#include "persist/include/clinica_dentista.h"

namespace Ui {
class TratamentoPopup;
}

enum tipoAcao { iniciar, encerrar };

class TratamentoPopup : public QDialog
{
    Q_OBJECT

public:
    explicit TratamentoPopup(QWidget *parent = 0);
    ~TratamentoPopup();
    clinica::dentista_ptr getDentistaSelecionado();
    QDate getDataSelecionada();

private slots:
    void accept();

public slots:
    int exec(tipoAcao acao);

private:
    Ui::TratamentoPopup *ui;

    clinica::list_of_dentista m_lista_dentista;
    tipoAcao m_acao;
    void init();
    void carregarListaDentista();
};

#endif // TRATAMENTOPOPUP_H
