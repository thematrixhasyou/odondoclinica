#ifndef PROCEDIMENTODIALOGO_H
#define PROCEDIMENTODIALOGO_H

#include <QDialog>
#include "persist/include/clinica_procedimento.h"

namespace Ui {
class ProcedimentoDialogo;
}

class ProcedimentoDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit ProcedimentoDialogo(QWidget *parent = 0);
    ~ProcedimentoDialogo();

private:
    Ui::ProcedimentoDialogo *ui;
    clinica::procedimento_ptr m_procedimento;
    qx::QxCollection<long, QSharedPointer<clinica::procedimento> > m_lista_procedimentos;
    void configurarEventos();
    void carregaListaProcedimento();
    bool eventFilter(QObject *obj, QEvent *ev);
    void onListaRecebeFoco();
    void limpar();

private slots:
    void novo();
    void inativar();
    void salvar();
    void onProcedimentoSelecionado(int row);
};

#endif // PROCEDIMENTODIALOGO_H
