#ifndef RELATORIOFECHAMENTODIALOGO_H
#define RELATORIOFECHAMENTODIALOGO_H

#include "./persist/include/clinica_pagamento.h"
#include <QDialog>

namespace Ui {
class RelatorioFechamentoDialogo;
}

class RelatorioFechamentoDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit RelatorioFechamentoDialogo(QWidget *parent = 0);
    ~RelatorioFechamentoDialogo();

public slots:
    int exec();

private:
    Ui::RelatorioFechamentoDialogo *ui;
    QList<clinica::pagamento::detalhe_fechamento_periodo> m_listaDetalhe;

    void init();
    void configurarEventos();

private slots:
    void setValue(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage);
    void imprimir();
};

#endif // RELATORIOFECHAMENTODIALOGO_H
