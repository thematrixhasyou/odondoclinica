#ifndef RETIRADADIALOGO_H
#define RETIRADADIALOGO_H

#include <QDialog>

namespace Ui {
class RetiradaDialogo;
}

class RetiradaDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit RetiradaDialogo(QWidget *parent = 0);
    ~RetiradaDialogo();

public slots:
    int exec();

private:
    Ui::RetiradaDialogo *ui;

    void init();
    void configurarEventos();
    bool validaSalvar();

private slots:
    void salvar();

};

#endif // RETIRADADIALOGO_H
