#ifndef RELATORIOFECHAMENTODETALHADODIALOGO_H
#define RELATORIOFECHAMENTODETALHADODIALOGO_H

#include "./persist/include/clinica_caixa.h"
#include "./persist/include/clinica_pagamento.h"
#include <QDialog>

namespace Ui {
class RelatorioFechamentoDetalhadoDialogo;
}

class RelatorioFechamentoDetalhadoDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit RelatorioFechamentoDetalhadoDialogo(QWidget *parent = 0);
    ~RelatorioFechamentoDetalhadoDialogo();

public slots:
    int exec();

private:
    Ui::RelatorioFechamentoDetalhadoDialogo *ui;
    QList<clinica::caixa::detalhe_pagamento> m_listaDetalhe;

    void init();
    void configurarEventos();

private slots:
    void setValue(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage);
    void imprimir();
};

#endif // RELATORIOFECHAMENTODETALHADODIALOGO_H
