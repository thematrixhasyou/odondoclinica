#ifndef CLIENTEDIALOGO_H
#define CLIENTEDIALOGO_H

#include <QDialog>
#include "persist/include/clinica_cliente.h"

namespace Ui {
class ClienteDialogo;
}

class ClienteDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit ClienteDialogo(QWidget *parent = 0);
    ~ClienteDialogo();

private:
    Ui::ClienteDialogo *ui;
    clinica::cliente_ptr m_cliente;
    qx::QxCollection<long, QSharedPointer<clinica::cliente> > m_lista_cliente;
    void limparCliente();
    void carregaComboUF();
    void carregaComboSexo();
    void carregaComboPlano();
    void configurarEventos();
    void carregarComboBoxes();
    bool eventFilter(QObject *obj, QEvent *ev);
    bool isEmptyCPF(QString cpf);

private slots:
    void novo();
    void salvar();
    void inativar();
    void onNomePesquisaKeyPress(QString text);
    void limpar();
    void onResultadoSelecionado(int row);
    void onResultadoRecebeFoco();

};

#endif // CLIENTEDIALOGO_H
