#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

//! [0]
class SobreDialogo : public QDialog
{
    Q_OBJECT

public:
    explicit SobreDialogo(QWidget *parent = 0);
};
//! [0]

#endif
