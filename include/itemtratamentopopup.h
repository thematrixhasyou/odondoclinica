#ifndef ITEMTRATAMENTOPOPUP_H
#define ITEMTRATAMENTOPOPUP_H

#include <QDialog>
#include <QString>
#include "./persist/include/clinica_procedimento.h"

namespace Ui {
class ItemTratamentoPopup;
}

class ItemTratamentoPopup : public QDialog
{
    Q_OBJECT

public:
    explicit ItemTratamentoPopup(QWidget *parent = 0);
    ~ItemTratamentoPopup();
    clinica::procedimento_ptr getProcedimentoSelecionado();
    QString getSituacaoSelecionada();
    long getQuantidadeSelecionada();
    double getValorUnitarioSelecionado();

public slots:
    int exec();
    int exec(clinica::procedimento_ptr procedimento, long quantidade, double valorUnitario, QString situacao);

private slots:
    void accept();
    void calcularValorItem();

private:
    Ui::ItemTratamentoPopup *ui;
    clinica::list_of_procedimento m_listaProcedimentos;
    clinica::procedimento_ptr m_procedimento;
    QString m_situacao;
    long m_quantidade;
    double m_valorUnitario;

    void init();
    void configurarEventos();

};

#endif // ITEMTRATAMENTOPOPUP_H
