QT       += core gui sql widgets xml script printsupport
CONFIG   += QXT
QXT      += core gui

DEFINES  += _QX_ENABLE_BOOST

TARGET = clinica
TEMPLATE = app
INCLUDEPATH += ../../libs/QxOrm/include
INCLUDEPATH += ../../libs/boost_1_57/include
INCLUDEPATH += ../../libs/libqxt_adapted/include/QxtWidgets
INCLUDEPATH += ../../libs/libqxt_adapted/include/QxtCore
INCLUDEPATH += ../../libs/QtRptProject_2.0.1/QtRPT
INCLUDEPATH += ../../libs/QtRptProject_2.0.1/CommonFiles

SOURCES = ./src/main.cpp
SOURCES += ./src/relatoriofechamentodetalhadodialogo.cpp
SOURCES += ./src/relatoriofechamentodialogo.cpp
SOURCES += ./src/retiradadialogo.cpp
SOURCES += ./src/caixadialogo.cpp
SOURCES += ./src/pagamentopopup.cpp
SOURCES += ./src/tratamentopopup.cpp
SOURCES += ./src/itemtratamentopopup.cpp
SOURCES += ./src/tratamentodialogo.cpp
SOURCES += ./src/itemtratamentodelegate.cpp
SOURCES += ./src/procedimentodialogo.cpp
SOURCES += ./src/agendamentodialogo.cpp
SOURCES += ./src/agendardialogo.cpp
SOURCES += ./src/consultaragendadialogo.cpp
SOURCES += ./src/principal.cpp
SOURCES += ./src/sobredialogo.cpp
SOURCES += ./src/wigglywidget.cpp
SOURCES += ./src/clientedialogo.cpp
SOURCES += ./src/planodialogo.cpp
SOURCES += ./src/dentistadialogo.cpp
SOURCES += ./persist/src/clinica_cliente.cpp
SOURCES += ./persist/src/clinica_plano_odontologico.cpp
SOURCES += ./persist/src/clinica_dentista.cpp
SOURCES += ./persist/src/clinica_especialidade.cpp
SOURCES += ./persist/src/clinica_horarioTrabalho.cpp
SOURCES += ./persist/src/clinica_agendamento.cpp
SOURCES += ./persist/src/clinica_item_tratamento.cpp
SOURCES += ./persist/src/clinica_pagamento.cpp
SOURCES += ./persist/src/clinica_procedimento.cpp
SOURCES += ./persist/src/clinica_tratamento.cpp
SOURCES += ./persist/src/clinica_caixa.cpp
SOURCES += ./persist/src/clinica_retirada.cpp

HEADERS = ./include/principal.h
HEADERS += ./include/relatoriofechamentodetalhadodialogo.h
HEADERS += ./include/relatoriofechamentodialogo.h
HEADERS += ./include/retiradadialogo.h
HEADERS += ./include/caixadialogo.h
HEADERS += ./include/pagamentopopup.h
HEADERS += ./include/tratamentopopup.h
HEADERS += ./include/itemtratamentopopup.h
HEADERS += ./include/tratamentodialogo.h
HEADERS += ./include/itemtratamentodelegate.h
HEADERS += ./include/procedimentodialogo.h
HEADERS += ./include/agendamentodialogo.h
HEADERS += ./include/agendardialogo.h
HEADERS += ./include/consultaragendadialogo.h
HEADERS += ./include/sobredialogo.h
HEADERS += ./include/wigglywidget.h
HEADERS += ./include/clientedialogo.h
HEADERS += ./include/planodialogo.h
HEADERS += ./include/dentistadialogo.h
HEADERS += ./persist/include/clinica_export.h
HEADERS += ./persist/include/clinica_cliente.h
HEADERS += ./persist/include/clinica_plano_odontologico.h
HEADERS += ./persist/include/clinica_dentista.h
HEADERS += ./persist/include/clinica_especialidade.h
HEADERS += ./persist/include/clinica_horarioTrabalho.h
HEADERS += ./persist/include/clinica_agendamento.h
HEADERS += ./persist/include/clinica_item_tratamento.h
HEADERS += ./persist/include/clinica_pagamento.h
HEADERS += ./persist/include/clinica_procedimento.h
HEADERS += ./persist/include/clinica_tratamento.h
HEADERS += ./persist/include/clinica_caixa.h
HEADERS += ./persist/include/clinica_retirada.h

FORMS   = ./forms/principal.ui
FORMS   += ./forms/relatoriofechamentodetalhadodialogo.ui
FORMS   += ./forms/relatoriofechamentodialogo.ui
FORMS   += ./forms/retiradadialogo.ui
FORMS   += ./forms/caixadialogo.ui
FORMS   += ./forms/popups/pagamentopopup.ui
FORMS   += ./forms/tratamentodialogo.ui
FORMS   += ./forms/procedimentodialogo.ui
FORMS   += ./forms/agendamentodialogo.ui
FORMS   += ./forms/agendardialogo.ui
FORMS   += ./forms/consultaragendadialogo.ui
FORMS   += ./forms/clientedialogo.ui
FORMS   += ./forms/planodialogo.ui
FORMS   += ./forms/dentistadialogo.ui
FORMS   += ./forms/popups/tratamentopopup.ui
FORMS   += ./forms/popups/itemtratamentopopup.ui


LIBS += -L"../../libs/QxOrm/lib"
LIBS += -L"../../libs/boost_1_57/lib_shared"
LIBS += -L"../../libs/libqxt_adapted/bin"
LIBS += -L"../../libs/QtRptProject_2.0.1/bin"

CONFIG(debug, debug|release) {
LIBS += -l"QxtCored"
LIBS += -l"QxtWidgetsd"
LIBS += -l"QxOrmd"
LIBS += -l"libboost_serialization-mgw49-mt-d-1_57"
LIBS += -l"QtRPT"
} else {
LIBS += -l"QxtCore"
LIBS += -l"QxtWidgets"
LIBS += -l"QxOrm"
LIBS += -l"libboost_serialization-mgw49-mt-1_57"
LIBS += -l"QtRPT"
} # CONFIG(debug, debug|release)

RESOURCES += \
    resources/cartao_credito.qrc

