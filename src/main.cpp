#include "include/principal.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("Clínica Odontológica L+");
    app.setOrganizationName("SoftMaker");
    Principal w;
    w.setWindowFlags(Qt::Window);
    w.show();

    return app.exec();
}
