#include ".\include\retiradadialogo.h"
#include "ui_retiradadialogo.h"

#include <QMessageBox>
#include "persist/include/clinica_retirada.h"
#include "persist/include/clinica_caixa.h"

RetiradaDialogo::RetiradaDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RetiradaDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    configurarEventos();
}

RetiradaDialogo::~RetiradaDialogo()
{
    delete ui;
}

int RetiradaDialogo::exec()
{
    init();
    return QDialog::exec();
}

void RetiradaDialogo::init()
{
    ui->datMovimento->setDate(QDate::currentDate());
    ui->dblValor->setValue(100.00);
    ui->cmbTipoRetirada->setCurrentIndex(-1);
    ui->edtAutor->setText("");
    ui->txtMotivo->setDocument(new QTextDocument(""));
}

void RetiradaDialogo::configurarEventos()
{
    connect(ui->btnCancelar, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnSalvar, SIGNAL(clicked()), this, SLOT(salvar()));
}

bool RetiradaDialogo::validaSalvar()
{
    clinica::caixa_ptr caixa;
    if(!caixa->isMovimentoAberto(QDate::currentDate())) {
        QMessageBox::information(this, "Salvar", "O Caixa não está aberto para o movimento de: " + QDate::currentDate().toString("dd/MM/yyyy"));
        return false;
    }
    if(ui->cmbTipoRetirada->currentIndex() < 0) {
        QMessageBox::information(this, "Salvar", "Informe o tipo da retirada.");
        return false;
    }
    if(ui->dblValor->value() < 0.5) {
        QMessageBox::information(this, "Salvar", "Informe o valor da retirada.");
        return false;
    }
    if(ui->edtAutor->text().trimmed().isEmpty()) {
        QMessageBox::information(this, "Salvar", "Informe o autor da retirada.");
        return false;
    }
}

void RetiradaDialogo::salvar()
{
    if(validaSalvar()) {
        clinica::retirada_ptr retirada;
        retirada.reset(new clinica::retirada());
        clinica::caixa_ptr caixa;
        caixa = caixa->retornaMovimento(QDate::currentDate());

        retirada->setcaixa(caixa);
        retirada->settipo((ui->cmbTipoRetirada->currentIndex() == 0 ? "DIN" : "CHE"));
        retirada->setvalor(ui->dblValor->value());
        retirada->setautor(ui->edtAutor->text());
        retirada->setmotivo(ui->txtMotivo->toPlainText());

        QString retorno = retirada->salvar();
        if(retorno.compare("Ok") != 0) {
            QMessageBox::critical(this, "Salvar", "Erro ao salvar a retirada.\n" + retorno);
        } else {
            QMessageBox::information(this, "Salvar", "Retirada salva com sucesso.");
            QDialog::close();
        }
    }
}
