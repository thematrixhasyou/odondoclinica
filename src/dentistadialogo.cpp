#include <QtWidgets>
#include <QStandardItemModel>
#include "include/dentistadialogo.h"
#include "persist/include/clinica_horarioTrabalho.h"
#include "ui_dentistadialogo.h"

DentistaDialogo::DentistaDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DentistaDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    init();
}

DentistaDialogo::~DentistaDialogo()
{
    delete ui;
}

void DentistaDialogo::carregaComboEspecialidade()
{
    ui->especialidadeCmb->clear();

    clinica::list_of_especialidade lista_especialidades = clinica::especialidade::retornaTodasEspecialidadesOrdenadasPorDescricao();
    if(lista_especialidades.count() > 0) {
        _foreach(QSharedPointer<clinica::especialidade> especialidade, lista_especialidades) {
            ui->especialidadeCmb->addItem(especialidade->getdescricao(), QVariant::fromValue(especialidade->getid()));
        }
    }
    ui->especialidadeCmb->setCurrentIndex(-1);
}

void DentistaDialogo::configurarEventos()
{
    connect(ui->fecharBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->novoBtn, SIGNAL(clicked()), this, SLOT(novo()));
    connect(ui->salvarBtn, SIGNAL(clicked()), this, SLOT(salvar()));
    connect(ui->inativarBtn, SIGNAL(clicked()), this, SLOT(inativar()));
    connect(ui->dentistaLst, SIGNAL(currentRowChanged(int)), this, SLOT(onDentistaSelecionado(int)));
    connect(ui->horarioScd, SIGNAL(contextMenuRequested(QDateTime)), this, SLOT(contextMenuRequested(QDateTime)));
    connect(ui->horarioScd, SIGNAL(contextMenuRequested(QModelIndex)), this, SLOT(contextMenuRequested(QModelIndex)));
    connect(m_removerAction, SIGNAL(triggered()), this, SLOT(removerItemHorario()));
    connect(m_adiconarAction, SIGNAL(triggered()), this, SLOT(adicionarItemHorario()));
}

void DentistaDialogo::configurarMenuContexto()
{
    m_contextMenu = new QMenu(ui->horarioScd);
    ui->horarioScd->setContextMenuPolicy(Qt::ActionsContextMenu);
    m_adiconarAction = new QAction("Adicionar", m_contextMenu);
    m_removerAction = new QAction("Remover", m_contextMenu);
    ui->horarioScd->addAction(m_adiconarAction);
    ui->horarioScd->addAction(m_removerAction);
}

void DentistaDialogo::init()
{
    configurarMenuContexto();
    carregaComboEspecialidade();
    configurarEventos();
    carregarListaDentista();
}

void DentistaDialogo::iniciarHorariosTrabalho()
{
    m_horario_model = new QStandardItemModel(/*de segunda a sexta dois periodos e sabado um periodo*/ 11, 1, ui->horarioScd);
    QDateTime time = QDateTime(QDate(2015,11,16),QTime(8,0,0));
    ui->horarioScd->setViewMode(QxtScheduleView::CustomView);
    ui->horarioScd->setCurrentZoomDepth(30, Qxt::Minute);
    ui->horarioScd->setDateRange(time.date(),time.date().addDays(6));

    int row = 0;
    for(int i = 0; i < 6; ++i) {
        time = QDateTime(QDate(2015,11,16+i),QTime(8,30,0));
        QStandardItem *itemManha = new QStandardItem();

        itemManha->setData(time.toTime_t(), Qxt::ItemStartTimeRole);
        itemManha->setData(3.5*60*60, Qxt::ItemDurationRole);
        itemManha->setData(QColor(0,0,255),Qt::BackgroundRole);

        m_horario_model->setItem(row++, itemManha);

        if(i < 5) {
            time = QDateTime(QDate(2015,11,16+i),QTime(14,00,0));
            QStandardItem *itemTarde = new QStandardItem();

            itemTarde->setData(time.toTime_t(), Qxt::ItemStartTimeRole);
            itemTarde->setData(4.5*60*60, Qxt::ItemDurationRole);
            itemTarde->setData(QColor(0,0,255),Qt::BackgroundRole);

            m_horario_model->setItem(row++, itemTarde);
        }
    }

    ui->horarioScd->setModel(m_horario_model, true);
}

void DentistaDialogo::carregarListaDentista()
{
    ui->dentistaLst->clear();

    m_lista_dentista = clinica::dentista::retornaTodosDentistasOrdenadosPorNome();
    if(m_lista_dentista.count() > 0) {
        _foreach(QSharedPointer<clinica::dentista> dentista, m_lista_dentista) {
            ui->dentistaLst->addItem(dentista->getnome());
        }
        ui->dentistaLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

void DentistaDialogo::novo()
{
    m_dentista.reset(new clinica::dentista());
    limpar();
    ui->nomeEdt->setFocus();
}

void DentistaDialogo::salvar()
{
    m_dentista->setnome(ui->nomeEdt->text());
    m_dentista->setregistroCro(ui->croEdt->text());
    m_dentista->setduracaoConsulta(ui->duracaoSpn->value());
    m_dentista->setpercentualComissao(ui->comissaoSpn->value());

    clinica::dentista::type_listaEspecialidade listaEspecialidade;
    for(int i = 0; i < ui->especialidadeCmb->count(); ++i) {
        if(ui->especialidadeCmb->itemCheckState(i) == Qt::Checked) {
            clinica::especialidade_ptr especialidade;
            especialidade.reset(new clinica::especialidade());
            especialidade->setid(ui->especialidadeCmb->itemData(i).toInt());
            especialidade->setdescricao(ui->especialidadeCmb->itemText(i));
            listaEspecialidade.insert(especialidade->getid(), especialidade);
        }
    }
    m_dentista->setlistaEspecialidade(listaEspecialidade);

    clinica::dentista::type_listaHorarioTrabalho listaHorarioTrabalho;
    for(int i = 0; i < m_horario_model->rowCount(); ++i) {
        clinica::horarioTrabalho_ptr horario;
        horario.reset(new clinica::horarioTrabalho());
        horario->setid(m_horario_model->item(i)->data(Qxt::UserRole).toInt());
        horario->setdentista(m_dentista);
        horario->sethorarioInicio(QDateTime::fromTime_t(m_horario_model->item(i)->data(Qxt::ItemStartTimeRole).toInt()).time());
        horario->sethorarioTermino(QDateTime::fromTime_t(m_horario_model->item(i)->data(Qxt::ItemStartTimeRole).toInt()+m_horario_model->item(i)->data(Qxt::ItemDurationRole).toInt()).time());
        horario->setdiaDaSemana(QDateTime::fromTime_t(m_horario_model->item(i)->data(Qxt::ItemStartTimeRole).toInt()).date().dayOfWeek());
        listaHorarioTrabalho.insert(i, horario);
    }
    m_dentista->setlistaHorarioTrabalho(listaHorarioTrabalho);

    QString retorno = m_dentista->salvar(m_lista_horarios_removidos);
    if(retorno.compare("Ok") == 0) {
        QMessageBox::information(this, "Salvar", "Dentista salvo com sucesso.");
        limpar();
        carregarListaDentista();
    } else {
        QMessageBox::critical(this, "Salvar", "Erro ao salvar Dentista.\n" + retorno);
    }
}

void DentistaDialogo::inativar()
{
    if(QMessageBox::question(this, "Inativar", "O dados do dentista não ficarão mais disponíveis.\nConfirma inativar o dentista?") == QMessageBox::Yes) {
        QString retorno = m_dentista->inativar();
        if(retorno.compare("Ok") == 0) {
            QMessageBox::information(this, "Inativar", "Dentista inativado com sucesso.");
            limpar();
            carregarListaDentista();
        } else {
            QMessageBox::critical(this, "Inativar", "Erro ao inativar Dentista.\n" + retorno);
        }
    }
}

void DentistaDialogo::limpar()
{
    ui->nomeEdt->setText("");
    ui->croEdt->setText("");
    ui->duracaoSpn->setValue(15);
    ui->comissaoSpn->setValue(0);
    carregaComboEspecialidade();
    iniciarHorariosTrabalho();
}

void DentistaDialogo::desmarcarTodasEspecialidades()
{
    for(int i = 0; i < ui->especialidadeCmb->count(); ++i) {
        ui->especialidadeCmb->setItemCheckState(i, Qt::Unchecked);
    }
}

void DentistaDialogo::marcarEspecialidadesDentista()
{
    if(m_dentista->getlistaEspecialidade(true).count() > 0) {
        QStringList dentistaEspecialidade;

        _foreach(QSharedPointer<clinica::especialidade> especialidade, m_dentista->getlistaEspecialidade()) {
            dentistaEspecialidade.append(especialidade->getdescricao());
        }

        ui->especialidadeCmb->setCheckedItems(dentistaEspecialidade);
    }
}

void DentistaDialogo::carregarHorariosTrabalho()
{
    int periodos = m_dentista->getlistaHorarioTrabalho(true).count();
    m_horario_model = new QStandardItemModel(periodos, 1, ui->horarioScd);
    QDateTime time = QDateTime(QDate(2015,11,16),QTime(8,0,0));
    ui->horarioScd->setViewMode(QxtScheduleView::CustomView);
    ui->horarioScd->setCurrentZoomDepth(30, Qxt::Minute);
    ui->horarioScd->setDateRange(time.date(),time.date().addDays(6));

    int i = 0;
    _foreach(QSharedPointer<clinica::horarioTrabalho> horario, m_dentista->getlistaHorarioTrabalho()) {

        QStandardItem *itemHorario = new QStandardItem();
        time = QDateTime(QDate(2015,11,15 + horario->getdiaDaSemana()), horario->gethorarioInicio());

        itemHorario->setData(time.toTime_t(), Qxt::ItemStartTimeRole);
        itemHorario->setData(horario->gethorarioInicio().secsTo(horario->gethorarioTermino()), Qxt::ItemDurationRole);
        itemHorario->setData(QColor(0,0,255),Qt::BackgroundRole);
        itemHorario->setData((int)horario->getid(), Qxt::UserRole);

        m_horario_model->setItem(i++, itemHorario);
    }

    m_lista_horarios_removidos.clear();
    ui->horarioScd->setModel(m_horario_model, true);
}

void DentistaDialogo::onDentistaSelecionado(int row)
{
    if(row >= 0 && row < m_lista_dentista.count()) {
        m_dentista = m_lista_dentista.getByIndex(row);

        ui->nomeEdt->setText(m_dentista->getnome());
        ui->croEdt->setText(m_dentista->getregistroCro());
        ui->duracaoSpn->setValue(m_dentista->getduracaoConsulta());
        ui->comissaoSpn->setValue(m_dentista->getpercentualComissao());

        desmarcarTodasEspecialidades();
        marcarEspecialidadesDentista();
        carregarHorariosTrabalho();
    }
}

void DentistaDialogo::contextMenuRequested(const QDateTime &dateTime)
{
    m_novo_horario = dateTime;
    ui->horarioScd->addAction(m_adiconarAction);
    ui->horarioScd->removeAction(m_removerAction);
}

void DentistaDialogo::contextMenuRequested(const QModelIndex &index)
{
    m_model_index = index;
    ui->horarioScd->removeAction(m_adiconarAction);
    ui->horarioScd->addAction(m_removerAction);
}

void DentistaDialogo::removerItemHorario()
{
    m_lista_horarios_removidos.append(m_model_index.data(Qxt::UserRole).toInt());
    m_horario_model->removeRow(m_model_index.row());
    ui->horarioScd->setModel(m_horario_model, true);
}

void DentistaDialogo::adicionarItemHorario()
{
    if(m_novo_horario.time().hour() >= 8 && m_novo_horario.time().hour() < 14) {
        QDateTime time = QDateTime(m_novo_horario.date(), QTime(8,30,0));
        QStandardItem *itemManha = new QStandardItem();

        itemManha->setData(time.toTime_t(), Qxt::ItemStartTimeRole);
        itemManha->setData(3.5*60*60, Qxt::ItemDurationRole);
        itemManha->setData(QColor(0,0,255),Qt::BackgroundRole);

        m_horario_model->appendRow(itemManha);

    } else if(m_novo_horario.time().hour() >= 14 && m_novo_horario.time().hour() < 19) {
        QDateTime time = QDateTime(m_novo_horario.date(), QTime(14,00,0));
        QStandardItem *itemTarde = new QStandardItem();

        itemTarde->setData(time.toTime_t(), Qxt::ItemStartTimeRole);
        itemTarde->setData(4.5*60*60, Qxt::ItemDurationRole);
        itemTarde->setData(QColor(0,0,255),Qt::BackgroundRole);

        m_horario_model->appendRow(itemTarde);
    }
    ui->horarioScd->setModel(m_horario_model, true);
}
