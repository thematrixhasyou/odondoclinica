#include <QtWidgets>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QPoint>
#include <QToolTip>
#include "include/agendamentodialogo.h"
#include "include/agendardialogo.h"
#include "persist/include/clinica_agendamento.h"
#include "ui_agendamentodialogo.h"

AgendamentoDialogo::AgendamentoDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AgendamentoDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    ui->horarioTrw->clear();
    ui->horarioTrw->setColumnCount(1);
    ui->horarioTrw->setHeaderLabel(QString("Dia/Horario"));
    init();
}

AgendamentoDialogo::~AgendamentoDialogo()
{
    delete ui;
}

void AgendamentoDialogo::init()
{
    configurarMenuContexto();
    configurarEventos();
    carregarListaDentista();
}

void AgendamentoDialogo::configurarEventos()
{
    connect(ui->fecharBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->dentistaLst, SIGNAL(currentRowChanged(int)), this, SLOT(onDentistaSelecionado(int)));
    connect(ui->agendaWgt, SIGNAL(contextMenuRequested(QDateTime)), this, SLOT(contextMenuRequested(QDateTime)));
    connect(ui->agendaWgt, SIGNAL(contextMenuRequested(QModelIndex)), this, SLOT(contextMenuRequested(QModelIndex)));
    connect(ui->agendaWgt, SIGNAL(itemMoved(QModelIndex)), this, SLOT(alterarAgendamento(QModelIndex)));
    connect(m_removerAction, SIGNAL(triggered()), this, SLOT(removerAgendamento()));
    connect(m_adiconarAction, SIGNAL(triggered()), this, SLOT(adicionarAgendamento()));
    connect(m_confirmarPresencaAction, SIGNAL(triggered()), this, SLOT(confirmarPresenca()));
    connect(ui->calendarioWgt, SIGNAL(selectionChanged()), this, SLOT(carregaAgendaDentista()));
    connect(ui->agendaWgt, SIGNAL(indexSelected(QModelIndex)), this, SLOT(showTooltip(QModelIndex)));
}

void AgendamentoDialogo::carregarListaDentista()
{
    ui->dentistaLst->clear();

    m_lista_dentista = clinica::dentista::retornaTodosDentistasOrdenadosPorNome();
    if(m_lista_dentista.count() > 0) {
        _foreach(QSharedPointer<clinica::dentista> dentista, m_lista_dentista) {
            ui->dentistaLst->addItem(dentista->getnome());
        }
        ui->dentistaLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

void AgendamentoDialogo::configurarMenuContexto()
{
    m_contextMenu = new QMenu(ui->agendaWgt);
    ui->agendaWgt->setContextMenuPolicy(Qt::ActionsContextMenu);
    m_adiconarAction = new QAction("Adicionar", m_contextMenu);
    m_removerAction = new QAction("Remover", m_contextMenu);
    m_confirmarPresencaAction = new QAction("Compareceu", m_contextMenu);
    ui->agendaWgt->addAction(m_adiconarAction);
    ui->agendaWgt->addAction(m_removerAction);
    ui->agendaWgt->addAction(m_confirmarPresencaAction);
}

void AgendamentoDialogo::carregarEspecialidadesDentista()
{
    ui->especialidadeLst->clear();

    m_lista_especialidades = m_dentista->getlistaEspecialidade(true);
    if(m_lista_especialidades.count() > 0) {
        _foreach(QSharedPointer<clinica::especialidade> especialidade, m_lista_especialidades) {
            ui->especialidadeLst->addItem(especialidade->getdescricao());
        }
        ui->especialidadeLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

QDate AgendamentoDialogo::retornaSegundaFeira(QDate dia)
{
    while(dia.dayOfWeek() != 1) {
        dia = dia.addDays(-1);
    }
    return dia;
}

void AgendamentoDialogo::exibeHorarioDentista()
{
    int diaSemana = -1;
    QTreeWidgetItem *dia;

    ui->horarioTrw->clear();
    ui->horarioTrw->setColumnCount(1);
    ui->horarioTrw->setHeaderLabel(QString("Dia/Horario"));

    _foreach(QSharedPointer<clinica::horarioTrabalho> horario, m_dentista->getlistaHorarioTrabalhoOrdenadoPorDiaHoraInicio()) {

        if(diaSemana != horario->getdiaDaSemana()) {
            dia = new QTreeWidgetItem(ui->horarioTrw);
            dia->setText(0, QDate::longDayName(horario->getdiaDaSemana()));
            diaSemana = horario->getdiaDaSemana();
        }

        QTreeWidgetItem *itemHorario = new QTreeWidgetItem(dia);
        itemHorario->setText(0, horario->gethorarioInicio().toString("HH:mm") + " - " + horario->gethorarioTermino().toString("HH:mm"));

    }
}


void AgendamentoDialogo::carregaAgendaDentista()
{
    clinica::dentista::type_listaAgendamento agendamentosDaSemana = m_dentista->getlistaAgendamentoDaSemana(retornaSegundaFeira(ui->calendarioWgt->selectedDate()));
    int numAgendamentos = agendamentosDaSemana.count();
    m_agenda_model = new QStandardItemModel(numAgendamentos, 1, ui->agendaWgt);
    QDateTime time = QDateTime(retornaSegundaFeira(ui->calendarioWgt->selectedDate()),QTime(8,0,0));
    ui->agendaWgt->setViewMode(QxtScheduleView::CustomView);
    ui->agendaWgt->setCurrentZoomDepth(10, Qxt::Minute);
    ui->agendaWgt->setDateRange(time.date(),time.date().addDays(6));

    int i = 0;
    _foreach(QSharedPointer<clinica::agendamento> agendamento, agendamentosDaSemana) {

        QStandardItem *itemAgenda = new QStandardItem();

        time = agendamento->getdataHora();

        itemAgenda->setData(time.toTime_t(), Qxt::ItemStartTimeRole);
        itemAgenda->setData(QVariant::fromValue(agendamento->getduracao()*60), Qxt::ItemDurationRole);
        itemAgenda->setData(agendamento->getCorFundo(),Qt::BackgroundRole);
        itemAgenda->setData((int)agendamento->getagendamentoId(), Qxt::UserRole);

        m_agenda_model->setItem(i++, itemAgenda);
    }

    ui->agendaWgt->setModel(m_agenda_model, false);

}

void AgendamentoDialogo::onDentistaSelecionado(int row)
{
    if(row >= 0 && row < m_lista_dentista.count()) {
        m_dentista = m_lista_dentista.getByIndex(row);

        carregarEspecialidadesDentista();
        carregaAgendaDentista();
        exibeHorarioDentista();
    }
}

void AgendamentoDialogo::contextMenuRequested(const QDateTime &dateTime)
{
    m_novo_horario = dateTime;
    ui->agendaWgt->addAction(m_adiconarAction);
    ui->agendaWgt->removeAction(m_removerAction);
    ui->agendaWgt->removeAction(m_confirmarPresencaAction);
}

void AgendamentoDialogo::contextMenuRequested(const QModelIndex &index)
{
    m_model_index = index;
    ui->agendaWgt->removeAction(m_adiconarAction);
    ui->agendaWgt->addAction(m_removerAction);
    ui->agendaWgt->addAction(m_confirmarPresencaAction);
}

void AgendamentoDialogo::removerAgendamento()
{
    clinica::agendamento_ptr agendamento = clinica::agendamento::recuperar(m_model_index.data(Qxt::UserRole).toInt());

    QString retorno = agendamento->apagar();
    if(retorno == "Ok") {
        m_agenda_model->removeRow(m_model_index.row());
        ui->agendaWgt->setModel(m_agenda_model, true);
    } else {
        QMessageBox::critical(this, "Remover", "Erro ao remover agendamento.\n" + retorno);
    }
}

void AgendamentoDialogo::confirmarPresenca()
{
    clinica::agendamento_ptr agendamento = clinica::agendamento::recuperar(m_model_index.data(Qxt::UserRole).toInt());
    agendamento->setcomparecimento(true);

    QString retorno = agendamento->salvar();
    if(retorno != "Ok") {
        QMessageBox::critical(this, "Confirmar", "Erro ao confirmar presença ao agendamento.\n" + retorno);
    } else {
        carregaAgendaDentista();
    }
}

void AgendamentoDialogo::alterarAgendamento(QModelIndex modelIndex)
{
    clinica::agendamento_ptr agendamento = clinica::agendamento::recuperar(modelIndex.data(Qxt::UserRole).toInt());
    agendamento->setdataHora(QDateTime::fromTime_t(modelIndex.data(Qxt::ItemStartTimeRole).toInt()));
    agendamento->setduracao(modelIndex.data(Qxt::ItemDurationRole).toInt()/60);

    QString retorno = agendamento->salvar();
    if(retorno != "Ok") {
        QMessageBox::critical(this, "Alterar", "Erro ao alterar agendamento.\n" + retorno);
    }
}

void AgendamentoDialogo::adicionarAgendamento()
{
    AgendarDialogo *agendarDlg = new AgendarDialogo(this);
    agendarDlg->m_dentista = m_dentista;
    agendarDlg->m_lista_especialidades = m_lista_especialidades;
    agendarDlg->m_especialidade = ui->especialidadeLst->currentItem()->text();
    agendarDlg->m_cliente.reset(new clinica::cliente());
    agendarDlg->m_dataHora = this->m_novo_horario;
    agendarDlg->init();
    agendarDlg->exec();

    carregaAgendaDentista();
}

void AgendamentoDialogo::showTooltip(QModelIndex modelIndex)
{
    if(modelIndex.isValid()) {
        clinica::agendamento_ptr agendamento = clinica::agendamento::recuperar(modelIndex.data(Qxt::UserRole).toInt());

        QString detalhes = "Agendamento: " + agendamento->getdataHora().toString("dd/MM/yyyy HH:mm") + "\n";
        detalhes = detalhes.append("Paciente: " + agendamento->getnome_paciente() + "\n");
        if(!agendamento->getclienteId().isNull()) {
            detalhes = detalhes.append("Ficha: " + QString::asprintf("%06i", agendamento->getclienteId()->getcliente_id()) + "\n");
        }
        detalhes = detalhes.append("Dentista: " + agendamento->getdentistaId()->getnome() + "\n");
        detalhes = detalhes.append("Especialidade: " + agendamento->getespecialidadeId()->getdescricao() + "\n");
        detalhes = detalhes.append("Observação: " + agendamento->getobservacao());

        QPoint pos = QPoint(this->pos().x()+this->rect().width()-200, this->pos().y()+80);

        QToolTip::showText(pos, detalhes);
    }

}
