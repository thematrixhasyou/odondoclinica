#include "./include/pagamentopopup.h"
#include "ui_pagamentopopup.h"
#include "./persist/include/clinica_pagamento.h"

#include <QMessageBox>
#include <QPushButton>

Pagamentopopup::Pagamentopopup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Pagamentopopup)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    configurarEventos();
}

Pagamentopopup::~Pagamentopopup()
{
    delete ui;
}

void Pagamentopopup::init()
{
    ui->tblParcela->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->tblParcela->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tblParcela->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tblParcela->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tblParcela->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->edtCliente->setText("");
    ui->edtCliente->setDisabled(false);
    ui->lblFicha->setText("");
    ui->lblIdentidade->setText("");
    ui->lblCpf->setText("");
    ui->lblFixo->setText("");
    ui->lblCelular->setText("");
    carregarListaDentista();
    ui->cmbDentista->setDisabled(false);
    ui->datPagamento->setDate(QDate::currentDate());
    ui->rdbDebito->setChecked(true);
    ui->dblValor->setValue(0.0);
    ui->spiQtdeParcela->setValue(1);
    ui->tblParcela->setRowCount(0);
    ui->edtUltimosDigitos->setText("");
    ui->edtAutorizacao->setText("");
    ui->cmbBandeira->setCurrentIndex(1);
    habilitarCartaoCredito(false);
    m_listaPagamento.clear();

    if(!m_tratamento.isNull()) {
        m_tratamento->getcliente()->load();
        m_tratamento->getdentista()->load();

        ui->edtCliente->setText(m_tratamento->getcliente()->getnome());
        ui->lblFicha->setText(QString::asprintf("%06i", m_tratamento->getcliente()->getcliente_id()));
        ui->lblIdentidade->setText(m_tratamento->getcliente()->getdoc_identidade());
        ui->lblCpf->setText(m_tratamento->getcliente()->getcpf());
        ui->lblFixo->setText(m_tratamento->getcliente()->gettel_fixo());
        ui->lblCelular->setText(m_tratamento->getcliente()->gettel_celular());
        ui->edtCliente->setDisabled(true);

        ui->cmbDentista->setCurrentText(m_tratamento->getdentista()->getnome());
        ui->cmbDentista->setDisabled(true);
    } else {
        ui->edtCliente->setFocus();
    }
}

void Pagamentopopup::configurarEventos()
{
    nome_completer = new QCompleter(this);
    nome_completer->setMaxVisibleItems(5);
    nome_completer->setCompletionMode(QCompleter::PopupCompletion);
    nome_completer->setFilterMode(Qt::MatchStartsWith);
    nome_completer->setCaseSensitivity(Qt::CaseInsensitive);
    nome_completer->setWrapAround(false);
    nome_completer->setModel(clinica::cliente::retornaTodosNomeID(nome_completer));
    nome_completer->setModelSorting(QCompleter::CaseSensitivelySortedModel);

    ui->edtCliente->setCompleter(nome_completer);

    connect(ui->edtCliente, SIGNAL(returnPressed()), this, SLOT(carregarDetalhesCliente()));
    connect(ui->edtCliente, SIGNAL(editingFinished()), this, SLOT(carregarDetalhesCliente()));
    connect(ui->rdbDebito, SIGNAL(clicked(bool)), this, SLOT(pagamentoEmDebito(bool)));
    connect(ui->rdbCredito, SIGNAL(clicked(bool)), this, SLOT(pagamentoEmCredito(bool)));
    connect(ui->rdbDinheiro, SIGNAL(clicked(bool)), this, SLOT(pagamentoEmDinheiro(bool)));
    connect(ui->rdbCheque, SIGNAL(clicked(bool)), this, SLOT(pagamentoEmCheque(bool)));
    connect(ui->spiQtdeParcela, SIGNAL(valueChanged(int)), this, SLOT(calcularParcelas()));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()), Qt::UniqueConnection);
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()), Qt::UniqueConnection);
}

void Pagamentopopup::habilitarCartaoCredito(bool habilitar)
{
    ui->cmbBandeira->setEnabled(habilitar);
    ui->edtUltimosDigitos->setEnabled(habilitar);
    ui->edtAutorizacao->setEnabled(habilitar);
    ui->spiQtdeParcela->setEnabled(habilitar);
    ui->spiQtdeParcela->setValue(1);
    ui->tblParcela->setRowCount(0);
    ui->tblParcela->setEnabled(habilitar);
    ui->edtUltimosDigitos->setText("");
    ui->edtAutorizacao->setText("");
}

void Pagamentopopup::pagamentoEmDebito(bool pressed)
{
    habilitarCartaoCredito(!pressed);
}

void Pagamentopopup::pagamentoEmCredito(bool pressed)
{
    habilitarCartaoCredito(pressed);
}

void Pagamentopopup::pagamentoEmDinheiro(bool pressed)
{
    habilitarCartaoCredito(!pressed);
}

void Pagamentopopup::pagamentoEmCheque(bool pressed)
{
    habilitarCartaoCredito(!pressed);
}

void Pagamentopopup::calcularParcelas()
{
    int qtdParcelas = ui->spiQtdeParcela->value();

    ui->tblParcela->setRowCount(qtdParcelas);

    for(int i = 1; i <= qtdParcelas; ++i) {
        QTableWidgetItem *data_realizacao = new QTableWidgetItem(ui->datPagamento->date().toString("dd/MM/yyyy"));
        ui->tblParcela->setItem(i-1, 0, data_realizacao);

        QTableWidgetItem *data_efetivacao = new QTableWidgetItem(ui->datPagamento->date().addDays(i*30).toString("dd/MM/yyyy"));
        ui->tblParcela->setItem(i-1, 1, data_efetivacao);

        QTableWidgetItem *valor = new QTableWidgetItem(QString::number(ui->dblValor->value()/qtdParcelas, 'f', 2));
        valor->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        ui->tblParcela->setItem(i-1, 2, valor);
    }
}

void Pagamentopopup::carregarDetalhesCliente()
{
    if(!ui->edtCliente->text().isEmpty()) {
        int cliente_id = obterClienteID();
        if(cliente_id == 0) {
            QMessageBox::warning(this, "Pagamento", "Cliente não foi localizado: " + ui->edtCliente->text());
            return;
        }
        m_cliente = clinica::cliente::retornaPeloID(cliente_id);
        if(m_cliente->getcliente_id() == 0) {
            QMessageBox::warning(this, "Pagamento", "Cliente não foi localizado: " + ui->edtCliente->text());
            return;
        }

        ui->lblFicha->setText(QString::asprintf("%06i", m_cliente->getcliente_id()));
        ui->lblIdentidade->setText(m_cliente->getdoc_identidade());
        ui->lblCpf->setText(m_cliente->getcpf());
        ui->lblFixo->setText(m_cliente->gettel_fixo());
        ui->lblCelular->setText(m_cliente->gettel_celular());

        carregarTratamento();
    }
}

bool Pagamentopopup::validarPagamento()
{
    bool retorno = false;
    if(m_cliente.isNull() || m_cliente->getcliente_id() == 0) {
        QMessageBox::warning(this, "Pagamento", "Informe o cliente responsável pelo pagamento.", QMessageBox::Ok);
        ui->edtCliente->setFocus();
    } else if(ui->cmbDentista->currentIndex() < 0) {
        QMessageBox::warning(this, "Pagamento", "Informe o dentista que realiza o tratamento.", QMessageBox::Ok);
        ui->cmbDentista->setFocus();
    } else if(ui->dblValor->value() <= 0.00) {
        QMessageBox::warning(this, "Pagamento", "Informe o valor do pagamento.", QMessageBox::Ok);
        ui->dblValor->setFocus();
    } else if(ui->rdbCredito->isChecked()) {
        if(ui->cmbBandeira->currentIndex() < 0) {
            QMessageBox::warning(this, "Pagamento", "Informe a bandeira do cartão de crédito.", QMessageBox::Ok);
            ui->cmbBandeira->setFocus();
        } else if (!validarCartaoCreditoUltimosDigitos()) {
            QMessageBox::warning(this, "Pagamento", "Informe corretamente os últimos dígitos do número do cartão de crédito.", QMessageBox::Ok);
            ui->edtUltimosDigitos->setFocus();
        } else if(!validarCartaoCreditoAutorizacao()) {
            QMessageBox::warning(this, "Pagamento", "Informe corretamento o número da autorização do cartão de crédito", QMessageBox::Ok);
            ui->edtAutorizacao->setFocus();
        } else {
            retorno = true;
        }
    } else {
        retorno = true;
    }
    return retorno;
}

bool Pagamentopopup::validarCartaoCreditoUltimosDigitos()
{
    return (ui->edtUltimosDigitos->text().length() >= 4);
}

bool Pagamentopopup::validarCartaoCreditoAutorizacao()
{
    return (ui->edtAutorizacao->text().length() >= 6);
}

void Pagamentopopup::criarListaPagamento()
{
    m_listaPagamento.clear();
    clinica::pagamento_ptr pagamento;
    clinica::caixa_ptr caixa;
    caixa = caixa->retornaMovimento(ui->datPagamento->date());

    if(ui->rdbDinheiro->isChecked()) {
        pagamento.reset(new clinica::pagamento());
        pagamento->setdata_realizacao(ui->datPagamento->date());
        pagamento->setdata_efetivacao(ui->datPagamento->date());
        pagamento->setCaixa(caixa);
        pagamento->settipo("DIN");
        pagamento->setvalor(ui->dblValor->value());
        m_listaPagamento.insert(1, pagamento);

    } else if(ui->rdbCheque->isChecked()) {
        pagamento.reset(new clinica::pagamento());
        pagamento->setdata_realizacao(ui->datPagamento->date());
        pagamento->setdata_efetivacao(ui->datPagamento->date());
        pagamento->setCaixa(caixa);
        pagamento->settipo("CHE");
        pagamento->setvalor(ui->dblValor->value());
        m_listaPagamento.insert(1, pagamento);

    } else if(ui->rdbDebito->isChecked()) {
        pagamento.reset(new clinica::pagamento());
        pagamento->setdata_realizacao(ui->datPagamento->date());
        pagamento->setdata_efetivacao(ui->datPagamento->date().addDays(3));
        pagamento->setCaixa(caixa);
        pagamento->settipo("DEB");
        pagamento->setvalor(ui->dblValor->value());
        m_listaPagamento.insert(1, pagamento);

    } else { //Credito
        int qtdParcelas = ui->spiQtdeParcela->value();

        for(int i = 1; i <= qtdParcelas; ++i) {
            pagamento.reset(new clinica::pagamento());
            pagamento->setdata_realizacao(ui->datPagamento->date());
            pagamento->setdata_efetivacao(ui->datPagamento->date().addDays(i*30));
            pagamento->setCaixa(caixa);
            pagamento->settipo("CRE");
            pagamento->setcc_bandeira(ui->cmbBandeira->currentText());
            pagamento->setcc_final_numero(ui->edtUltimosDigitos->text());
            pagamento->setcc_autorizacao(ui->edtAutorizacao->text());
            pagamento->setvalor(ui->dblValor->value()/qtdParcelas);

            m_listaPagamento.insert(i, pagamento);
        }
    }
}

void Pagamentopopup::carregarListaDentista()
{
    ui->cmbDentista->clear();

    m_listaDentista = clinica::dentista::retornaTodosDentistasOrdenadosPorNome();
    if(m_listaDentista.count() > 0) {
        _foreach(QSharedPointer<clinica::dentista> dentista, m_listaDentista) {
            ui->cmbDentista->addItem(dentista->getnome(), QVariant::fromValue(dentista->getid()));
        }
        ui->cmbDentista->setCurrentIndex(-1);
    }
}

int Pagamentopopup::obterClienteID()
{
    int i = 0;
    while(ui->edtCliente->text() != nome_completer->currentIndex().data().toString() && i < nome_completer->completionCount()) {
        nome_completer->setCurrentRow(++i);
    }
    if(ui->edtCliente->text() == nome_completer->currentIndex().data().toString()) {
        return nome_completer->completionModel()->index(nome_completer->currentIndex().row(), 1).data().toInt();
    }
    return 0;
}

void Pagamentopopup::salvarPagamento()
{
    QString retorno;

    if(m_tratamento->getdentista().isNull()) {
        clinica::dentista_ptr dentista;
        dentista.reset(new clinica::dentista(ui->cmbDentista->currentData().toInt()));
        dentista->load();
        m_tratamento->setdentista(dentista);

        retorno = m_tratamento->salvarSemListaItemTratamento();
        if(retorno.compare("Ok") != 0) {
            QMessageBox::critical(this, "Salvar", "Erro ao gerar novo Tratamento.\n" + retorno);
            return;
        }
    }

    _foreach(QSharedPointer<clinica::pagamento> pagamento, m_listaPagamento) {

        pagamento->settratamento(m_tratamento);
        retorno = pagamento->salvar();
        if(retorno.compare("Ok") != 0) {
            QMessageBox::critical(this, "Salvar", "Erro ao salvar Pagamento.\n" + retorno);
            return;
        }
    }
}

void Pagamentopopup::carregarTratamento()
{
    clinica::cliente::type_listaTratamento listaTratamento = m_cliente->getlistaTratamento(true);

    if(listaTratamento.count() > 0) {
        _foreach(QSharedPointer<clinica::tratamento> tratamento, listaTratamento) {
            if(tratamento->getdata_termino().isNull()) {
                m_tratamento = tratamento;
            }
        }
    } else {
        m_tratamento.clear();
    }
    if(m_tratamento.isNull() || m_tratamento->gettratamento_id() <= 0) {
        m_tratamento.reset(new clinica::tratamento());
        m_tratamento->setcliente(m_cliente);
        m_tratamento->setdata_inicio(ui->datPagamento->date());
        ui->cmbDentista->setCurrentIndex(-1);
        ui->cmbDentista->setDisabled(false);
    } else {
        m_tratamento->getdentista()->load();
        ui->cmbDentista->setCurrentText(m_tratamento->getdentista()->getnome());
        ui->cmbDentista->setDisabled(true);
    }
}

void Pagamentopopup::accept()
{
    if(validarPagamento()) {
        criarListaPagamento();
        salvarPagamento();
        QDialog::accept();
    }
}

int Pagamentopopup::exec()
{
    init();
    QPushButton *saveBtn = ui->buttonBox->button(QDialogButtonBox::Ok);
    saveBtn->setDisabled(false);
    return QDialog::exec();
}

int Pagamentopopup::exec(clinica::tratamento_ptr tratamento)
{
    m_tratamento = tratamento;
    m_cliente = tratamento->getcliente();
    return this->exec();
}
