#include <QtWidgets>
#include "include/planodialogo.h"
#include "ui_planodialogo.h"
#include "persist/include/clinica_plano_odontologico.h"

PlanoDialogo::PlanoDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlanoDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    ui->planoLst->installEventFilter(this);
    setModal(true);
    configurarEventos();
    carregaListaPlano();
}

PlanoDialogo::~PlanoDialogo()
{
    delete ui;
}

void PlanoDialogo::configurarEventos()
{
    connect(ui->novoBtn, SIGNAL(clicked()), this, SLOT(novo()));
    connect(ui->salvarBtn, SIGNAL(clicked()), this, SLOT(salvar()));
    connect(ui->inativarBtn, SIGNAL(clicked()), this, SLOT(inativar()));
    connect(ui->fecharBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->planoLst, SIGNAL(currentRowChanged(int)), this, SLOT(onPlanoSelecionado(int)));
}

void PlanoDialogo::novo() {
    m_plano.reset(new clinica::plano_odontologico());
    limpar();
}

void PlanoDialogo::limpar() {
    ui->nomeEdt->clear();
    ui->observacaoEdt->clear();
    ui->nomeEdt->setFocus();
}

bool PlanoDialogo::eventFilter(QObject *obj, QEvent *ev) {
    if(obj == ui->planoLst && ev->type() == QEvent::FocusIn) {
        onListaRecebeFoco();
    }
    return false;
}

void PlanoDialogo::onListaRecebeFoco() {
    if(ui->planoLst->count() > 0) {
        ui->planoLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

void PlanoDialogo::carregaListaPlano() {
    ui->planoLst->clear();

    m_lista_planos = clinica::plano_odontologico::retornaTodosPlanosOrdenadosPorNome();
    if(m_lista_planos.count() > 0) {
        _foreach(QSharedPointer<clinica::plano_odontologico> plano, m_lista_planos) {
            ui->planoLst->addItem(plano->getnome());
        }
        ui->planoLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

void PlanoDialogo::onPlanoSelecionado(int row) {
    if(row >= 0 && row < m_lista_planos.count()) {
        m_plano = m_lista_planos.getByIndex(row);

        ui->nomeEdt->setText(m_plano->getnome());
        ui->observacaoEdt->setPlainText(m_plano->getobservacao());

    }
}

void PlanoDialogo::salvar() {
    m_plano->setnome(ui->nomeEdt->text());
    m_plano->setobservacao(ui->observacaoEdt->toPlainText());

    QString retorno = m_plano->salvar();
    if(retorno.compare("Ok") == 0) {
        QMessageBox::information(this, "Salvar", "Plano Odontológico salvo com sucesso.");
        limpar();
        carregaListaPlano();
    } else {
        QMessageBox::critical(this, "Salvar", "Erro ao salvar Plano Odontológico.\n" + retorno);
    }
}

void PlanoDialogo::inativar() {
    if(QMessageBox::question(this, "Inativar", "O dados do plano odontológico não ficarão mais disponíveis.\nConfirma inativar o plano odontológico?") == QMessageBox::Yes) {
        QString retorno = m_plano->inativar();
        if(retorno.compare("Ok") == 0) {
            QMessageBox::information(this, "Inativar", "Plano Odontológico inativado com sucesso.");
            limpar();
            carregaListaPlano();
        } else {
            QMessageBox::critical(this, "Inativar", "Erro ao inativar Plano Odontológico.\n" + retorno);
        }
    }
}

