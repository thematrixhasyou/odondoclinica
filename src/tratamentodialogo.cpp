#include "./include/tratamentodialogo.h"
#include "ui_tratamentodialogo.h"
#include <qtrpt.h>

#include <QMessageBox>
#include <QDebug>

TratamentoDialogo::TratamentoDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TratamentoDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    init();
}

TratamentoDialogo::~TratamentoDialogo()
{
    delete ui;
}

void TratamentoDialogo::init()
{
    m_valor_tratamento = 0.0f;
    m_valor_pagamento = 0.0f;
    m_valor_pagar = 0.0f;
    configurarEventos();

    m_tratamentoPopup = new TratamentoPopup(this);
    m_itemTratamentoPopup = new ItemTratamentoPopup(this);
    m_pagamentoPopup = new Pagamentopopup(this);

    ui->itemTratamentoTbl->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->itemTratamentoTbl->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->itemTratamentoTbl->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->pagamentoTbl->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    ui->pagamentoTbl->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->pagamentoTbl->setEditTriggers(QAbstractItemView::NoEditTriggers);

    completer = new QCompleter(this);
    completer->setMaxVisibleItems(5);
    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setFilterMode(Qt::MatchStartsWith);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setWrapAround(false);
    completer->setModel(clinica::cliente::retornaTodosNomeID(completer));
    completer->setModelSorting(QCompleter::CaseSensitivelySortedModel);

    ui->clienteEdt->setCompleter(completer);

    ui->novoBtn->setDisabled(true);
    ui->encerrarBtn->setDisabled(true);
    ui->reabrirBtn->setDisabled(true);
    ui->imprimirBtn->setDisabled(true);

    ui->novoItemBtn->setDisabled(true);
    ui->removerItemBtn->setDisabled(true);
    ui->novoPagamentoBtn->setDisabled(true);
    ui->removerPagamentoBtn->setDisabled(true);

}

void TratamentoDialogo::configurarEventos()
{
    connect(ui->clienteEdt, SIGNAL(returnPressed()), this, SLOT(carregarDetalhesCliente()));
    connect(ui->clienteEdt, SIGNAL(editingFinished()), this, SLOT(carregarDetalhesCliente()));
    connect(ui->carregarBtn, SIGNAL(clicked()), this, SLOT(carregarDetalhesCliente()));
    connect(ui->tratamentoTrw,SIGNAL(itemSelectionChanged()), this, SLOT(carregarItensTratamento()));

    connect(ui->novoItemBtn, SIGNAL(clicked()), this, SLOT(novoItemTratamento()));
    connect(ui->removerItemBtn, SIGNAL(clicked()), this, SLOT(removerItemTratamento()));
    connect(ui->itemTratamentoTbl, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editarItemTratamento(QModelIndex)));

    connect(ui->novoPagamentoBtn, SIGNAL(clicked()), this, SLOT(novoPagamento()));
    connect(ui->removerPagamentoBtn, SIGNAL(clicked()), this, SLOT(removerPagamento()));

    connect(ui->novoBtn, SIGNAL(clicked()), this, SLOT(novoTratamento()));
    connect(ui->encerrarBtn, SIGNAL(clicked()), this, SLOT(encerrarTratamento()));
    connect(ui->reabrirBtn, SIGNAL(clicked()), this, SLOT(reabrirTratamento()));
    connect(ui->imprimirBtn, SIGNAL(clicked()), this, SLOT(imprimirTratamento()));

    connect(ui->limparBtn, SIGNAL(clicked()), this, SLOT(limpar()));
    connect(ui->fecharBtn, SIGNAL(clicked()), this, SLOT(close()));
}

int TratamentoDialogo::obterClienteID()
{
    int i = 0;
    while(ui->clienteEdt->text() != completer->currentIndex().data().toString() && i < completer->completionCount()) {
        completer->setCurrentRow(++i);
    }
    if(ui->clienteEdt->text() == completer->currentIndex().data().toString()) {
        return completer->completionModel()->index(completer->currentIndex().row(), 1).data().toInt();
    }
    return 0;
}

void TratamentoDialogo::carregarTratamentos()
{
    int ano = -1;
    long dentista_id = -1;
    QString datas;
    QString nomeDentista;
    QTreeWidgetItem *itemAno;
    QTreeWidgetItem *itemDentista;
    QTreeWidgetItem *itemDatas;
    QTreeWidgetItem *itemSelecionar = nullptr;

    ui->tratamentoTrw->clear();
    ui->tratamentoTrw->setColumnCount(1);
    ui->tratamentoTrw->setHeaderLabel(QString("Ano/Dentista/Data Início - Data Termino"));
    m_lista_tratamento = m_cliente->getlistaTratamentoOrndenadoPorDataInicioDescDentistaAsc();

    _foreach(QSharedPointer<clinica::tratamento> tratamento, m_lista_tratamento) {
        if(ano != tratamento->getdata_inicio().year()) {
            ano = tratamento->getdata_inicio().year();
            itemAno = new QTreeWidgetItem(ui->tratamentoTrw);
            itemAno->setText(0, QString::number(ano));
            dentista_id = -1;
        }

        if(dentista_id != tratamento->getDentistaMesmoInativo()->getid()) {
            dentista_id = tratamento->getdentista()->getid();

            nomeDentista = tratamento->getdentista()->getnome();
            if(!tratamento->getdentista()->getinativo().isNull()) {
                nomeDentista = nomeDentista.append(" - Inativo");
            }

            itemDentista = new QTreeWidgetItem(itemAno);
            itemDentista->setText(0, nomeDentista);
        }

        datas = tratamento->getdata_inicio().toString("dd/MM/yyyy");
        if(!tratamento->getdata_termino().isNull()) {
            datas = datas.append(" - ");
            datas = datas.append(tratamento->getdata_termino().toString("dd/MM/yyyy"));
        }
        itemDatas = new QTreeWidgetItem(itemDentista);
        itemDatas->setText(0, datas);
        itemDatas->setData(0, Qt::UserRole, QVariant::fromValue(tratamento->gettratamento_id()));
        if(tratamento->getdata_termino().isNull()) {
            itemSelecionar = itemDatas;
        }
    }
    if(itemSelecionar) {
        ui->tratamentoTrw->setCurrentItem(itemSelecionar, 2, QItemSelectionModel::ClearAndSelect);
        ui->tratamentoTrw->setFocus();
    }
    atualizaSituacaoBotoes();
}

void TratamentoDialogo::carregarItensTratamento()
{
    m_valor_tratamento = 0.0f;
    QVariant v_tratamento_id = ui->tratamentoTrw->currentItem()->data(0, Qt::UserRole);
    if(!v_tratamento_id.isNull()) {
        m_tratamento = clinica::tratamento::retornaPeloID(v_tratamento_id.toInt());
        m_lista_item_tratamento = m_tratamento->getlistaItemTratamentoOrdenadaPelaOrdem();

        ui->itemTratamentoTbl->setRowCount(m_lista_item_tratamento.count());

        int i = 0;
        _foreach(QSharedPointer<clinica::item_tratamento> item_tratamento, m_lista_item_tratamento) {

            QTableWidgetItem *ordem = new QTableWidgetItem(QString::number(item_tratamento->getordem()));
            ui->itemTratamentoTbl->setItem(i, 0, ordem);

            QTableWidgetItem *procedimento = new QTableWidgetItem(item_tratamento->getprocedimento()->getdescricao());
            procedimento->setData(Qt::UserRole, QVariant::fromValue(item_tratamento->getprocedimento()->getprocedimento_id()));
            ui->itemTratamentoTbl->setItem(i, 1, procedimento);

            QTableWidgetItem *quantidade = new QTableWidgetItem(QString::number(item_tratamento->getquantidade()));
            ui->itemTratamentoTbl->setItem(i, 2, quantidade);

            QTableWidgetItem *valor_unitario = new QTableWidgetItem(QString::number(item_tratamento->getvalor_unitario(), 'f', 2));
            valor_unitario->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ui->itemTratamentoTbl->setItem(i, 3, valor_unitario);

            double valor = item_tratamento->getvalor_unitario() * item_tratamento->getquantidade();
            QTableWidgetItem *valor_item = new QTableWidgetItem(QString::number(valor, 'f', 2));
            valor_item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ui->itemTratamentoTbl->setItem(i, 4, valor_item);

            QString realizacao;
            if(item_tratamento->getrealizacao() == "CON") {
                realizacao = "Concluído";
            } else if(item_tratamento->getrealizacao() == "EAN") {
                realizacao = "Em Andamento";
            } else {
                realizacao = "Não Iniciado";
            }

            QTableWidgetItem *situacao = new QTableWidgetItem(realizacao);
            situacao->setData(Qt::UserRole, QVariant::fromValue(item_tratamento->getrealizacao()));
            ui->itemTratamentoTbl->setItem(i, 5, situacao);

            i++;
            m_valor_tratamento += valor;
        }
    } else {
        m_tratamento.reset();
        m_lista_item_tratamento.clear();
        m_valor_tratamento = 0;
        ui->itemTratamentoTbl->setRowCount(0);
    }
    carregarPagamentos();
    atualizaSituacaoBotoes();
    atualizarValoresTotais();
}

void TratamentoDialogo::carregarPagamentos()
{
    m_valor_pagamento = 0.0f;
    m_valor_pagar = 0.0f;
    m_lista_pagamento.clear();
    ui->pagamentoTbl->setRowCount(0);
    QString tipo_str, cc_str = "n/a";
    int i = 0;

    if(!m_tratamento.isNull()) {
        m_lista_pagamento = m_tratamento->getlistaPagamentoOrdenadaPelaData();
        ui->pagamentoTbl->setRowCount(m_lista_pagamento.count());

        _foreach(QSharedPointer<clinica::pagamento> pagamento, m_lista_pagamento) {

            QTableWidgetItem *data_realizacao = new QTableWidgetItem(pagamento->getdata_realizacao().toString("dd/MM/yyyy"));
            ui->pagamentoTbl->setItem(i, 0, data_realizacao);

            QTableWidgetItem *data_efetivacao = new QTableWidgetItem(pagamento->getdata_efetivacao().toString("dd/MM/yyyy"));
            ui->pagamentoTbl->setItem(i, 1, data_efetivacao);

            cc_str = "n/a";
            if(pagamento->gettipo() == "DIN") {
                tipo_str = "Dinheiro";
            } else if(pagamento->gettipo() == "CHE") {
                tipo_str = "Cheque";
            } else if(pagamento->gettipo() == "DEB") {
                tipo_str = "C.Débito";
            } else {
                tipo_str = "C.Crédito";
                cc_str = pagamento->getcc_bandeira().append(" - Últ Dig: ").append(pagamento->getcc_final_numero()).append(" - Autoiz: ").append(pagamento->getcc_autorizacao());
            }

            QTableWidgetItem *tipo = new QTableWidgetItem(tipo_str);
            ui->pagamentoTbl->setItem(i, 2, tipo);

            QTableWidgetItem *valor = new QTableWidgetItem(QString::number(pagamento->getvalor(), 'f', 2));
            valor->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ui->pagamentoTbl->setItem(i, 3, valor);

            QTableWidgetItem *cc = new QTableWidgetItem(cc_str);
            ui->pagamentoTbl->setItem(i, 4, cc);

            i++;
            m_valor_pagamento += pagamento->getvalor();
        }
    }
    atualizarValoresTotais();
}

void TratamentoDialogo::atualizarValoresTotais()
{
    m_valor_pagar = m_valor_tratamento - m_valor_pagamento;
    ui->valorTratamentoEdt->setText(QString::number(m_valor_tratamento, 'f', 2));
    ui->valorPagamentoEdt->setText(QString::number(m_valor_pagamento, 'f', 2));
    ui->valorPagarEdt->setText(QString::number(m_valor_pagar, 'f', 2));
}

bool TratamentoDialogo::validarSalvar()
{
    if(m_tratamento->getdentista().isNull()) {
        QMessageBox::information(this, "Salvar", "Selecione um Dentista.");
        return false;
    }
    if(m_lista_item_tratamento.count() == 0) {
        QMessageBox::information(this, "Salvar", "Informe pelo menos um item do tratamento.");
        return false;
    }
    if(!m_tratamento->getdata_inicio().isNull() && m_tratamento->getdata_inicio() > QDate::currentDate()) {
        QMessageBox::information(this, "Salvar", "A data de início do tratamento deve ser menor ou igual a data de hoje.");
        return false;
    }
    if(!m_tratamento->getdata_termino().isNull() && m_tratamento->getdata_termino() > QDate::currentDate()) {
        QMessageBox::information(this, "Salvar", "A data de término do tratamento deve ser menor ou igual a data de hoje.");
        return false;
    }
    if(!m_tratamento->getdata_inicio().isNull() && !m_tratamento->getdata_termino().isNull() &&
            m_tratamento->getdata_termino() < m_tratamento->getdata_inicio()) {
        QMessageBox::information(this, "Salvar", "A data de término do tratamento deve ser maior ou igual a data de início.");
        return false;
    }
    return true;
}

bool TratamentoDialogo::validarEncerrarTratamento()
{
    if(m_valor_pagar > 0) {
        if(QMessageBox::question(this, "Encerrar Tratamento", "Confirma encerrar o tratamento mesmo houvendo valor a pagar?") == QMessageBox::Yes) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

void TratamentoDialogo::carregarDetalhesCliente()
{
    if(!ui->clienteEdt->text().isEmpty()) {
        int cliente_id = obterClienteID();
        if(cliente_id == 0) {
            QMessageBox::warning(this, "Cadastrar Tratamento", "Cliente não foi localizado: " + ui->clienteEdt->text());
            return;
        }
        m_cliente = clinica::cliente::retornaPeloID(cliente_id);
        if(m_cliente->getcliente_id() == 0) {
            QMessageBox::warning(this, "Cadastrar Tratamento", "Cliente não foi localizado: " + ui->clienteEdt->text());
            return;
        }

        limparExcetoCliente();
        ui->cpfLbl->setText(m_cliente->getcpf());
        ui->identidadeLbl->setText(m_cliente->getdoc_identidade());
        ui->fixoLbl->setText(m_cliente->gettel_fixo());
        ui->celularLbl->setText(m_cliente->gettel_celular());
        ui->fichaLbl->setText(QString::asprintf("%06i", m_cliente->getcliente_id()));

        carregarTratamentos();
    }
}

void TratamentoDialogo::novoItemTratamento()
{
    if(m_itemTratamentoPopup->exec() == QDialog::Accepted) {
        clinica::item_tratamento_ptr itemTratamento;
        itemTratamento.reset(new clinica::item_tratamento());
        itemTratamento->setprocedimento(m_itemTratamentoPopup->getProcedimentoSelecionado());
        itemTratamento->setquantidade(m_itemTratamentoPopup->getQuantidadeSelecionada());
        itemTratamento->setvalor_unitario(m_itemTratamentoPopup->getValorUnitarioSelecionado());
        itemTratamento->setrealizacao(m_itemTratamentoPopup->getSituacaoSelecionada());
        itemTratamento->setordem(m_tratamento->retornaProximaOrdemLivre());
        itemTratamento->settratamento(m_tratamento);

        salvarItemTratamento(itemTratamento);
    }
}

void TratamentoDialogo::editarItemTratamento(QModelIndex model)
{
    if(tratamentoEmAndamento()) {
        clinica::item_tratamento_ptr itemTratamento;
        itemTratamento = m_lista_item_tratamento.getByIndex(model.row());

        if(m_itemTratamentoPopup->exec(itemTratamento->getprocedimento(), itemTratamento->getquantidade(),
                                       itemTratamento->getvalor_unitario(), itemTratamento->getrealizacao()) == QDialog::Accepted) {

            itemTratamento->setprocedimento(m_itemTratamentoPopup->getProcedimentoSelecionado());
            itemTratamento->setquantidade(m_itemTratamentoPopup->getQuantidadeSelecionada());
            itemTratamento->setvalor_unitario(m_itemTratamentoPopup->getValorUnitarioSelecionado());
            itemTratamento->setrealizacao(m_itemTratamentoPopup->getSituacaoSelecionada());

            salvarItemTratamento(itemTratamento);

            if(itemTratamento->getrealizacao() == "CON" && todosItensConcluidos()) {
                if(QMessageBox::question(this, "Item de Tratamento", "Deseja encerrar o Tratamento?",
                                             QMessageBox::Yes | QMessageBox::No ) == QMessageBox::Yes) {
                    if(validarEncerrarTratamento()) {
                        m_tratamento->setdata_termino(QDate::currentDate());
                        salvarTratatamento();
                    }
                }
            }

        }
    }
}

void TratamentoDialogo::removerItemTratamento()
{
    if(ui->itemTratamentoTbl->selectedItems().count() <= 0) {
        QMessageBox::information(this, "Item de Tratamento", "Selecione um item do tratamento.");
    } else if(QMessageBox::question(this, "Item de Tratamento", "Confirma remover o item do tratamento selecionado?",
                             QMessageBox::Yes | QMessageBox::No ) == QMessageBox::Yes) {
        excluirItemTratamento(m_lista_item_tratamento.getByIndex(ui->itemTratamentoTbl->selectedItems().first()->row()));
    }
}

void TratamentoDialogo::novoPagamento()
{
    clinica::caixa_ptr caixa;
    caixa.reset(new clinica::caixa());

    if(caixa->isMovimentoAberto(QDate::currentDate())) {
        if(m_pagamentoPopup->exec(m_tratamento) == QDialog::Accepted) {
            carregarPagamentos();
        }
    } else {
        QMessageBox::information(this, "Pagamento", "O Caixa não está aberto para o movimento de: " + QDate::currentDate().toString("dd/MM/yyyy"));
    }
}

void TratamentoDialogo::removerPagamento()
{
    clinica::caixa_ptr caixa;
    caixa.reset(new clinica::caixa());

    if(caixa->isMovimentoAberto(QDate::currentDate())) {
        if(ui->pagamentoTbl->selectedItems().count() <= 0) {
            QMessageBox::information(this, "Pagamento", "Selecione um pagamento.");
        } else {
            excluirPagamento(m_lista_pagamento.getByIndex(ui->pagamentoTbl->selectedItems().first()->row()));
        }
    } else {
        QMessageBox::information(this, "Pagamento", "O Caixa não está aberto para o movimento de: " + QDate::currentDate().toString("dd/MM/yyyy"));
    }
}

void TratamentoDialogo::limpar()
{
    ui->clienteEdt->setText("");
    m_cliente.clear();
    limparExcetoCliente();
    ui->clienteEdt->setFocus();
}

void TratamentoDialogo::limparDetalhes()
{
    ui->itemTratamentoTbl->setRowCount(0);
    ui->pagamentoTbl->setRowCount(0);
    ui->valorTratamentoEdt->setText("");
    ui->valorPagamentoEdt->setText("");
    ui->valorPagarEdt->setText("");
    m_lista_item_tratamento.clear();
    m_lista_pagamento.clear();
}

bool TratamentoDialogo::isPagamentoParcelado(clinica::pagamento_ptr pagamento)
{
    bool retorno = false;
    if(pagamento->gettipo() == "CRE") {
        _foreach(QSharedPointer<clinica::pagamento> it_pagamento, m_lista_pagamento) {
            if( it_pagamento->gettipo() == "CRE" &&
                it_pagamento->getdata_realizacao() == pagamento->getdata_realizacao() &&
                it_pagamento->getdata_efetivacao() != pagamento->getdata_efetivacao() &&
                it_pagamento->getcc_autorizacao() == pagamento->getcc_autorizacao()) {
                return true;
            }
        }
    }
    return retorno;
}

void TratamentoDialogo::excluirTodasParcelas(clinica::pagamento_ptr pagamento)
{
    QString retorno;
    _foreach(QSharedPointer<clinica::pagamento> it_pagamento, m_lista_pagamento) {
        if(it_pagamento->gettipo() == "CRE" &&
           it_pagamento->getdata_realizacao() == pagamento->getdata_realizacao() &&
           it_pagamento->getcc_autorizacao() == pagamento->getcc_autorizacao()) {
            retorno = it_pagamento->excluir();
            if(retorno.compare("Ok") != 0) {
                QMessageBox::critical(this, "Salvar", "Erro ao remover parcela do Pagamento.\n" + retorno);
                return;
            }
        }
    }
}

void TratamentoDialogo::limparExcetoCliente()
{
    ui->cpfLbl->setText("");
    ui->identidadeLbl->setText("");
    ui->fixoLbl->setText("");
    ui->celularLbl->setText("");
    ui->fichaLbl->setText("");
    ui->tratamentoTrw->clear();
    m_tratamento.clear();
    m_lista_tratamento.clear();
    limparDetalhes();
    atualizaSituacaoBotoes();
}

void TratamentoDialogo::atualizaSituacaoBotoes()
{
    if(!m_tratamento.isNull()) {
        ui->encerrarBtn->setDisabled(!tratamentoEmAndamento());
        ui->reabrirBtn->setDisabled(tratamentoEmAndamento() || existeTratamentoEmAndamento() || existeTratamentoMaisRecente());
        ui->imprimirBtn->setDisabled(false);
        ui->novoItemBtn->setDisabled(!tratamentoEmAndamento());
        ui->removerItemBtn->setDisabled(!tratamentoEmAndamento());
        ui->novoPagamentoBtn->setDisabled(!tratamentoEmAndamento());
        ui->removerPagamentoBtn->setDisabled(!tratamentoEmAndamento());
    } else {
        ui->encerrarBtn->setDisabled(true);
        ui->reabrirBtn->setDisabled(true);
        ui->imprimirBtn->setDisabled(true);
        ui->novoItemBtn->setDisabled(true);
        ui->removerItemBtn->setDisabled(true);
        ui->novoPagamentoBtn->setDisabled(true);
        ui->removerPagamentoBtn->setDisabled(true);
    }
    ui->novoBtn->setDisabled(m_cliente.isNull() || existeTratamentoEmAndamento());
}

bool TratamentoDialogo::tratamentoEmAndamento()
{
    return !m_tratamento.isNull() && m_tratamento->getdata_termino().isNull();
}

bool TratamentoDialogo::todosItensConcluidos()
{
    for(int i = 0; i < m_lista_item_tratamento.count(); ++i) {
        if(m_lista_item_tratamento.getByIndex(i)->getrealizacao() != "CON") {
            return false;
        }
    }
    return true;
}

bool TratamentoDialogo::existeTratamentoEmAndamento()
{
    _foreach(QSharedPointer<clinica::tratamento> tratamento, m_lista_tratamento) {
        if(tratamento->getdata_termino().isNull()) {
            return true;
        }
    }
    return false;
}

bool TratamentoDialogo::existeTratamentoMaisRecente()
{
    _foreach(QSharedPointer<clinica::tratamento> tratamento, m_lista_tratamento) {
        if(tratamento->getdata_inicio() > m_tratamento->getdata_inicio()) {
            return true;
        }
    }
    return false;
}

void TratamentoDialogo::salvarTratatamento()
{
    QString retorno = m_tratamento->salvarSemListaItemTratamento();
    if(retorno.compare("Ok") != 0) {
        QMessageBox::critical(this, "Salvar", "Erro ao salvar Tratamento.\n" + retorno);
        return;
    }
    carregarTratamentos();
    limparDetalhes();
}

void TratamentoDialogo::salvarItemTratamento(clinica::item_tratamento_ptr itemTratamento)
{
    QString retorno = itemTratamento->salvar();
    if(retorno.compare("Ok") != 0) {
        QMessageBox::critical(this, "Salvar", "Erro ao salvar Item do Tratamento.\n" + retorno);
        return;
    }
    carregarItensTratamento();
}

void TratamentoDialogo::excluirItemTratamento(clinica::item_tratamento_ptr itemTratamento)
{
    QString retorno = itemTratamento->excluir();
    if(retorno.compare("Ok") != 0) {
        QMessageBox::critical(this, "Salvar", "Erro ao remover Item do Tratamento.\n" + retorno);
        return;
    }
    carregarItensTratamento();
}

void TratamentoDialogo::excluirPagamento(clinica::pagamento_ptr pagamento)
{
    if(pagamento->getdata_realizacao() != QDate::currentDate()) {
        QMessageBox::information(this, "Salvar", "Não é possível remover pagamento realizado em outro movimento de caixa.");
        return;
    }
    if(pagamento->gettipo() != "CRE" || !isPagamentoParcelado(pagamento)) {
        if(QMessageBox::question(this, "Pagamento", "Confirma remover o pagamento selecionado?",
                                         QMessageBox::Yes | QMessageBox::No ) == QMessageBox::Yes) {
            QString retorno = pagamento->excluir();
            if(retorno.compare("Ok") != 0) {
                QMessageBox::critical(this, "Salvar", "Erro ao remover Pagamento.\n" + retorno);
            }
        }
    } else {
        if(QMessageBox::question(this, "Pagamento", "O pagamento selecionado é parcelado.\nConfirma remover todas as apcelas do pagamento selecionado?",
                                            QMessageBox::Yes | QMessageBox::No ) == QMessageBox::Yes) {
            excluirTodasParcelas(pagamento);
        }

    }
    carregarPagamentos();
}

void TratamentoDialogo::novoTratamento()
{
    m_tratamento.reset(new clinica::tratamento());
    m_lista_item_tratamento.clear();
    ui->itemTratamentoTbl->setRowCount(0);
    if(m_tratamentoPopup->exec(tipoAcao::iniciar) == QDialog::Accepted) {
        m_tratamento->setcliente(m_cliente);
        m_tratamento->setdentista(m_tratamentoPopup->getDentistaSelecionado());
        m_tratamento->setdata_inicio(m_tratamentoPopup->getDataSelecionada());
        salvarTratatamento();
    } else {
        atualizaSituacaoBotoes();
    }
}

void TratamentoDialogo::encerrarTratamento()
{
    if(validarEncerrarTratamento() && m_tratamentoPopup->exec(tipoAcao::encerrar) == QDialog::Accepted) {
        m_tratamento->setdata_termino(m_tratamentoPopup->getDataSelecionada());
        salvarTratatamento();
    } else {
        atualizaSituacaoBotoes();
    }
}

void TratamentoDialogo::reabrirTratamento()
{
    m_tratamento->setdata_termino(QDate());
    salvarTratatamento();
}

void TratamentoDialogo::imprimirTratamento()
{
    QtRPT *report = new QtRPT(this);
    report->recordCount << m_lista_item_tratamento.count() + m_lista_pagamento.count();
    report->loadReport("detalhe_tratamento.xml");
    QObject::connect(report, SIGNAL(setValue(const int, const QString, QVariant&, const int)), this, SLOT(setValoresImpressao(const int, const QString, QVariant&, const int)));
    report->printExec();
}

void TratamentoDialogo::setValoresImpressao(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage)
{
    if (reportPage == 0) {
        if (paramName == "nome_cliente") {
            paramValue = m_cliente->getnome();
        }
        else if (paramName == "numero_ficha") {
            paramValue = QString::asprintf("%06i", m_cliente->getcliente_id());
        }
        else if (paramName == "nome_dentista") {
            paramValue = m_tratamento->getDentistaMesmoInativo()->getnome();
        }
        else if (paramName == "situacao_tratamento") {
            paramValue = (tratamentoEmAndamento()?"em Andamento":"Encerrado");
        }
        else if (paramName == "data_inicio") {
            paramValue = m_tratamento->getdata_inicio().toString("dd/MM/yyyy");
        }
        else if (paramName == "data_termino") {
            paramValue = (!m_tratamento->getdata_termino().isNull()?m_tratamento->getdata_termino().toString("dd/MM/yyyy"):"");
        }
        else if (paramName == "total_tratamento") {
            paramValue = m_valor_tratamento;
        }
        else if (paramName == "total_pagamento") {
            paramValue = m_valor_pagamento;
        }
        else if (paramName == "valor_pagar") {
            paramValue = m_valor_pagar;
        }
        if(recNo < m_lista_item_tratamento.count()) {
            if (paramName == "tipo_registro") {
                paramValue = "Itens do Tratamento";
            }
            else if (paramName == "procedimento") {
                paramValue = m_lista_item_tratamento.getByIndex(recNo)->getprocedimento()->getdescricao();
            }
            else if (paramName == "quantidade") {
                paramValue =  QVariant::fromValue(m_lista_item_tratamento.getByIndex(recNo)->getquantidade());
            }
            else if (paramName == "valor_unitario") {
                paramValue =  m_lista_item_tratamento.getByIndex(recNo)->getvalor_unitario();
            }
            else if (paramName == "valor_item") {
                paramValue =  m_lista_item_tratamento.getByIndex(recNo)->getquantidade() *  m_lista_item_tratamento.getByIndex(recNo)->getvalor_unitario();
            }
        } else {
            int newRecNo =  recNo - m_lista_item_tratamento.count();
            if (paramName == "tipo_registro") {
                paramValue = "Recebimentos";
            }
            else if (paramName == "tipo_pagamento") {
                if(m_lista_pagamento.getByIndex(newRecNo)->gettipo() == "DIN") {
                    paramValue = "Dinheiro";
                } else if(m_lista_pagamento.getByIndex(newRecNo)->gettipo() == "CHE") {
                    paramValue = "Cheque";
                } else if(m_lista_pagamento.getByIndex(newRecNo)->gettipo() == "DEB") {
                    paramValue = "C.Débito";
                } else {
                    paramValue = "C.Crédito";
                }
            }
            else if (paramName == "valor_pagamento") {
                paramValue = m_lista_pagamento.getByIndex(newRecNo)->getvalor();
            }
            else if (paramName == "data_realizacao") {
                paramValue = m_lista_pagamento.getByIndex(newRecNo)->getdata_realizacao().toString("dd/MM/yyyy");
            }
            else if (paramName == "data_efetivacao") {
                paramValue = m_lista_pagamento.getByIndex(newRecNo)->getdata_efetivacao().toString("dd/MM/yyyy");
            }
            else if (paramName == "bandeira") {
                if(m_lista_pagamento.getByIndex(newRecNo)->gettipo() == "CRE") {
                    paramValue = m_lista_pagamento.getByIndex(newRecNo)->getcc_bandeira();
                }
            }
            else if (paramName == "autorizacao") {
                if(m_lista_pagamento.getByIndex(newRecNo)->gettipo() == "CRE") {
                    paramValue = m_lista_pagamento.getByIndex(newRecNo)->getcc_autorizacao();
                }
            }
        }
    }
}

