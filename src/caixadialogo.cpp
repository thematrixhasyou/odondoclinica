#include ".\include\caixadialogo.h"
#include "ui_caixadialogo.h"
#include <qtrpt.h>
#include <QMessageBox>


CaixaDialogo::CaixaDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CaixaDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    configurarEventos();
}

CaixaDialogo::~CaixaDialogo()
{
    delete ui;
}

int CaixaDialogo::exec()
{
    init();
    return QDialog::exec();
}

void CaixaDialogo::init()
{
    ui->datMovimento->setDate(QDate::currentDate());
    verificaSituacaoMovimento();
}

void CaixaDialogo::configurarEventos()
{
    connect(ui->btnOk, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnAbrirCaixa, SIGNAL(clicked()), this, SLOT(abrirMovimento()));
    connect(ui->btnFecharCaixa, SIGNAL(clicked()), this, SLOT(fecharMovimento()));
    connect(ui->btnDetalharCaixa, SIGNAL(clicked()), this, SLOT(detalharMovimento()));
    connect(ui->datMovimento, SIGNAL(dateChanged(QDate)), this, SLOT(onDataMovimentoSelecionada()));
}

void CaixaDialogo::verificaSituacaoMovimento()
{
    clinica::caixa_ptr ultimoMovimento = m_caixa->retornaUltimoMovimento();
    m_caixa = m_caixa->retornaMovimento(ui->datMovimento->date());
    ui->btnAbrirCaixa->setText("Abrir");

    limparTotaisMovimento();
    //se nao existe movimento
    if(m_caixa.isNull() || (!m_caixa.isNull() && m_caixa->getdata_movimento().isNull())) {
        ui->dblTroco->setValue(0.0);
        ui->btnDetalharCaixa->setDisabled(true);
        if(!ultimoMovimento.isNull()) {
            //se o movimento eh mais antigo que o ultimo
            if(!ultimoMovimento->getdata_movimento().isNull() && (ui->datMovimento->date() < ultimoMovimento->getdata_movimento())) {
                ui->btnAbrirCaixa->setDisabled(true);
                ui->btnFecharCaixa->setDisabled(true);
            } else { //movimento eh mais recente que o ultimo
                //se o ultimo esta fechado
                if(!ultimoMovimento->getdata_movimento().isNull() && !ultimoMovimento->getdata_hora_fechamento().isNull()) {
                    ui->btnAbrirCaixa->setDisabled(false);
                    ui->btnFecharCaixa->setDisabled(true);
                } else { //o ultimo esta aberto
                    ui->btnAbrirCaixa->setDisabled(true);
                    ui->btnFecharCaixa->setDisabled(true);
                }
            }
        }
        else {
            ui->btnAbrirCaixa->setDisabled(false);
            ui->btnFecharCaixa->setDisabled(true);
        }
    } else { //existe movimento
        ui->dblTroco->setValue(m_caixa->gettroco_inicial());
        ui->btnDetalharCaixa->setDisabled(false);
        //se movimento esta aberto
        if(m_caixa->getdata_hora_fechamento().isNull()) { //movimento esta aberto
            qDebug() << "2.1";
            ui->btnAbrirCaixa->setDisabled(true);
            ui->btnFecharCaixa->setDisabled(false);
        } else { //movimento esta fechado
            qDebug() << "2.2";
            //se o movimento eh o ultimo
            if(!ultimoMovimento->getdata_movimento().isNull() && (ui->datMovimento->date() == ultimoMovimento->getdata_movimento())) { // movmento eh o ultimo
                qDebug() << "2.2.1";
                ui->btnAbrirCaixa->setText("Reabrir");
                ui->btnAbrirCaixa->setDisabled(false);
                ui->btnFecharCaixa->setDisabled(true);
            } else { //nao eh o ultimo
                qDebug() << "2.2.2";
                ui->btnAbrirCaixa->setDisabled(true);
                ui->btnFecharCaixa->setDisabled(true);
            }
        }
        carregaTotaisMovimento();
    }
}

void CaixaDialogo::carregaTotaisMovimento()
{
    m_listaTotaisPagamentos = m_caixa->retornaPagamentosTotalizados();
    m_listaTotaisRetiradas = m_caixa->retornaRetiradasTotalizadas();
    double totalEntrada = 0;
    double totalSaida = 0;

    if(m_listaTotaisPagamentos.exist("DIN")) {
        ui->tblCaixa->item(linha_dinheiro,coluna_entrada)->setText(QString::number(m_listaTotaisPagamentos.getByKey("DIN").toDouble(), 'f', 2));
        totalEntrada += m_listaTotaisPagamentos.getByKey("DIN").toDouble();
    }

    if(m_listaTotaisPagamentos.exist("CHE")) {
        ui->tblCaixa->item(linha_cheque,coluna_entrada)->setText(QString::number(m_listaTotaisPagamentos.getByKey("CHE").toDouble(), 'f', 2));
        totalEntrada += m_listaTotaisPagamentos.getByKey("CHE").toDouble();
    }

    if(m_listaTotaisPagamentos.exist("DEB")) {
        ui->tblCaixa->item(linha_cartao_debito,coluna_entrada)->setText(QString::number(m_listaTotaisPagamentos.getByKey("DEB").toDouble(), 'f', 2));
        totalEntrada += m_listaTotaisPagamentos.getByKey("DEB").toDouble();
    }

    if(m_listaTotaisPagamentos.exist("CRE")) {
        ui->tblCaixa->item(linha_cartao_credito,coluna_entrada)->setText(QString::number(m_listaTotaisPagamentos.getByKey("CRE").toDouble(), 'f', 2));
        totalEntrada += m_listaTotaisPagamentos.getByKey("CRE").toDouble();
    }

    if(m_listaTotaisRetiradas.exist("DIN")) {
        ui->tblCaixa->item(linha_dinheiro,coluna_saida)->setText(QString::number(m_listaTotaisRetiradas.getByKey("DIN").toDouble(), 'f', 2));
        totalSaida += m_listaTotaisRetiradas.getByKey("DIN").toDouble();
    }

    if(m_listaTotaisRetiradas.exist("CHE")) {
        ui->tblCaixa->item(linha_cheque,coluna_saida)->setText(QString::number(m_listaTotaisRetiradas.getByKey("CHE").toDouble(), 'f', 2));
        totalSaida += m_listaTotaisRetiradas.getByKey("CHE").toDouble();
    }

    ui->tblCaixa->item(linha_total,coluna_entrada)->setText(QString::number(totalEntrada, 'f', 2));
    ui->tblCaixa->item(linha_total,coluna_saida)->setText(QString::number(totalSaida, 'f', 2));

    ui->tblCaixa->item(linha_saldo,coluna_entrada)->setText(QString::number(totalEntrada - totalSaida, 'f', 2));
}

void CaixaDialogo::limparTotaisMovimento()
{
    ui->tblCaixa->item(linha_dinheiro,coluna_entrada)->setText("");
    ui->tblCaixa->item(linha_cheque,coluna_entrada)->setText("");
    ui->tblCaixa->item(linha_cartao_debito,coluna_entrada)->setText("");
    ui->tblCaixa->item(linha_cartao_credito,coluna_entrada)->setText("");

    ui->tblCaixa->item(linha_dinheiro,coluna_saida)->setText("");
    ui->tblCaixa->item(linha_cheque,coluna_saida)->setText("");

    ui->tblCaixa->item(linha_total,coluna_entrada)->setText("");
    ui->tblCaixa->item(linha_total,coluna_saida)->setText("");

    ui->tblCaixa->item(linha_saldo,coluna_entrada)->setText("");
}

void CaixaDialogo::onDataMovimentoSelecionada()
{
    verificaSituacaoMovimento();
}

void CaixaDialogo::abrirMovimento()
{
    QString retorno;
    if(m_caixa.isNull()) {
        m_caixa.reset(new clinica::caixa());
        m_caixa->setdata_movimento(ui->datMovimento->date());
    } else {
        m_caixa->setdata_hora_fechamento(QDateTime());
    }
    m_caixa->settroco_inicial(ui->dblTroco->value());

    retorno = m_caixa->salvar();
    if(retorno.compare("Ok") != 0) {
        QMessageBox::critical(this, "Salvar", "Erro ao abrir Movimento de Caixa.\n" + retorno);
        return;
    }
    verificaSituacaoMovimento();
}

void CaixaDialogo::fecharMovimento()
{
    m_caixa->setdata_hora_fechamento(QDateTime::currentDateTime());

    QString retorno = m_caixa->salvar();
    if(retorno.compare("Ok") != 0) {
        QMessageBox::critical(this, "Salvar", "Erro ao fechar Movimento de Caixa.\n" + retorno);
        return;
    }
    verificaSituacaoMovimento();
}

void CaixaDialogo::detalharMovimento()
{
    m_listaDetalhePagamento = m_caixa->retornaListaDetalhePagamento();
    m_listaRetirada = m_caixa->getlistaRetirada(true);

    if(m_listaDetalhePagamento.count() == 0 && m_listaRetirada.count() == 0) {
        QMessageBox::information(this, "Detalhar", "Não há nenhum pagamento ou retirada realizado.");
    } else {
        QtRPT *report = new QtRPT(this);
        report->recordCount << m_listaDetalhePagamento.count();
        report->recordCount << m_listaRetirada.count();
        report->loadReport("detalhe_movimento_caixa.xml");
        QObject::connect(report, SIGNAL(setValue(const int, const QString, QVariant&, const int)), this, SLOT(setValue(const int, const QString, QVariant&, const int)));
        report->printExec();
    }
}

void CaixaDialogo::setValue(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage)
{
    double total_retirada, total_pagamento;

    total_pagamento = 0.0f;
    if(m_listaTotaisPagamentos.exist("DIN")) {
        total_pagamento += m_listaTotaisPagamentos.getByKey("DIN").toDouble();
    }
    if(m_listaTotaisPagamentos.exist("CHE")) {
        total_pagamento += m_listaTotaisPagamentos.getByKey("CHE").toDouble();
    }
    if(m_listaTotaisPagamentos.exist("DEB")) {
        total_pagamento += m_listaTotaisPagamentos.getByKey("DEB").toDouble();
    }
    if(m_listaTotaisPagamentos.exist("CRE")) {
        total_pagamento += m_listaTotaisPagamentos.getByKey("CRE").toDouble();
    }

    total_retirada = 0.0f;
    if(m_listaTotaisRetiradas.exist("DIN")) {
        total_retirada += m_listaTotaisRetiradas.getByKey("DIN").toDouble();
    }
    if(m_listaTotaisRetiradas.exist("CHE")) {
        total_retirada += m_listaTotaisRetiradas.getByKey("CHE").toDouble();
    }

    if (reportPage == 0) {
        if (paramName == "data_movimento") {
            paramValue = m_caixa->getdata_movimento().toString("dd/MM/yyyy");
        } else if (paramName == "nome") {
            paramValue = m_listaDetalhePagamento.at(recNo).nome_cliente;
        } else if (paramName == "tipo") {
            paramValue = m_listaDetalhePagamento.at(recNo).tipo_pagamento;
        } else if (paramName == "valor") {
            paramValue = m_listaDetalhePagamento.at(recNo).valor;
        } else if (paramName == "data_realizacao") {
            paramValue = m_listaDetalhePagamento.at(recNo).data_realizacao;
        } else if (paramName == "data_efetivacao") {
            paramValue = m_listaDetalhePagamento.at(recNo).data_efetivacao;
        } else if (paramName == "bandeira") {
            paramValue = m_listaDetalhePagamento.at(recNo).bandeira_cartao;
        } else if (paramName == "autorizacao") {
            paramValue = m_listaDetalhePagamento.at(recNo).autorizacao_cartao;
        }
    }
    if (reportPage == 1) {
        if (paramName == "data_movimento") {
            paramValue = m_caixa->getdata_movimento().toString("dd/MM/yyyy");
        } else if (paramName == "autor") {
            paramValue = m_listaRetirada.getByIndex(recNo)->getautor();
        } else if (paramName == "tipo") {
            paramValue = m_listaRetirada.getByIndex(recNo)->gettipo();
        } else if (paramName == "valor") {
            paramValue = m_listaRetirada.getByIndex(recNo)->getvalor();
        } else if (paramName == "motivo") {
            paramValue = m_listaRetirada.getByIndex(recNo)->getmotivo().left(50);
        } else if (paramName == "total_pagamento_dinheiro" && m_listaTotaisPagamentos.exist("DIN")) {
            paramValue = m_listaTotaisPagamentos.getByKey("DIN");
        } else if (paramName == "total_pagamento_cheque" && m_listaTotaisPagamentos.exist("CHE")) {
            paramValue = m_listaTotaisPagamentos.getByKey("CHE");
        } else if (paramName == "total_pagamento_debito" && m_listaTotaisPagamentos.exist("DEB")) {
            paramValue = m_listaTotaisPagamentos.getByKey("DEB");
        } else if (paramName == "total_pagamento_credito" && m_listaTotaisPagamentos.exist("CRE")) {
            paramValue = m_listaTotaisPagamentos.getByKey("CRE");
        } else if (paramName == "total_pagamento") {
            paramValue = QString::number(total_pagamento, 'f', 2);
        } else if (paramName == "total_retirada_dinheiro" && m_listaTotaisRetiradas.exist("DIN")) {
            paramValue = m_listaTotaisRetiradas.getByKey("DIN");
        } else if (paramName == "total_retirada_cheque" && m_listaTotaisRetiradas.exist("CHE")) {
            paramValue = m_listaTotaisRetiradas.getByKey("CHE");
        } else if (paramName == "total_retirada") {
            paramValue = QString::number(total_retirada, 'f', 2);
        } else if (paramName == "saldo") {
            paramValue = QString::number(total_pagamento-total_retirada, 'f', 2);
        }
    }
}

