#include <QtWidgets>
#include <qtrpt.h>
#include "include/principal.h"
#include "include/sobredialogo.h"
#include "include/clientedialogo.h"
#include "include/planodialogo.h"
#include "include/dentistadialogo.h"
#include "include/procedimentodialogo.h"
#include "include/agendamentodialogo.h"
#include "include/consultaragendadialogo.h"
#include "include/tratamentodialogo.h"
#include "include/caixadialogo.h"
#include "include/retiradadialogo.h"
#include "include/pagamentopopup.h"
#include "include/relatoriofechamentodialogo.h"
#include "include/relatoriofechamentodetalhadodialogo.h"
#include "ui_principal.h"

Principal::Principal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Principal)
{
    ui->setupUi(this);
    init();
}

Principal::~Principal()
{
    delete ui;
}

void Principal::init()
{
    connect(ui->actionSobre, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionClientes, SIGNAL(triggered()), this, SLOT(cliente()));
    connect(ui->actionPlanos, SIGNAL(triggered()), this, SLOT(plano()));
    connect(ui->actionDentistas, SIGNAL(triggered()), this, SLOT(dentista()));
    connect(ui->actionProcedimentos, SIGNAL(triggered()), this, SLOT(procedimento()));
    connect(ui->actionMarcacao, SIGNAL(triggered()), this, SLOT(agendamento()));
    connect(ui->actionConsulta, SIGNAL(triggered()), this, SLOT(consultarAgenda()));
    connect(ui->actionTratamento, SIGNAL(triggered()), this, SLOT(cadastroTratamento()));
    connect(ui->actionMovimento, SIGNAL(triggered()), this, SLOT(caixaMovimento()));
    connect(ui->actionRetirada, SIGNAL(triggered()), this, SLOT(caixaRetirada()));
    connect(ui->actionPagamento, SIGNAL(triggered()), this, SLOT(caixaPagamento()));
    connect(ui->actionRelFechamentoConsolidado, SIGNAL(triggered()), this, SLOT(relatorioFechamentoConsolidado()));
    connect(ui->actionRelFechamentoDetalhado, SIGNAL(triggered()), this, SLOT(relatorioFechamentoDetalhado()));
    connect(ui->actionRelTratamentoDevedor, SIGNAL(triggered()), this, SLOT(relatorioTratamentoDevedor()));
    readSettings();
    ui->statusBar->showMessage("Seja bem-vindo.", 30000);
    connectDatabase();
}


void Principal::readSettings()
{
    QSettings settings("clinica.ini", QSettings::IniFormat);
    QPoint pos = settings.value("pos", QPoint(100, 10)).toPoint();
    QSize size = settings.value("size", QSize(1024, 670)).toSize();
    move(pos);
    resize(size);
}

void Principal::writeSettings()
{
    QSettings settings("clinica.ini", QSettings::IniFormat);
    settings.setValue("pos", pos());
    settings.setValue("size", size());
}

void Principal::closeEvent(QCloseEvent *event)
{
    writeSettings();
    event->accept();
}

void Principal::about()
{
    SobreDialogo *sobreDlg = new SobreDialogo(this);
    sobreDlg->setModal(true);
    sobreDlg->show();
}

void Principal::cliente() {
    ClienteDialogo *clienteDlg = new ClienteDialogo(this);
    clienteDlg->show();
}

void Principal::plano() {
    PlanoDialogo *planoDlg = new PlanoDialogo(this);
    planoDlg->show();
}

void Principal::dentista() {
    DentistaDialogo *dentistaDlg = new DentistaDialogo(this);
    dentistaDlg->show();
}

void Principal::procedimento()
{
    ProcedimentoDialogo * procedimentoDlg = new ProcedimentoDialogo(this);
    procedimentoDlg->show();
}

void Principal::agendamento()
{
    AgendamentoDialogo *agendaDlg = new AgendamentoDialogo(this);
    agendaDlg->show();
}

void Principal::consultarAgenda()
{
    ConsultarAgendaDialogo *consultarDlg = new ConsultarAgendaDialogo(this);
    consultarDlg->show();
}

void Principal::cadastroTratamento()
{
    TratamentoDialogo *tratamentoDlg = new TratamentoDialogo(this);
    tratamentoDlg->show();
}

void Principal::caixaMovimento()
{
    CaixaDialogo *caixaMovimentoDlg = new CaixaDialogo(this);
    caixaMovimentoDlg->exec();
}

void Principal::caixaRetirada()
{
    RetiradaDialogo *retiradaDlg = new RetiradaDialogo(this);
    retiradaDlg->exec();
}

void Principal::caixaPagamento()
{
    Pagamentopopup *pagamentoPopup = new Pagamentopopup(this);
    pagamentoPopup->exec();
}

void Principal::relatorioFechamentoConsolidado()
{
    RelatorioFechamentoDialogo *relatorioDlg = new RelatorioFechamentoDialogo(this);
    relatorioDlg->exec();
}

void Principal::relatorioFechamentoDetalhado()
{
    RelatorioFechamentoDetalhadoDialogo *relatorioDlg = new RelatorioFechamentoDetalhadoDialogo(this);
    relatorioDlg->exec();
}

void Principal::relatorioTratamentoDevedor()
{
    clinica::tratamento tratamento;
    m_listaTratamentoDevedor = tratamento.retornaListaTratamentoDevedor();

    if(m_listaTratamentoDevedor.count() == 0) {
        QMessageBox::information(this, "Relatório de Tratamentos Devedores", "Não há tratamentos devedores.");
        return;
    }

    QtRPT *report = new QtRPT(this);
    report->recordCount << m_listaTratamentoDevedor.count();
    report->loadReport("relatorio_tratamento_devedor.xml");
    QObject::connect(report, SIGNAL(setValue(const int, const QString, QVariant&, const int)), this, SLOT(setValoresRelTratamentoDevedor(const int, const QString, QVariant&, const int)));
    report->printExec();
}


void Principal::connectDatabase() {
    QSettings settings("clinica.ini", QSettings::IniFormat);
    QString host_name = settings.value("db_host_name", "localhost").toString();
    QString user_name = settings.value("db_user_name", "thematrix").toString();

    // Parameters to connect to database
    qx::QxSqlDatabase::getSingleton()->setDriverName("QMYSQL");
    qx::QxSqlDatabase::getSingleton()->setDatabaseName("clinica");
    qx::QxSqlDatabase::getSingleton()->setHostName(host_name);
    qx::QxSqlDatabase::getSingleton()->setUserName(user_name);
    qx::QxSqlDatabase::getSingleton()->setPassword("h@sYou");
    qx::QxSqlDatabase::getSingleton()->setPort(3306);

    // Only for debug purpose : assert if invalid offset detected fetching a relation
    qx::QxSqlDatabase::getSingleton()->setVerifyOffsetRelation(true);
}

void Principal::setValoresRelTratamentoDevedor(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage)
{
    if (reportPage == 0) {
        if (paramName == "nome_dentista") {
            paramValue = m_listaTratamentoDevedor.at(recNo).nome_dentista;
        }
        else if (paramName == "nome_cliente") {
            paramValue = m_listaTratamentoDevedor.at(recNo).nome_cliente;
        }
        else if (paramName == "data_inicio") {
            paramValue = m_listaTratamentoDevedor.at(recNo).data_inicio;
        }
        else if (paramName == "valor_tratamento") {
            paramValue = m_listaTratamentoDevedor.at(recNo).valor_tratamento;
        }
        else if (paramName == "valor_pagto") {
            paramValue = m_listaTratamentoDevedor.at(recNo).valor_pagto;
        }
        else if (paramName == "saldo_devedor") {
            paramValue = m_listaTratamentoDevedor.at(recNo).saldo_devedor;
        }
    }
}
