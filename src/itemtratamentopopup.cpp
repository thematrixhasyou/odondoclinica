#include ".\include\itemtratamentopopup.h"
#include "ui_itemtratamentopopup.h"
#include <QMessageBox>

ItemTratamentoPopup::ItemTratamentoPopup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ItemTratamentoPopup)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    init();
    configurarEventos();
}

ItemTratamentoPopup::~ItemTratamentoPopup()
{
    delete ui;
}

clinica::procedimento_ptr ItemTratamentoPopup::getProcedimentoSelecionado()
{
    clinica::procedimento_ptr procedimento;
    procedimento.reset(new clinica::procedimento());
    if(ui->procedimentoCmb->currentIndex() >= 0) {
        procedimento = m_listaProcedimentos.getByIndex(ui->procedimentoCmb->currentIndex());
    }
    m_procedimento = procedimento;
    return m_procedimento;
}

QString ItemTratamentoPopup::getSituacaoSelecionada()
{
    if(ui->situacaoCmb->currentIndex() >= 0) {
        m_situacao = ui->situacaoCmb->currentData().toString();
    }
    return m_situacao;
}

long ItemTratamentoPopup::getQuantidadeSelecionada()
{
    m_quantidade = ui->quantidadeSpn->value();
    return m_quantidade;
}

double ItemTratamentoPopup::getValorUnitarioSelecionado()
{
    m_valorUnitario = ui->valorUnitarioSpn->value();
    return m_valorUnitario;
}

int ItemTratamentoPopup::exec()
{
    m_procedimento.reset(new clinica::procedimento());
    ui->procedimentoCmb->setCurrentIndex(-1);
    m_quantidade = 0;
    ui->quantidadeSpn->setValue(m_quantidade);
    m_valorUnitario = 0.0f;
    ui->valorUnitarioSpn->setValue(m_valorUnitario);
    ui->valorTotalEdt->setText("0.00");
    m_situacao = "NIN";
    ui->situacaoCmb->setCurrentIndex(0);
    return QDialog::exec();
}

int ItemTratamentoPopup::exec(clinica::procedimento_ptr procedimento, long quantidade, double valorUnitario, QString situacao)
{
    m_procedimento = procedimento;
    ui->procedimentoCmb->setCurrentText(m_procedimento->getdescricao());
    m_quantidade = quantidade;
    ui->quantidadeSpn->setValue(m_quantidade);
    m_valorUnitario = valorUnitario;
    ui->valorUnitarioSpn->setValue(m_valorUnitario);
    ui->valorTotalEdt->setText(QString::number(m_quantidade * m_valorUnitario, 'f', 2));
    m_situacao = situacao;
    ui->situacaoCmb->setCurrentIndex(ui->situacaoCmb->findData(QVariant::fromValue(m_situacao)));

    return QDialog::exec();
}

void ItemTratamentoPopup::accept()
{
    if(ui->procedimentoCmb->currentIndex() < 0) {
        QMessageBox::warning(this, "Item de Tratamento", "Selecione um Procedimento.", QMessageBox::Ok);
    } else if (ui->quantidadeSpn->value() <= 0) {
        QMessageBox::warning(this, "Item de Tratamento", "Informe a quantidade.", QMessageBox::Ok);
    } else if (ui->valorUnitarioSpn->value() <= 0.0f) {
        QMessageBox::warning(this, "Item de Tratamento", "Informe o valor unitário.", QMessageBox::Ok);
    } else if (ui->situacaoCmb->currentIndex() < 0) {
        QMessageBox::warning(this, "Item de Tratamento", "Selecione uma situação.", QMessageBox::Ok);
    } else {
        QDialog::accept();
    }
}

void ItemTratamentoPopup::calcularValorItem()
{
    getQuantidadeSelecionada();
    getValorUnitarioSelecionado();
    ui->valorTotalEdt->setText(QString::number(m_quantidade * m_valorUnitario, 'f', 2));
}

void ItemTratamentoPopup::init()
{
    m_listaProcedimentos = clinica::procedimento::retornaTodosProcedimentosOrdenadosPorNome();
    _foreach(QSharedPointer<clinica::procedimento> procedimento, m_listaProcedimentos) {
        ui->procedimentoCmb->addItem(procedimento->getdescricao(), QVariant::fromValue(procedimento->getprocedimento_id()));
    }

    ui->situacaoCmb->insertItem(0, "Não Iniciado", QVariant("NIN"));
    ui->situacaoCmb->insertItem(1, "Em Andamento", QVariant("EAN"));
    ui->situacaoCmb->insertItem(2, "Concluído", QVariant("CON"));
}

void ItemTratamentoPopup::configurarEventos()
{
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(ui->quantidadeSpn, SIGNAL(valueChanged(int)), this, SLOT(calcularValorItem()));
    connect(ui->valorUnitarioSpn, SIGNAL(valueChanged(double)), this, SLOT(calcularValorItem()));
}
