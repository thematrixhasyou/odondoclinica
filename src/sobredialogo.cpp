#include <QLineEdit>
#include <QVBoxLayout>

#include "include/sobredialogo.h"
#include "include/wigglywidget.h"

//! [0]
SobreDialogo::SobreDialogo(QWidget *parent)
    : QDialog(parent)
{
    setWindowFlags(Qt::Tool | Qt::MSWindowsFixedSizeDialogHint);
    WigglyWidget *wigglyWidget = new WigglyWidget;

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(wigglyWidget);

    wigglyWidget->setText(tr("Clínica Odontológica L+"));

    setWindowTitle(tr("Sobre"));
    resize(450, 300);
}
//! [0]
