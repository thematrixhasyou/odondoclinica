#include ".\include\tratamentopopup.h"
#include "ui_tratamentopopup.h"
#include <QMessageBox>

TratamentoPopup::TratamentoPopup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TratamentoPopup)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    init();
}

TratamentoPopup::~TratamentoPopup()
{
    delete ui;
}

clinica::dentista_ptr TratamentoPopup::getDentistaSelecionado()
{
    clinica::dentista_ptr dentista;
    dentista.reset(new clinica::dentista());
    if(ui->dentistaCmb->currentIndex() >= 0) {
        dentista = m_lista_dentista.getByIndex(ui->dentistaCmb->currentIndex());
    }
    return dentista;
}

QDate TratamentoPopup::getDataSelecionada()
{
    return ui->dataCal->selectedDate();
}

void TratamentoPopup::accept()
{
    if(m_acao == tipoAcao::iniciar && ui->dentistaCmb->currentIndex() < 0) {
        QMessageBox::warning(this, "Novo Tratamento", "Selecione um Dentista.", QMessageBox::Ok);
    } else if (m_acao == tipoAcao::iniciar && ui->dataCal->selectedDate().isNull()) {
        QMessageBox::warning(this, "Novo Tratamento", "Selecione a data inicial.", QMessageBox::Ok);
    } else if (m_acao == tipoAcao::encerrar && ui->dataCal->selectedDate().isNull()) {
        QMessageBox::warning(this, "Novo Tratamento", "Selecione a data de término.", QMessageBox::Ok);
    } else {
        QDialog::accept();
    }
}

int TratamentoPopup::exec(tipoAcao acao)
{
    m_acao = acao;
    ui->dentistaCmb->setCurrentIndex(-1);

    if(acao == tipoAcao::iniciar) {
        ui->lblData->setText("Data de Início:");
        ui->dentistaCmb->setDisabled(false);
    } else if(acao == tipoAcao::encerrar) {
        ui->lblData->setText("Data de Término:");
        ui->dentistaCmb->setDisabled(true);
    }
    return QDialog::exec();
}

void TratamentoPopup::init()
{
    m_lista_dentista.clear();
    carregarListaDentista();
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

void TratamentoPopup::carregarListaDentista()
{
    ui->dentistaCmb->clear();

    m_lista_dentista = clinica::dentista::retornaTodosDentistasOrdenadosPorNome();
    if(m_lista_dentista.count() > 0) {
        _foreach(QSharedPointer<clinica::dentista> dentista, m_lista_dentista) {
            ui->dentistaCmb->addItem(dentista->getnome(), QVariant::fromValue(dentista->getid()));
        }
        ui->dentistaCmb->setCurrentIndex(-1);
    }
}
