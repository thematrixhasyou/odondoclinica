#include "include/procedimentodialogo.h"
#include "ui_procedimentodialogo.h"
#include "persist/include/clinica_procedimento.h"
#include <QMessageBox>

ProcedimentoDialogo::ProcedimentoDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProcedimentoDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    ui->procedimentoLst->installEventFilter(this);
    setModal(true);
    configurarEventos();
    carregaListaProcedimento();
}

ProcedimentoDialogo::~ProcedimentoDialogo()
{
    delete ui;
}

void ProcedimentoDialogo::configurarEventos()
{
    connect(ui->novoBtn, SIGNAL(clicked()), this, SLOT(novo()));
    connect(ui->salvarBtn, SIGNAL(clicked()), this, SLOT(salvar()));
    connect(ui->inativarBtn, SIGNAL(clicked()), this, SLOT(inativar()));
    connect(ui->fecharBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->procedimentoLst, SIGNAL(currentRowChanged(int)), this, SLOT(onProcedimentoSelecionado(int)));
}

void ProcedimentoDialogo::novo() {
    m_procedimento.reset(new clinica::procedimento());
    limpar();
}

void ProcedimentoDialogo::limpar() {
    ui->descricaoEdt->clear();
    ui->descricaoEdt->setFocus();
}

bool ProcedimentoDialogo::eventFilter(QObject *obj, QEvent *ev) {
    if(obj == ui->procedimentoLst && ev->type() == QEvent::FocusIn) {
        onListaRecebeFoco();
    }
    return false;
}

void ProcedimentoDialogo::onListaRecebeFoco() {
    if(ui->procedimentoLst->count() > 0) {
        ui->procedimentoLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

void ProcedimentoDialogo::carregaListaProcedimento() {
    ui->procedimentoLst->clear();

    m_lista_procedimentos = clinica::procedimento::retornaTodosProcedimentosOrdenadosPorNome();
    if(m_lista_procedimentos.count() > 0) {
        _foreach(QSharedPointer<clinica::procedimento> procedimento, m_lista_procedimentos) {
            ui->procedimentoLst->addItem(procedimento->getdescricao());
        }
        ui->procedimentoLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

void ProcedimentoDialogo::onProcedimentoSelecionado(int row) {
    if(row >= 0 && row < m_lista_procedimentos.count()) {
        m_procedimento = m_lista_procedimentos.getByIndex(row);

        ui->descricaoEdt->setText(m_procedimento->getdescricao());
    }
}

void ProcedimentoDialogo::salvar() {
    m_procedimento->setdescricao(ui->descricaoEdt->text());

    QString retorno = m_procedimento->salvar();
    if(retorno.compare("Ok") == 0) {
        QMessageBox::information(this, "Salvar", "Procedimento salvo com sucesso.");
        limpar();
        carregaListaProcedimento();
    } else {
        QMessageBox::critical(this, "Salvar", "Erro ao salvar Procedimento.\n" + retorno);
    }
}

void ProcedimentoDialogo::inativar() {
    if(QMessageBox::question(this, "Inativar", "O procedimento não ficará mais disponível.\nConfirma inativar o procedimento?") == QMessageBox::Yes) {
        QString retorno = m_procedimento->inativar();
        if(retorno.compare("Ok") == 0) {
            QMessageBox::information(this, "Inativar", "Procedimento inativado com sucesso.");
            limpar();
            carregaListaProcedimento();
        } else {
            QMessageBox::critical(this, "Inativar", "Erro ao inativar Procedimento.\n" + retorno);
        }
    }
}
