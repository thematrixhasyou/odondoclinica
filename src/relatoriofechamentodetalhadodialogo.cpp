#include ".\include\relatoriofechamentodetalhadodialogo.h"
#include "ui_relatoriofechamentodetalhadodialogo.h"
#include <qtrpt.h>

#include <QMessageBox>

RelatorioFechamentoDetalhadoDialogo::RelatorioFechamentoDetalhadoDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RelatorioFechamentoDetalhadoDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    configurarEventos();
}

RelatorioFechamentoDetalhadoDialogo::~RelatorioFechamentoDetalhadoDialogo()
{
    delete ui;
}

int RelatorioFechamentoDetalhadoDialogo::exec()
{
    init();
    return QDialog::exec();
}

void RelatorioFechamentoDetalhadoDialogo::init()
{
    ui->datInicio->setDate(QDate::currentDate());
    ui->datTermino->setDate(QDate::currentDate());
    ui->rdbProducao->setChecked(true);
}

void RelatorioFechamentoDetalhadoDialogo::configurarEventos()
{
    connect(ui->btnFechar, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnImprimir, SIGNAL(clicked()), this, SLOT(imprimir()));
}

void RelatorioFechamentoDetalhadoDialogo::setValue(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage)
{
    if (reportPage == 0) {
        if (paramName == "data_inicio") {
            paramValue = ui->datInicio->date().toString("dd/MM/yyyy");
        }
        else if (paramName == "data_termino") {
            paramValue = ui->datTermino->date().toString("dd/MM/yyyy");
        }
        else if (paramName == "subtitulo") {
            if(ui->rdbProducao->isChecked()) {
                paramValue = "Produção no Período";
            }
            else if(ui->rdbFaturamentoCorrente->isChecked()) {
                paramValue = "Faturamento no Período";
            }
            else if(ui->rdbFaturamentoFuturo->isChecked()) {
                paramValue = "Faturamento Futuro";
            }
        }
        else if (paramName == "nome_dentista") {
            paramValue = m_listaDetalhe.at(recNo).nome_dentista;
        }
        else if (paramName == "nome_cliente") {
            paramValue = m_listaDetalhe.at(recNo).nome_cliente;
        }
        else if (paramName == "tipo") {
            paramValue = m_listaDetalhe.at(recNo).tipo_pagamento;
        }
        else if (paramName == "valor") {
            paramValue = m_listaDetalhe.at(recNo).valor;
        }
        else if (paramName == "data_realizacao") {
            paramValue = m_listaDetalhe.at(recNo).data_realizacao;
        }
        else if (paramName == "data_efetivacao") {
            paramValue = m_listaDetalhe.at(recNo).data_efetivacao;
        }
        else if (paramName == "bandeira") {
            paramValue = m_listaDetalhe.at(recNo).bandeira_cartao;
        }
        else if (paramName == "autorizacao") {
            paramValue = m_listaDetalhe.at(recNo).autorizacao_cartao;
        }
    }
}

void RelatorioFechamentoDetalhadoDialogo::imprimir()
{
    clinica::pagamento pagamento;
    if(ui->datTermino->date() <= ui->datInicio->date()) {
        QMessageBox::information(this, "Relatório de Fechamento", "A data de término não pode ser menor ou igual a data de início do período.");
        return;
    }

    if(ui->rdbProducao->isChecked()) {
        m_listaDetalhe = pagamento.retornaDetalheProducaoPeriodo(ui->datInicio->date(), ui->datTermino->date());
    }
    else if(ui->rdbFaturamentoCorrente->isChecked()) {
        m_listaDetalhe = pagamento.retornaDetalheFaturamentoPeriodo(ui->datInicio->date(), ui->datTermino->date());
    }
    else if(ui->rdbFaturamentoFuturo->isChecked()) {
        m_listaDetalhe = pagamento.retornaDetalheFaturamentoFuturo(ui->datTermino->date());
    }

    if(m_listaDetalhe.count() == 0) {
        QMessageBox::information(this, "Relatório de Fechamento", "Não há pagamentos realizados no período.");
        return;
    }

    QtRPT *report = new QtRPT(this);
    report->recordCount << m_listaDetalhe.count();
    report->loadReport("relatorio_fechamento_detalhado.xml");
    QObject::connect(report, SIGNAL(setValue(const int, const QString, QVariant&, const int)), this, SLOT(setValue(const int, const QString, QVariant&, const int)));
    report->printExec();
}
