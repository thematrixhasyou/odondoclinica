#include <QtWidgets>
#include <QMessageBox>
#include "include/clientedialogo.h"
#include "persist/include/clinica_cliente.h"
#include "persist/include/clinica_plano_odontologico.h"
#include "ui_clientedialogo.h"

ClienteDialogo::ClienteDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClienteDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    ui->resultadoLst->installEventFilter(this);
    configurarEventos();
    carregarComboBoxes();
}

ClienteDialogo::~ClienteDialogo()
{
    delete ui;
}

void ClienteDialogo::configurarEventos()
{
    connect(ui->fecharBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->novoBtn, SIGNAL(clicked()), this, SLOT(novo()));
    connect(ui->salvarBtn, SIGNAL(clicked()), this, SLOT(salvar()));
    connect(ui->nomePesquisarEdt, SIGNAL(textChanged(QString)), this, SLOT(onNomePesquisaKeyPress(QString)));
    connect(ui->limparBtn, SIGNAL(clicked()), this, SLOT(limpar()));
    connect(ui->resultadoLst, SIGNAL(currentRowChanged(int)), this, SLOT(onResultadoSelecionado(int)));
    connect(ui->inativarBtn, SIGNAL(clicked()), this, SLOT(inativar()));
}

void ClienteDialogo::carregarComboBoxes()
{
    carregaComboPlano();
    carregaComboSexo();
    carregaComboUF();
}

void ClienteDialogo::novo() {
    m_cliente.reset(new clinica::cliente());
    limpar();
    ui->nomeEdt->setFocus();
}

void ClienteDialogo::limparCliente() {
    ui->fichaLbl->setText("");
    ui->nomeEdt->clear();
    ui->identidadeEdt->clear();
    ui->cpfEdt->clear();
    ui->sexoCmb->setCurrentIndex(-1);
    ui->profissaoEdt->clear();
    ui->planoCmb->setCurrentText("Nenhum");
    ui->cepEdt->clear();
    ui->logradouroEdt->clear();
    ui->numeroEdt->clear();
    ui->complementoEdt->clear();
    ui->bairroEdt->clear();
    ui->cidadeEdt->clear();
    ui->ufCmb->setCurrentText("RJ");
    ui->dddCelularEdt->setText("21");
    ui->telefoneCelularEdt->clear();
    ui->dddFixoEdt->setText("21");
    ui->telefoneFixoEdt->clear();
    ui->observacaoTxt->clear();
}

void ClienteDialogo::carregaComboUF() {
    ui->ufCmb->clear();
    ui->ufCmb->addItem("AC", "AC");
    ui->ufCmb->addItem("AL", "AL");
    ui->ufCmb->addItem("AP", "AP");
    ui->ufCmb->addItem("AM", "AM");
    ui->ufCmb->addItem("BA", "BA");
    ui->ufCmb->addItem("CE", "CE");
    ui->ufCmb->addItem("DF", "DF");
    ui->ufCmb->addItem("ES", "ES");
    ui->ufCmb->addItem("GO", "GO");
    ui->ufCmb->addItem("MA", "MA");
    ui->ufCmb->addItem("MT", "MT");
    ui->ufCmb->addItem("MS", "MS");
    ui->ufCmb->addItem("MG", "MG");
    ui->ufCmb->addItem("PA", "PA");
    ui->ufCmb->addItem("PB", "PB");
    ui->ufCmb->addItem("PR", "PR");
    ui->ufCmb->addItem("PE", "PE");
    ui->ufCmb->addItem("PI", "PI");
    ui->ufCmb->addItem("RJ", "RJ");
    ui->ufCmb->addItem("RN", "RN");
    ui->ufCmb->addItem("RS", "RS");
    ui->ufCmb->addItem("RO", "RO");
    ui->ufCmb->addItem("RR", "RR");
    ui->ufCmb->addItem("SC", "SC");
    ui->ufCmb->addItem("SP", "SP");
    ui->ufCmb->addItem("SE", "SE");
    ui->ufCmb->addItem("TO", "TO");
    ui->ufCmb->setCurrentIndex(-1);
}

void ClienteDialogo::carregaComboSexo() {
    ui->sexoCmb->clear();
    ui->sexoCmb->addItem("Feminino", "F");
    ui->sexoCmb->addItem("Masculino", "M");
    ui->sexoCmb->setCurrentIndex(-1);
}

void ClienteDialogo::carregaComboPlano() {
    ui->planoCmb->clear();
    ui->planoCmb->addItem("Nenhum", -1);

    clinica::list_of_plano_odontologico lista_planos = clinica::plano_odontologico::retornaTodosPlanosOrdenadosPorNome();
    if(lista_planos.count() > 0) {
        _foreach(QSharedPointer<clinica::plano_odontologico> plano, lista_planos) {
            ui->planoCmb->addItem(plano->getnome(), QVariant::fromValue(plano->getplano_odontologico_id()));
        }
    }
    ui->planoCmb->setCurrentText("Nenhum");
}

void ClienteDialogo::salvar() {

    if(ui->nomeEdt->text().trimmed().isEmpty() || ui->nomeEdt->text().trimmed().length() < 9) {
        QMessageBox::information(this, "Salvar Cliente", "Informe o nome do cliente com no mínimo 9 caracteres.");
        ui->nomeEdt->setFocus();
        return;
    }
    if(ui->sexoCmb->currentIndex() < 0) {
        QMessageBox::information(this, "Salvar Cliente", "Informe o sexo do cliente.");
        ui->sexoCmb->setFocus();
        return;
    }
    if(m_cliente->getcliente_id() <= 0 && !isEmptyCPF(ui->cpfEdt->text()) && clinica::cliente::existeCPF(ui->cpfEdt->text())) {
        QMessageBox::information(this, "Salvar Cliente", "O CPF informado já está cadastrado, faça a pesquisa pelo CPF.");
        return;
    }

    m_cliente->setnome(ui->nomeEdt->text().trimmed());
    if(!ui->identidadeEdt->text().trimmed().isEmpty()) {
        m_cliente->setdoc_identidade(ui->identidadeEdt->text());
    }
    if(!isEmptyCPF(ui->cpfEdt->text())) {
        m_cliente->setcpf(ui->cpfEdt->text());
    }
    m_cliente->setprofissao(ui->profissaoEdt->text());
    m_cliente->setcep(ui->cepEdt->text());
    m_cliente->setlogradouro(ui->logradouroEdt->text());
    m_cliente->setnum_endereco(ui->numeroEdt->text());
    m_cliente->setcompl_endereco(ui->complementoEdt->text());
    m_cliente->setbairro(ui->bairroEdt->text());
    m_cliente->setcidade(ui->cidadeEdt->text());
    m_cliente->setddd_celular(ui->dddCelularEdt->text());
    m_cliente->settel_celular(ui->telefoneCelularEdt->text());
    m_cliente->setddd_fixo(ui->dddFixoEdt->text());
    m_cliente->settel_fixo(ui->telefoneFixoEdt->text());
    m_cliente->setobservacao(ui->observacaoTxt->toPlainText());

    m_cliente->setgenero(ui->sexoCmb->currentData().toString());
    m_cliente->setuf(ui->ufCmb->currentText());
    if(ui->planoCmb->currentText().compare("Nenhum") != 0) {
        clinica::cliente::type_plano_odontologico plano;
        plano.reset(new clinica::plano_odontologico());
        plano->setplano_odontologico_id(ui->planoCmb->currentData().toInt());
        m_cliente->setplano_odontologico(plano);
    }

    QString retorno = m_cliente->salvar();
    if(retorno.compare("Ok") == 0) {
        QMessageBox::information(this, "Salvar", "Cliente salvo com sucesso.");
        ui->fichaLbl->setText(QString::asprintf("%06i", m_cliente->getcliente_id()));
    } else {
        QMessageBox::critical(this, "Salvar", "Erro ao salvar Cliente.\n" + retorno);
    }
}

void ClienteDialogo::onNomePesquisaKeyPress(QString text) {
    text = text.trimmed();
    if(text.length() > 2) {
        m_lista_cliente.clear();
        ui->resultadoLst->clear();
        limparCliente();
        m_lista_cliente = clinica::cliente::buscaTextualPorNome(text);
        if(m_lista_cliente.count() > 0) {
            _foreach(QSharedPointer<clinica::cliente> cliente, m_lista_cliente) {
                ui->resultadoLst->addItem(cliente->getnome());
            }
        }
    }
}

void ClienteDialogo::limpar() {
    limparCliente();
    m_lista_cliente.clear();
    ui->nomePesquisarEdt->clear();
    ui->resultadoLst->clear();
    ui->nomePesquisarEdt->setFocus();
}

void ClienteDialogo::onResultadoSelecionado(int row) {
    if(row >= 0 && row < m_lista_cliente.count()) {
        m_cliente = m_lista_cliente.getByIndex(row);

        ui->fichaLbl->setText(QString::asprintf("%06i", m_cliente->getcliente_id()));
        ui->nomeEdt->setText(m_cliente->getnome());
        ui->identidadeEdt->setText(m_cliente->getdoc_identidade());
        ui->cpfEdt->setText(m_cliente->getcpf());
        ui->profissaoEdt->setText(m_cliente->getprofissao());
        ui->cepEdt->setText(m_cliente->getcep());
        ui->logradouroEdt->setText(m_cliente->getlogradouro());
        ui->numeroEdt->setText(m_cliente->getnum_endereco());
        ui->complementoEdt->setText(m_cliente->getcompl_endereco());
        ui->bairroEdt->setText(m_cliente->getbairro());
        ui->cidadeEdt->setText(m_cliente->getcidade());
        ui->dddCelularEdt->setText(m_cliente->getddd_celular());
        ui->telefoneCelularEdt->setText(m_cliente->gettel_celular());
        ui->dddFixoEdt->setText(m_cliente->getddd_fixo());
        ui->telefoneFixoEdt->setText(m_cliente->gettel_fixo());
        ui->observacaoTxt->setPlainText(m_cliente->getobservacao());

        ui->sexoCmb->setCurrentIndex(ui->sexoCmb->findData(m_cliente->getgenero()));
        ui->ufCmb->setCurrentText(m_cliente->getuf());
        if(m_cliente->getplano_odontologico().isNull()) {
            ui->planoCmb->setCurrentText("Nenhum");
        } else {
            ui->planoCmb->setCurrentIndex(ui->planoCmb->findText(m_cliente->getplano_odontologico(true)->getnome()));
        }
    }
}

void ClienteDialogo::onResultadoRecebeFoco() {
    if(ui->resultadoLst->count() > 0) {
        ui->resultadoLst->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
}

bool ClienteDialogo::eventFilter(QObject *obj, QEvent *ev) {
    if(obj == ui->resultadoLst && ev->type() == QEvent::FocusIn) {
        onResultadoRecebeFoco();
    }
    return false;
}

bool ClienteDialogo::isEmptyCPF(QString cpf)
{
    return (cpf.trimmed().isEmpty() || cpf.trimmed() == "..-");
}

void ClienteDialogo::inativar() {
    if(QMessageBox::question(this, "Inativar", "O dados do cliente não ficarão mais disponíveis.\nConfirma inativar o cliente?") == QMessageBox::Yes) {
        QString retorno = m_cliente->inativar();
        if(retorno.compare("Ok") == 0) {
            QMessageBox::information(this, "Inativar", "Cliente inativado com sucesso.");
        } else {
            QMessageBox::critical(this, "Inativar", "Erro ao inativar Cliente.\n" + retorno);
        }
        limpar();
    }
}
