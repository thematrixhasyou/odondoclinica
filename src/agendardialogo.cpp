#include "include/agendardialogo.h"
#include "persist/include/clinica_agendamento.h"
#include "ui_agendardialogo.h"
#include <QMessageBox>
#include <QStandardItemModel>

AgendarDialogo::AgendarDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AgendarDialogo)
{
    ui->setupUi(this);
}

AgendarDialogo::~AgendarDialogo()
{
    delete ui;
}

void AgendarDialogo::init()
{
    ui->nomeLbl->setText(m_dentista->getnome());
    ui->duracaoSpn->setValue(m_dentista->getduracaoConsulta());
    ui->dataHoraEdt->setDateTime(m_dataHora);
    carregarEspecialidadesDentista();
    configurarEventos();

    completer = new QCompleter(this);
    completer->setMaxVisibleItems(5);
    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setFilterMode(Qt::MatchStartsWith);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setWrapAround(false);
    completer->setModel(clinica::cliente::retornaTodosNomeID(completer));

    ui->pacienteEdt->setCompleter(completer);
}

void AgendarDialogo::carregarEspecialidadesDentista()
{
    ui->especialidadeCmb->clear();

    if(m_lista_especialidades.count() > 0) {
        _foreach(QSharedPointer<clinica::especialidade> especialidade, m_lista_especialidades) {
            ui->especialidadeCmb->addItem(especialidade->getdescricao(), QVariant::fromValue(especialidade->getid()));
        }
        ui->especialidadeCmb->setCurrentText(m_especialidade);
    }

}

void AgendarDialogo::configurarEventos()
{
    connect(ui->cancelarBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->agendarBtn, SIGNAL(clicked()), this, SLOT(agendar()));

}

int AgendarDialogo::obtemClienteID()
{
    int i = 0;
    while(ui->pacienteEdt->text() != completer->currentIndex().data().toString() && i < completer->completionCount()) {
        completer->setCurrentRow(++i);
    }
    if(ui->pacienteEdt->text() == completer->currentIndex().data().toString()) {
        return completer->completionModel()->index(completer->currentIndex().row(), 1).data().toInt();
    }
    return 0;
}

void AgendarDialogo::agendar()
{
    int cliente_id = 0, especialidade_id = 0, i = 0;
    clinica::cliente_ptr cliente;
    clinica::especialidade_ptr especialidade;
    clinica::agendamento_ptr agendamento;

    cliente_id = obtemClienteID();

    if(cliente_id == 0) {
        if(QMessageBox::question(this, "Agendar", "O paciente " + ui->pacienteEdt->text() + " não foi localizado. \nConfirma o agendamento?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::No) {
            return;
        }
    } else {
        cliente.reset(new clinica::cliente(cliente_id));
        cliente->load();
    }

    especialidade_id = ui->especialidadeCmb->currentData().toInt();
    especialidade.reset(new clinica::especialidade(especialidade_id));

    agendamento.reset(new clinica::agendamento());
    agendamento->setdataHora(ui->dataHoraEdt->dateTime());
    agendamento->setduracao(ui->duracaoSpn->value());
    agendamento->setobservacao(ui->observacaoEdt->toPlainText());
    if(!cliente.isNull()) {
        agendamento->setclienteId(cliente);
        agendamento->setnome_paciente(cliente->getnome());
    } else {
        agendamento->setnome_paciente(ui->pacienteEdt->text().trimmed());
    }
    agendamento->setdentistaId(m_dentista);
    agendamento->setespecialidadeId(especialidade);

    if(!m_dentista->estaNoHorarioDeTrabalho(agendamento->getdataHora(), agendamento->getduracao())) {
        if(QMessageBox::question(this, "Agendar", "O agendamento está fora do horário de trabalho do Dentista.\n Deseja agendar assim mesmo?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::No) {
            return;
        }
    }

    QString retorno = agendamento->salvar();
    if(retorno.compare("Ok") != 0) {
        QMessageBox::critical(this, "Agendar", "Erro ao gravar o agendamento.\n" + retorno);
    } else {
        close();
    }

}
