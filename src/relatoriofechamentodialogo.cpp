#include ".\include\relatoriofechamentodialogo.h"
#include "ui_relatoriofechamentodialogo.h"
#include <qtrpt.h>

#include <QMessageBox>

RelatorioFechamentoDialogo::RelatorioFechamentoDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RelatorioFechamentoDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    configurarEventos();
}

RelatorioFechamentoDialogo::~RelatorioFechamentoDialogo()
{
    delete ui;
}

int RelatorioFechamentoDialogo::exec()
{
    init();
    return QDialog::exec();
}

void RelatorioFechamentoDialogo::init()
{
    ui->datInicio->setDate(QDate::currentDate());
    ui->datTermino->setDate(QDate::currentDate());
}

void RelatorioFechamentoDialogo::configurarEventos()
{
    connect(ui->btnFechar, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnImprimir, SIGNAL(clicked()), this, SLOT(imprimir()));
}

void RelatorioFechamentoDialogo::setValue(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage)
{
    if (reportPage == 0) {
        if (paramName == "data_inicio") {
            paramValue = ui->datInicio->date().toString("dd/MM/yyyy");
        }
        else if (paramName == "data_termino") {
            paramValue = ui->datTermino->date().toString("dd/MM/yyyy");
        }
        else if (paramName == "nome_dentista") {
            paramValue = m_listaDetalhe.at(recNo).nome_dentista;
        }
        else if (paramName == "producao_cheque") {
            paramValue = m_listaDetalhe.at(recNo).producao_cheque;
        }
        else if (paramName == "producao_credito") {
            paramValue = m_listaDetalhe.at(recNo).producao_credito;
        }
        else if (paramName == "producao_debito") {
            paramValue = m_listaDetalhe.at(recNo).producao_debito;
        }
        else if (paramName == "producao_dinheiro") {
            paramValue = m_listaDetalhe.at(recNo).producao_dinheiro;
        }
        else if (paramName == "producao_total") {
            paramValue = m_listaDetalhe.at(recNo).producao_total;
        }
        else if (paramName == "fat_corr_chegue") {
            paramValue = m_listaDetalhe.at(recNo).fat_corr_chegue;
        }
        else if (paramName == "fat_corr_debito") {
            paramValue = m_listaDetalhe.at(recNo).fat_corr_debito;
        }
        else if (paramName == "fat_corr_dinheiro") {
            paramValue = m_listaDetalhe.at(recNo).fat_corr_dinheiro;
        }
        else if (paramName == "fat_corr_credito") {
            paramValue = m_listaDetalhe.at(recNo).fat_corr_credito;
        }
        else if (paramName == "fat_corr_total") {
            paramValue = m_listaDetalhe.at(recNo).fat_corr_total;
        }
        else if (paramName == "fat_corr_comissao") {
            paramValue = m_listaDetalhe.at(recNo).fat_corr_comissao;
        }
        else if (paramName == "fat_fut_credito") {
            paramValue = m_listaDetalhe.at(recNo).fat_fut_credito;
        }
        else if (paramName == "fat_fut_debito") {
            paramValue = m_listaDetalhe.at(recNo).fat_fut_debito;
        }
        else if (paramName == "fat_fut_total") {
            paramValue = m_listaDetalhe.at(recNo).fat_fut_total;
        }
    }
}

void RelatorioFechamentoDialogo::imprimir()
{
    clinica::pagamento pagamento;
    if(ui->datTermino->date() <= ui->datInicio->date()) {
        QMessageBox::information(this, "Relatório de Fechamento", "A data de término não pode ser menor ou igual a data de início do período.");
        return;
    }

    m_listaDetalhe = pagamento.retornaFechamentoPeriodo(ui->datInicio->date(), ui->datTermino->date());

    if(m_listaDetalhe.count() == 0) {
        QMessageBox::information(this, "Relatório de Fechamento", "Não há pagamentos realizados no período.");
        return;
    }

    QtRPT *report = new QtRPT(this);
    report->recordCount << m_listaDetalhe.count();
    report->loadReport("relatorio_fechamento_consolidado.xml");
    QObject::connect(report, SIGNAL(setValue(const int, const QString, QVariant&, const int)), this, SLOT(setValue(const int, const QString, QVariant&, const int)));
    report->printExec();

}
