#include <qtrpt.h>
#include "include/consultaragendadialogo.h"
#include "persist/include/clinica_dentista.h"
#include "ui_consultaragendadialogo.h"

ConsultarAgendaDialogo::ConsultarAgendaDialogo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConsultarAgendaDialogo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setModal(true);
    init();
}

ConsultarAgendaDialogo::~ConsultarAgendaDialogo()
{
    delete ui;
}

void ConsultarAgendaDialogo::init()
{
    connect(ui->fecharBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->dentistaCmb, SIGNAL(currentIndexChanged(int)), this, SLOT(onDentistaSelecionado(int)));
    connect(ui->dataEdt, SIGNAL(dateChanged(QDate)), this, SLOT(onDataAlterada(QDate)));
    connect(ui->imprimirBtn, SIGNAL(clicked()), this, SLOT(imprimir()));

    carregarListaDentista();
    ui->dataEdt->setDate(QDate::currentDate());
}

void ConsultarAgendaDialogo::carregarListaDentista()
{
    ui->dentistaCmb->clear();

    m_lista_dentista = clinica::dentista::retornaTodosDentistasOrdenadosPorNome();
    if(m_lista_dentista.count() > 0) {
        _foreach(QSharedPointer<clinica::dentista> dentista, m_lista_dentista) {
            ui->dentistaCmb->addItem(dentista->getnome());
        }
        ui->dentistaCmb->setCurrentIndex(0);
        m_dentista = m_lista_dentista.getByIndex(0);
    }
}

void ConsultarAgendaDialogo::carregarAgenda()
{
    QString nome_plano;
    clinica::dentista::type_listaAgendamento agendamentosDoDia = m_dentista->getlistaAgendamentoDoDia(m_data);
    ui->agendaTbl->setRowCount(agendamentosDoDia.count());
    ui->agendaTbl->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    ui->agendaTbl->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);

    int i = 0;
    _foreach(QSharedPointer<clinica::agendamento> agendamento, agendamentosDoDia) {

        QTableWidgetItem *inicio = new QTableWidgetItem(agendamento->getdataHora().time().toString("HH:mm"));
        ui->agendaTbl->setItem(i, 0, inicio);

        QTableWidgetItem *termino = new QTableWidgetItem(agendamento->getdataHora().time().addSecs(agendamento->getduracao()*60).toString("HH:mm"));
        ui->agendaTbl->setItem(i, 1, termino);

        QTableWidgetItem *especialidade = new QTableWidgetItem(agendamento->getespecialidadeId()->getdescricao());
        ui->agendaTbl->setItem(i, 2, especialidade);

        QTableWidgetItem *paciente = new QTableWidgetItem(agendamento->getnome_paciente());
        ui->agendaTbl->setItem(i, 3, paciente);


        if(!agendamento->getclienteId().isNull()) {
            clinica::plano_odontologico_ptr plano_ptr = agendamento->getclienteId()->getplano_odontologico(true);
            if(plano_ptr) {
                nome_plano = plano_ptr->getnome();
            }
            else {
                nome_plano = "Sem plano";
            }
        } else {
            nome_plano = "Cliente não cadastrado";
        }
        QTableWidgetItem *plano = new QTableWidgetItem(nome_plano);
        ui->agendaTbl->setItem(i, 4, plano);

        i++;
    }
}

void ConsultarAgendaDialogo::onDentistaSelecionado(int row)
{
    if(row >= 0 && row < m_lista_dentista.count()) {
        m_dentista = m_lista_dentista.getByIndex(row);
        carregarAgenda();
    }
}

void ConsultarAgendaDialogo::onDataAlterada(QDate data)
{
    m_data = data;
    carregarAgenda();
}

void ConsultarAgendaDialogo::imprimir()
{
    QtRPT *report = new QtRPT(this);
    report->loadReport("agenda_diaria.xml");
    report->recordCount << ui->agendaTbl->rowCount();
    QObject::connect(report, SIGNAL(setValue(const int, const QString, QVariant&, const int)), this, SLOT(setValue(const int, const QString, QVariant&, const int)));
    report->printExec(true,false);
}

void ConsultarAgendaDialogo::setValue(const int recNo, const QString paramName, QVariant &paramValue, const int reportPage)
{
    if (paramName == "dentista")
        paramValue = m_dentista->getnome();
    if (paramName == "data")
        paramValue = m_data.toString("dddd - dd 'de' MMMM 'de' yyyy");
    if (paramName == "inicio") {
        if (ui->agendaTbl->item(recNo,0) == 0) return;
        paramValue = ui->agendaTbl->item(recNo,0)->text();
    }
    if (paramName == "termino") {
        if (ui->agendaTbl->item(recNo,1) == 0) return;
        paramValue = ui->agendaTbl->item(recNo,1)->text();
    }
    if (paramName == "especialidade") {
        if (ui->agendaTbl->item(recNo,2) == 0) return;
        paramValue = ui->agendaTbl->item(recNo,2)->text();
    }
    if (paramName == "paciente") {
        if (ui->agendaTbl->item(recNo,3) == 0) return;
        paramValue = ui->agendaTbl->item(recNo,3)->text();
    }
    if (paramName == "plano") {
        if (ui->agendaTbl->item(recNo,4) == 0) return;
        paramValue = ui->agendaTbl->item(recNo,4)->text();
    }
}
