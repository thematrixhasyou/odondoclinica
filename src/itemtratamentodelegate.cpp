#include "./include/itemtratamentodelegate.h"
#include <QComboBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QWidget>
#include <QModelIndex>
#include <QString>
#include <QApplication>


ItemTratamentoDelegate::ItemTratamentoDelegate(QObject *parent):QItemDelegate(parent)
{
    m_listaProcedimentos = clinica::procedimento::retornaTodosProcedimentosOrdenadosPorNome();
}

QWidget *ItemTratamentoDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
    if (index.column() == 1) {
        QComboBox *editor = new QComboBox(parent);
        _foreach(QSharedPointer<clinica::procedimento> procedimento, m_listaProcedimentos) {
            editor->addItem(procedimento->getdescricao(), QVariant::fromValue(procedimento->getprocedimento_id()));
        }
        return editor;
    }
    if (index.column() == 2) {
        QSpinBox *editor = new QSpinBox(parent);
        editor->setFrame(false);
        editor->setMinimum(0);
        editor->setMaximum(99);
        return editor;
    }
    if (index.column() == 3) {
        QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
        editor->setFrame(false);
        editor->setMinimum(0);
        editor->setMaximum(9999);
        return editor;
    }
    if (index.column() == 5) {
        QComboBox *editor = new QComboBox(parent);
        editor->insertItem(0, "Não Iniciado", QVariant("NIN"));
        editor->insertItem(1, "Em Andamento", QVariant("EAN"));
        editor->insertItem(2, "Concluído", QVariant("CON"));
        return editor;
    }

    return nullptr;
}

void ItemTratamentoDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if (index.column() == 1 || index.column() == 5) {
        QComboBox *comboBox = static_cast<QComboBox*>(editor);
        int value = comboBox->findText(index.data(Qt::EditRole).toString());
        comboBox->setCurrentIndex(value);
    }
    if (index.column() == 2) {
        int value = index.model()->data(index, Qt::EditRole).toInt();
        QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
        spinBox->setValue(value);
    }
    if (index.column() == 3) {
        double value = index.model()->data(index, Qt::EditRole).toDouble();
        QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
        spinBox->setValue(value);
    }
}

void ItemTratamentoDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if (index.column() == 1 || index.column() == 5) {
        QComboBox *comboBox = static_cast<QComboBox*>(editor);
        model->setData(index, comboBox->currentData(Qt::EditRole), Qt::EditRole);
        model->setData(index, comboBox->currentData(Qt::UserRole), Qt::UserRole);
    }
    if (index.column() == 2) {
        QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
        spinBox->interpretText();
        int value = spinBox->value();
        model->setData(index, value, Qt::EditRole);
    }
    if (index.column() == 3) {
        QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
        spinBox->interpretText();
        double value = spinBox->value();
        model->setData(index, value, Qt::EditRole);
    }
}

void ItemTratamentoDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

void ItemTratamentoDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem myOption = option;
    QString text;

    if (index.column() == 3) {
        text = QString::number(index.data(Qt::EditRole).toDouble(), 'f', 2);
    } else {
        text = index.data(Qt::EditRole).toString();
    }

    myOption.text = text;
    if (index.column() == 3 || index.column() == 4) {
        myOption.displayAlignment = Qt::AlignRight | Qt::AlignVCenter;
    }

    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}

